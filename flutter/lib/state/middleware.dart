import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mealmonkey/firebase/models/firestoreModel.dart';
import 'package:mealmonkey/firebase/models/meal.dart';
import 'package:mealmonkey/firebase/models/plan.dart';
import 'package:mealmonkey/firebase/models/user.dart';
import 'package:mealmonkey/firebase/models/week.dart';
import 'package:mealmonkey/state/action.dart';
import 'package:mealmonkey/state/appstate.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

final allEpics = combineEpics<AppState>([
  updateUserEpic,
  watchUserEpic,
  updatePlanEpic,
  watchPlanEpic,
  updateWeekEpic,
  watchWeekEpic,
  updateMealsEpic,
  watchMealsEpic,
]);

/// Updates the User on firestore when there is a change in the local redux store. 
Stream<dynamic> updateUserEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
  return new Observable(actions).ofType(new TypeToken<UpdateUserAction>()).flatMap(
    (UpdateUserAction updateUserAction) {
      return new Observable.fromFuture(
        updateUserAction.user
            .save()
            .then((_) => new UserDataPushedAction())
            .catchError((error) => new UserOnErrorEventAction(error: error)),
      );
    },
  );
}

// Watches Firestore and when there is a change updates local Redux store (without updating Firestore)
Stream<dynamic> watchUserEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
  return new Observable(actions).ofType(new TypeToken<RequestUserDataEventsAction>()).switchMap(
    (RequestUserDataEventsAction requestAction) {
      if (store.state.user == null) return null; // NOTE: May need to check store.state.user
      return getUser(store.state.user)
          .map((user) => new UpdateLocalUserAction(user: user))
          .takeUntil(actions.where((action) => action is CancelUserDataEventsAction));
    },
  );
}

Observable<User> getUser(User user) =>
    getObservable(user, (DocumentSnapshot doc) => User.from(doc));

/// Updates the Plan on firestore when there is a change in the local redux store. 
Stream<dynamic> updatePlanEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
  return new Observable(actions).ofType(new TypeToken<UpdatePlanAction>()).flatMap(
    (UpdatePlanAction updatePlanAction) {
      return new Observable.fromFuture(
        updatePlanAction.plan
            .save()
            .then((_) => new PlanDataPushedAction())
            .catchError((error) => new PlanOnErrorEventAction(error: error)),
      );
    },
  );
}

// Watches Firestore and when there is a change updates local Redux store (without updating Firestore)
Stream<dynamic> watchPlanEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
  return new Observable(actions).ofType(new TypeToken<RequestPlanDataEventsAction>()).switchMap(
    (RequestPlanDataEventsAction requestAction) {
      if (store.state.plan == null) return null;
      return getPlan(store.state.plan)
          .map((plan) => new UpdateLocalPlanAction(plan: plan))
          .takeUntil(actions.where((action) => action is CancelPlanDataEventsAction));
    },
  );
}

Observable<Plan> getPlan(Plan plan) =>
    getObservable(plan, (DocumentSnapshot doc) => Plan.from(doc));

/// Updates the Week on firestore when there is a change in the local redux store. 
Stream<dynamic> updateWeekEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
  return new Observable(actions).ofType(new TypeToken<UpdateWeekAction>()).flatMap(
    (UpdateWeekAction updateWeekAction) {
      return new Observable.fromFuture(
        updateWeekAction.week
            .save()
            .then((_) => new WeekDataPushedAction())
            .catchError((error) => new WeekOnErrorEventAction(error: error)),
      );
    },
  );
}

// Watches Firestore and when there is a change updates local Redux store (without updating Firestore)
Stream<dynamic> watchWeekEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
  return new Observable(actions).ofType(new TypeToken<RequestWeekDataEventsAction>()).switchMap(
    (RequestWeekDataEventsAction requestAction) {
      if (store.state.week == null) return null;
      return getWeek(store.state.week.reference)
          .map((week) => new UpdateLocalWeekAction(week: week))
          .takeUntil(actions.where((action) => action is CancelWeekDataEventsAction));
    },
  );
}

Observable<Week> getWeek(DocumentReference weekReference) {
  return new Observable(weekReference.snapshots()).map((DocumentSnapshot doc) => Week.from(doc));
}

/// Updates the Meals on firestore when there is a change in the local redux store. 
Stream<dynamic> updateMealsEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
  return new Observable(actions).ofType(new TypeToken<UpdateMealsAction>()).flatMap(
    (UpdateMealsAction updateMealsAction) {
      return Observable.fromIterable(
        updateMealsAction.meals.map(
          (meal) => new Observable.fromFuture(
                meal
                    .save()
                    .then((_) => new MealsDataPushedAction())
                    .catchError((error) => new MealsOnErrorEventAction(error: error)),
              ),
        ),
      );
    },
  );
}

// Watches Firestore and when there is a change updates local Redux store (without updating Firestore)
Stream<dynamic> watchMealsEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
  return new Observable(actions).ofType(new TypeToken<RequestMealsDataEventsAction>()).switchMap(
    (RequestMealsDataEventsAction requestAction) {
      if (store.state.week == null) return null;
      return getMeals(store.state.week.reference.collection(Meal.collectionName))
          .map((meals) => new UpdateLocalMealsAction(meals: meals))
          .takeUntil(actions.where((action) => action is CancelMealsDataEventsAction));
    },
  );
}

// FIXME: May need to use asyncMap here but not sure.
/// Returns [List<Meals>] from the firestore. 
Observable<List<Meal>> getMeals(Query query) {
  final Observable<QuerySnapshot> observableSnapshot = Observable(query.snapshots());
  return observableSnapshot.map<List<Meal>>((QuerySnapshot snapshot) {
    List<Meal> result = [];
    snapshot.documents.forEach((mealSnap) => result.add(Meal.from(mealSnap)));
    return result;
  });
}

typedef T FirestoreModelMapperFunction<T extends FirestoreModel>(DocumentSnapshot doc);

Observable<T> getObservable<T extends FirestoreModel>(
  T model,
  FirestoreModelMapperFunction mapper,
) =>
    Observable(model.reference.snapshots()).map(mapper);
