import 'package:mealmonkey/state/action.dart';
import 'package:mealmonkey/state/appstate.dart';

/// Updates the local storage using the [state] and an input [action].
AppState appReducer(AppState state, Object action) {
  if (action is UpdateUserAction) {
    return state.copyWith(user: action.user);
  } else if (action is UpdateLocalUserAction) {
    return state.copyWith(user: action.user);
  } else if (action is UpdatePlanAction) {
    return state.copyWith(plan: action.plan);
  } else if (action is UpdateLocalPlanAction) {
    return state.copyWith(plan: action.plan);
  } else if (action is UpdateWeekAction) {
    return state.copyWith(week: action.week);
  } else if (action is UpdateLocalWeekAction) {
    return state.copyWith(week: action.week);
  } else if (action is UpdateMealsAction) {
    return state.copyWith(meals: action.meals);
  } else if (action is UpdateLocalMealsAction) {
    return state.copyWith(meals: action.meals);
  } else if (action is SetLoadingAction) {
    return state.copyWith(loading: action.loading);
  } else if (action is SetOnboardedAction) {
    return state.copyWith(onboarded: action.onboarded);
  } else if (action is SetDisplayHelpAction) {
    return state.copyWith(displayHelp: action.displayHelp);
  } else if (action is SetFirstBoxCheckedAction) {
    return state.copyWith(firstBoxChecked: action.firstBoxChecked);
  } else if (action is SetFlushbarHelperAction) {
    return state.copyWith(flushbarHelper: action.flushbarHelper);
  } else if (action is LogoutUserAction) {
    return AppState.logout();
  } // else if (action is LoadedAction) {
  //   return action.state ?? state;
  // }

  print(action);

  return state;
}
