import 'package:mealmonkey/firebase/models/meal.dart';
import 'package:mealmonkey/firebase/models/plan.dart';
import 'package:mealmonkey/firebase/models/user.dart';
import 'package:mealmonkey/firebase/models/week.dart';
import 'package:mealmonkey/screens/util/flushbar.dart';

// User
class RequestUserDataEventsAction {}

class CancelUserDataEventsAction {}

class UserDataPushedAction {}

class UserOnErrorEventAction {
  final dynamic error;

  UserOnErrorEventAction({this.error});
}

class UpdateUserAction {
  final User user;

  UpdateUserAction({this.user});
}

class UpdateLocalUserAction {
  final User user;

  UpdateLocalUserAction({this.user});
}

// Plan
class RequestPlanDataEventsAction {}

class CancelPlanDataEventsAction {}

class PlanDataPushedAction {}

class PlanOnErrorEventAction {
  final dynamic error;

  PlanOnErrorEventAction({this.error});
}

class UpdatePlanAction {
  final Plan plan;

  UpdatePlanAction({this.plan});
}

class UpdateLocalPlanAction {
  final Plan plan;

  UpdateLocalPlanAction({this.plan});
}


// Week
class RequestWeekDataEventsAction {}

class CancelWeekDataEventsAction {}

class WeekDataPushedAction {}

class WeekOnErrorEventAction {
  final dynamic error;

  WeekOnErrorEventAction({this.error});
}

class UpdateWeekAction {
  final Week week;

  UpdateWeekAction({this.week});
}

class UpdateLocalWeekAction {
  final Week week;

  UpdateLocalWeekAction({this.week});
}

// Meals
class RequestMealsDataEventsAction {}

class CancelMealsDataEventsAction {}

class MealsDataPushedAction {}

class MealsOnErrorEventAction {
  final dynamic error;

  MealsOnErrorEventAction({this.error});
}

class UpdateMealsAction {
  final List<Meal> meals;

  UpdateMealsAction({this.meals});
}

class UpdateLocalMealsAction {
  final List<Meal> meals;

  UpdateLocalMealsAction({this.meals});
}

// class UpdateMealsIngredientsAction {
//   final Ingredient ingredient;

//   UpdateMealsIngredientsAction({this.ingredient});
// }

// Loading
class SetLoadingAction {
  final bool loading;

  SetLoadingAction({this.loading});
}

// Onboarded
class SetOnboardedAction {
  final bool onboarded;

  SetOnboardedAction({this.onboarded});
}

// DisplayHelp
class SetDisplayHelpAction {
  final bool displayHelp;

  SetDisplayHelpAction({this.displayHelp});
}

// FlushbarHelper
class SetFlushbarHelperAction {
  final FlushbarUtil flushbarHelper;

  SetFlushbarHelperAction({this.flushbarHelper});
}

// First Box Checked
class SetFirstBoxCheckedAction {
  final bool firstBoxChecked;

  SetFirstBoxCheckedAction({this.firstBoxChecked});
}


// Account Mangement
class LogoutUserAction {}