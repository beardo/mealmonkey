import 'package:mealmonkey/firebase/models/meal.dart';
import 'package:mealmonkey/firebase/models/plan.dart';
import 'package:mealmonkey/firebase/models/user.dart';
import 'package:mealmonkey/firebase/models/week.dart';
import 'package:mealmonkey/screens/util/flushbar.dart';
import 'package:meta/meta.dart';

/// Creates the state for the entire application.
class AppState {
  final User user;
  final Plan plan;
  final Week week;
  final List<Meal> meals;
  final bool loading;
  final bool onboarded;
  final bool displayHelp;
  final bool firstBoxChecked;
  final FlushbarUtil flushbarHelper;

  AppState({
    @required this.user,
    @required this.plan,
    @required this.week,
    @required this.meals,
    @required this.loading,
    @required this.onboarded,
    @required this.displayHelp,
    @required this.firstBoxChecked,
    @required this.flushbarHelper,
  });

  // Sets the initial state for the app.
  AppState.initialState()
      : user = null,
        plan = null,
        week = null,
        meals = [],
        loading = false,
        onboarded = false,
        displayHelp = true,
        firstBoxChecked = false,
        flushbarHelper = new FlushbarUtil();

  // Updates the state.
  AppState copyWith({
    User user,
    Plan plan,
    Week week,
    List<Meal> meals,
    bool onboarded,
    bool loading,
    bool displayHelp,
    bool firstBoxChecked,
    FlushbarUtil flushbarHelper,
  }) =>
      AppState(
        user: user ?? this.user,
        plan: plan ?? this.plan,
        week: week ?? this.week,
        meals: meals ?? this.meals,
        loading: loading ?? this.loading,
        onboarded: onboarded ?? this.onboarded,
        displayHelp: displayHelp ?? this.displayHelp,
        firstBoxChecked: firstBoxChecked ?? this.firstBoxChecked,
        flushbarHelper: flushbarHelper ?? this.flushbarHelper,
      );

  // Sets the state on logout.
  static AppState logout() {
    AppState state = AppState.initialState();
    return state.copyWith(onboarded: true);
  }

  // Sets the state with persisting onboarded boolean.
  static AppState fromJson(dynamic state) {
    AppState initialState = AppState.initialState();

    if (state == null) {
      return initialState;
    }

    return initialState.copyWith(
      onboarded: state['onboarded'],
      displayHelp: state['displayHelp'],
      firstBoxChecked: state['firstBoxChecked'],
    );
  }

  dynamic toJson() => {
        'onboarded': onboarded,
        'displayHelp': displayHelp,
        'firstBoxChecked': firstBoxChecked,
      };
}
