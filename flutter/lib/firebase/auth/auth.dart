import 'dart:async';
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:mealmonkey/firebase/auth/errors.dart';
import 'package:mealmonkey/firebase/auth/types.dart';
import 'package:mealmonkey/firebase/models/meal.dart';
import 'package:mealmonkey/firebase/models/plan.dart';
import 'package:mealmonkey/firebase/models/user.dart';
import 'package:mealmonkey/firebase/models/week.dart';
import 'package:mealmonkey/firebase/support_models/planUser.dart';
import 'package:mealmonkey/screens/util/flushbar.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;

class Auth {
  static final GoogleSignIn googleSignIn = new GoogleSignIn();
  static final FacebookLogin facebookSignIn = new FacebookLogin();

  static Future<AuthAction> authenticateUser({
    @optionalTypeArgs String email,
    @optionalTypeArgs String username,
    @optionalTypeArgs String password,
    @required AuthType type,
    @required AuthAction action,
    @required AuthViewModel viewModel,
    @required BuildContext context,
  }) async {

    FlushbarUtil flushbarHelper = FlushbarUtil.getInstance(StoreProvider.of(context));
    Flushbar loadingBar = flushbarHelper.create(type: FlushbarType.INFO, message: "Loading...");

    if (type == AuthType.google) {
      // Attempt to get the currently authenticated user
      GoogleSignInAccount currentGoogleUser = googleSignIn.currentUser;

      try {
        if (currentGoogleUser == null) {
          // Attempt to sign in without user interaction
          currentGoogleUser = await googleSignIn.signInSilently();
        }
        if (currentGoogleUser == null) {
          // Force the user to interactively sign in
          currentGoogleUser = await googleSignIn.signIn();
        }
      } catch (e) {
        if (e.code == "sign_in_failed") {
          // NOTE: This should be the error thrown, but the plugin is reporting the wrong issue..
          // throw GoogleAuthFailed();

          throw GoogleAuthCancelled();
        } else if (e.code == "sign_in_cancelled") {
          throw GoogleAuthCancelled();
        }
      }

      loadingBar.show(context);

      // Authenticate with Firebase
      FirebaseUser firebaseUser;

      try {
        final GoogleSignInAuthentication auth = await currentGoogleUser.authentication;
        firebaseUser = await FirebaseAuth.instance.signInWithGoogle(
          idToken: auth.idToken,
          accessToken: auth.accessToken,
        );
      } catch (e) {
        // loadingBar.dismiss();
        await flushbarHelper.dismissAll();
        throw (e);
      }

      // Check to see if existing User with Google account
      DocumentSnapshot userSnapshot =
          await Firestore.instance.collection(User.collectionName).document(firebaseUser.uid).get();

      // Update viewModel accordingly
      if (userSnapshot.exists) {
        action = AuthAction.login;
        await Auth.updateViewModel(
          viewModel: viewModel,
          action: action,
          type: type,
          userId: firebaseUser.uid,
          userSnapshot: userSnapshot,
        );
      } else {
        action = AuthAction.register;
        await Auth.updateViewModel(
          viewModel: viewModel,
          action: action,
          type: type,
          userId: firebaseUser.uid,
          username: currentGoogleUser.displayName,
          email: currentGoogleUser.email,
          photo: currentGoogleUser.photoUrl,
        );
      }

      // loadingBar.dismiss();
      await flushbarHelper.dismissAll();
    } else if (type == AuthType.facebook) {
      FacebookLoginResult facebookLoginResult =
          await facebookSignIn.logInWithReadPermissions(['email']);

      if (facebookLoginResult.status == FacebookLoginStatus.loggedIn) {
        final FacebookAccessToken accessToken = facebookLoginResult.accessToken;

        // Authenticate with Firebase
        FirebaseUser firebaseUser;
        try {
          loadingBar.show(context);
          firebaseUser = await FirebaseAuth.instance.signInWithFacebook(
            accessToken: accessToken.token,
          );
        } catch (e) {
          // loadingBar.dismiss();
          await flushbarHelper.dismissAll();
          throw (e);
        }

        // Check to see if existing User with Facebook account
        DocumentSnapshot userSnapshot = await Firestore.instance
            .collection(User.collectionName)
            .document(firebaseUser.uid)
            .get();

        // Update viewModel accordingly
        if (userSnapshot.exists) {
          action = AuthAction.login;
          await Auth.updateViewModel(
            viewModel: viewModel,
            action: action,
            type: type,
            userId: firebaseUser.uid,
            userSnapshot: userSnapshot,
          );
        } else {
          var result = await facebookSignIn.logInWithReadPermissions(['email']);
          var accessToken = result.accessToken;
          var graphResponse = await http.get(
            'https://graph.facebook.com/v2.12/me?fields=name,email,picture&access_token=${accessToken.token}',
          );
          var profile = json.decode(graphResponse.body);

          action = AuthAction.register;
          await Auth.updateViewModel(
            viewModel: viewModel,
            action: action,
            type: type,
            userId: firebaseUser.uid,
            username: profile['name'],
            email: profile['email'],
            photo: profile["picture"]["data"]["url"],
          );
        }

        // loadingBar.dismiss();
        await flushbarHelper.dismissAll();
      } else if (facebookLoginResult.status == FacebookLoginStatus.cancelledByUser) {
        throw FacebookAuthCancelled();
      } else if (facebookLoginResult.status == FacebookLoginStatus.error) {
        throw FacebookAuthFailed();
      }
    } else if (type == AuthType.custom) {
      if (action == AuthAction.register) {
        // Create the user on Firebase Auth
        FirebaseUser firebaseUser;
        try {
          loadingBar.show(context);
          firebaseUser = await FirebaseAuth.instance.createUserWithEmailAndPassword(
            email: email,
            password: password,
          );
        } catch (e) {
          await flushbarHelper.dismissAll();
          // loadingBar.dismiss();
          throw (e);
        }

        // TODO: Uncomment before going live
        // Send the user a verification email
        // newFirebaseUser.sendEmailVerification();

        // Update the local Redux store
        await Auth.updateViewModel(
          viewModel: viewModel,
          action: action,
          type: type,
          userId: firebaseUser.uid,
          username: username,
          email: email,
          photo: null,
        );

        // loadingBar.dismiss();
        await flushbarHelper.dismissAll();
      } else if (action == AuthAction.login) {
        // Try getting the existing user from Firebase using the form input
        FirebaseUser firebaseUser;
        try {
          loadingBar.show(context);
          firebaseUser = await FirebaseAuth.instance.signInWithEmailAndPassword(
            email: email,
            password: password,
          );
        } catch (e) {
          await flushbarHelper.dismissAll();
          throw (e);
        }

        // Get the user snapshot
        DocumentSnapshot userSnapshot = await Firestore.instance
            .collection(User.collectionName)
            .document(firebaseUser.uid)
            .get();

        // Update the local Redux store
        await Auth.updateViewModel(
          viewModel: viewModel,
          action: action,
          type: type,
          userId: firebaseUser.uid,
          userSnapshot: userSnapshot,
        );

        // loadingBar.dismiss();
        await flushbarHelper.dismissAll();
      }
    }

    return action;
  }

  static Future<void> updateViewModel({
    @required AuthViewModel viewModel,
    @required AuthAction action,
    @required AuthType type,
    @required String userId,
    @optionalTypeArgs DocumentSnapshot userSnapshot,
    @optionalTypeArgs String username,
    @optionalTypeArgs String email,
    @optionalTypeArgs String photo,
  }) async {
    viewModel.setLoading(true);
    if (action == AuthAction.register) {
      // Get the FCM token
      String fcmToken = await FirebaseMessaging().getToken();

      // Create an instance of our frontend model user
      User newUser = await User.create(
        user: User.initialUser(
          username: username,
          email: email,
          picture: photo,
          fcmToken: fcmToken,
          providerAccount: type != AuthType.custom,
        ),
        id: userId,
      );

      // Set the newly registered user into the Redux store
      viewModel.setUser(newUser, action);
    } else if (action == AuthAction.login) {
      // Convert the Firebase user to an instance of our frontend users
      User loggedInUser = User.from(userSnapshot);

      // Set our user to the logged in user
      viewModel.user = loggedInUser;
      viewModel.user.lastLogin = Timestamp.now();

      // Check if we have a new fcmToken
      String fcmToken = await FirebaseMessaging().getToken();
      if (!viewModel.user.fcmToken.contains(fcmToken)) {
        viewModel.user.fcmToken.add(fcmToken);
      }

      // Set the new user into the Redux store
      viewModel.setUser(viewModel.user, action);

      // Check if the user is setup
      if (viewModel.user.setup) {
        // Get the user's plan and put it in the Redux store
        viewModel.plan = Plan.from(await viewModel.user.plan.get());
        viewModel.setPlan(viewModel.plan, action);

        // Check to see if we need to add the new FCM Token to the user in the plan
        PlanUser planUser = viewModel.plan.getPlanUserFromRef(viewModel.user.reference);
        if (!planUser.fcmToken.contains(fcmToken)) {
          viewModel.plan.addFCMTokenToPlanUser(viewModel.user.reference, fcmToken);
          viewModel.setPlan(viewModel.plan, action);
        }

        // Get the plan's current week and put it in the Redux store
        viewModel.week = Week.from(await viewModel.plan.getCurrentWeek());
        viewModel.setWeek(viewModel.week);

        // Get the list of meals from the plan's current week and put them in the Redux store
        viewModel.meals = await viewModel.week.getMeals();
        viewModel.setMeals(viewModel.meals);
      }
    }
    viewModel.setLoading(false);
  }
}

typedef SetUser(User user, AuthAction action);
typedef SetPlan(Plan plan, AuthAction action);
typedef SetWeek(Week week);
typedef SetMeals(List<Meal> meals);
typedef SetLoading(bool loading);

class AuthViewModel {
  final SetUser setUser;
  final SetPlan setPlan;
  final SetWeek setWeek;
  final SetMeals setMeals;
  final SetLoading setLoading;
  User user;
  Plan plan;
  Week week;
  List<Meal> meals;
  bool loading;

  AuthViewModel({
    this.setUser,
    this.setPlan,
    this.setWeek,
    this.setMeals,
    this.setLoading,
    this.user,
    this.plan,
    this.loading,
  });
}
