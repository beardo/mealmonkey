enum AuthType {
  custom,
  google,
  facebook,
}

enum AuthAction {
  register,
  login,
}
