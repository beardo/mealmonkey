/// Exception for invalid parameters 
class GoogleAuthCancelled implements Exception {
  String get message => "You cancelled Google authentication.";
}

class GoogleAuthFailed implements Exception {
  String get message => "Something went wrong during Google Authentication.";
}

class FacebookAuthCancelled implements Exception {
  String get message => "You cancelled Facebook authentication.";
}
class FacebookAuthFailed implements Exception {
  String get message => "Something went wrong during Facebook authentication.";
}