enum IngredientStatus {
  CHECKED,
  NOT_CHECKED,
  DASHED,
  UNKNOWN,
}

class IngredientStatusHelper {
  static IngredientStatus fromString(String status) {
    switch (status) {
      case 'IngredientStatus.CHECKED':
        return IngredientStatus.CHECKED;
        break;
      case 'IngredientStatus.NOT_CHECKED':
        return IngredientStatus.NOT_CHECKED;
        break;
      case 'IngredientStatus.DASHED':
        return IngredientStatus.DASHED;
        break;
      default:
        return IngredientStatus.UNKNOWN;
    }
  }
}
