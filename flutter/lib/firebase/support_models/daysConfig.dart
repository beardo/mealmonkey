import 'package:cloud_firestore/cloud_firestore.dart';

class DaysConfig {
  List<DocumentReference> monday;
  List<DocumentReference> tuesday;
  List<DocumentReference> wednesday;
  List<DocumentReference> thursday;
  List<DocumentReference> friday;
  List<DocumentReference> saturday;
  List<DocumentReference> sunday;

  List<DocumentReference> getDay(List<DocumentReference> day){
    if (monday == day) return monday;
    if (tuesday == day) return tuesday;
    if (wednesday == day) return wednesday;
    if (thursday == day) return thursday;
    if (friday == day) return friday;
    if (saturday == day) return saturday;
    if (sunday == day) return sunday;
    return null;
  }

  List<List<DocumentReference>> getAllSelected(){
    List<List<DocumentReference>> allSelected = new List<List<DocumentReference>>();
    if (mondayIsSelected) allSelected.add(monday);
    if (tuesdayIsSelected) allSelected.add(tuesday);
    if (wednesdayIsSelected) allSelected.add(wednesday);
    if (thursdayIsSelected) allSelected.add(thursday);
    if (fridayIsSelected) allSelected.add(friday);
    if (saturdayIsSelected) allSelected.add(saturday);
    if (sundayIsSelected) allSelected.add(sunday);
    return allSelected;
  }

  // Shortcut for checking if a day is checked or not
  bool get mondayIsSelected => monday != null;
  bool get tuesdayIsSelected => tuesday != null;
  bool get wednesdayIsSelected => wednesday != null;
  bool get thursdayIsSelected => thursday != null;
  bool get fridayIsSelected => friday != null;
  bool get saturdayIsSelected => saturday != null;
  bool get sundayIsSelected => sunday != null;
  bool get anyDaySelected =>
      mondayIsSelected ||
      tuesdayIsSelected ||
      wednesdayIsSelected ||
      thursdayIsSelected ||
      fridayIsSelected ||
      saturdayIsSelected ||
      sundayIsSelected;
    
  // Shortcut for checking the number of days required
  int get numberOfDays {
    int count = 0;
    if(mondayIsSelected) count++;
    if(tuesdayIsSelected) count++;
    if(wednesdayIsSelected) count++;
    if(thursdayIsSelected) count++;
    if(fridayIsSelected) count++;
    if(saturdayIsSelected) count++;
    if(sundayIsSelected) count++;
    return count;
  }

  DaysConfig({
    this.monday,
    this.tuesday,
    this.wednesday,
    this.thursday,
    this.friday,
    this.saturday,
    this.sunday,
  });

  factory DaysConfig.fromMap(Map<String, dynamic> map) => DaysConfig(
        // FIXME: Should use lowercase days on Firebase
        monday: map['Monday'] != null ? List<DocumentReference>.from(map['Monday']) : null,
        tuesday: map['Tuesday'] != null ? List<DocumentReference>.from(map['Tuesday']) : null,
        wednesday: map['Wednesday'] != null ? List<DocumentReference>.from(map['Wednesday']) : null,
        thursday: map['Thursday'] != null ? List<DocumentReference>.from(map['Thursday']) : null,
        friday: map['Friday'] != null ? List<DocumentReference>.from(map['Friday']) : null,
        saturday: map['Saturday'] != null ? List<DocumentReference>.from(map['Saturday']) : null,
        sunday: map['Sunday'] != null ? List<DocumentReference>.from(map['Sunday']) : null,
      );

  Map<String, dynamic> toMap() {
    Map<String, dynamic> returnMap = new Map<String, dynamic>();

    if (this.mondayIsSelected) returnMap['Monday'] = monday;
    if (this.tuesdayIsSelected) returnMap['Tuesday'] = tuesday;
    if (this.wednesdayIsSelected) returnMap['Wednesday'] = wednesday;
    if (this.thursdayIsSelected) returnMap['Thursday'] = thursday;
    if (this.fridayIsSelected) returnMap['Friday'] = friday;
    if (this.saturdayIsSelected) returnMap['Saturday'] = saturday;
    if (this.sundayIsSelected) returnMap['Sunday'] = sunday;

    return returnMap;
  }
}