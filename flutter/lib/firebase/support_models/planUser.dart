import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mealmonkey/firebase/models/user.dart';
import 'package:meta/meta.dart';

// Make immutable (which means make everything final and don't save anything).
// If anything user related needs to be changed, query the `userReference` and save that.
// --> Cloud Func will trigger --> Plan will update --> Plan will be rebuilt as soon as the changes are done.

// UX Note:
// (Think about showing a loading icon when the user has submitted a change and removing that loading thing once the cloud function goes 'Done!').

class PlanUser {
  final DocumentReference userReference;
  String username;
  String picture;
  String email;
  List<String> fcmToken;

  // Consider putting @require on everything until you run into problems.
  PlanUser({
    @required this.userReference,
    @required this.username,
    @required this.picture,
    @required this.email,
    @required this.fcmToken,
  });

  Future<User> toRealUser() async {
    return User.from(await userReference.get());
  }

  static List<PlanUser> fromMapMany(List<Map<String, dynamic>> planUsersMap) =>
      planUsersMap.map((item) => PlanUser.fromMap(item)).toList();

  factory PlanUser.fromMap(Map<String, dynamic> map) => PlanUser(
        userReference: map['ref'],
        username: map['username'],
        picture: map['picture'],
        email: map['email'],
        fcmToken: List<String>.from(map['fcm_token']),
      );

  factory PlanUser.fromRealUser(User user) => PlanUser(
        userReference: user.reference,
        username: user.username,
        picture: user.picture,
        email: user.email,
        fcmToken: user.fcmToken,
      );

  // NOTE: Probably not required.
  Map<String, dynamic> toMap() => {
        'ref': userReference,
        'username': username,
        'picture': picture,
        'email': email,
        'fcm_token': fcmToken,
      };
}
