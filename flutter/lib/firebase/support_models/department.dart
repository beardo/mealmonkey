import 'package:mealmonkey/firebase/firebase_util.dart';
import 'package:mealmonkey/firebase/support_models/ingredient.dart';

class Department {
  final String name;
  List<Ingredient> ingredients;

  Department({
    this.name,
    this.ingredients,
  }) {
    this.ingredients ??= new List<Ingredient>();
  }

  static getDepartmentIndex(String department) {
    List<String> departments = [
      "Produce",
      "Butchery",
      "Seafood",
      "Grocery",
      "Frozen",
      "Deli",
      "Bakery",
      "Alcohol",
      "International",
      "Dairy",
      "Staple"
    ];

    return departments.indexOf(department);
  }

  static List<Department> fromMapMany(List<Map<String, dynamic>> departmentsMap) =>
      departmentsMap.map((item) => Department.fromMap(item)).toList();

  static int sortDepartments(String a, String b) {
    if (Department.getDepartmentIndex(a) < Department.getDepartmentIndex(b)) {
      return -1;
    } else if (Department.getDepartmentIndex(a) > Department.getDepartmentIndex(b)) {
      return 1;
    }
    return 0;
  }

  factory Department.fromMap(Map<String, dynamic> map) => Department(
        name: map['name'],
        ingredients: Ingredient.fromMapMany(FirebaseUtil.toTypedListMap(map['ingredients'])),
      );

  Map<String, dynamic> toMap() => {
        'name': name,
        'ingredients': ingredients.map((ingredient) => ingredient.toMap()).toList(),
      };
}
