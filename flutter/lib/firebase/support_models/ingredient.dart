import 'package:mealmonkey/firebase/support_models/ingredientStatus.dart';
import 'package:meta/meta.dart';

class Ingredient {
  final String name;
  final double quantity;
  final String unit;
  IngredientStatus checked;

  Ingredient({
    @required this.name,
    @required this.quantity,
    @required this.unit,
    this.checked,
  }) {
    this.checked ??= IngredientStatus.UNKNOWN;
  }

  static List<Ingredient> fromMapMany(
    List<Map<String, dynamic>> ingredientsMap,
  ) =>
      ingredientsMap.map((item) => Ingredient.fromMap(item)).toList();

  factory Ingredient.fromMap(Map<String, dynamic> map) => Ingredient(
        name: map['name'],
        quantity: map['quantity'].toDouble(),
        unit: map['unit'],
        checked: IngredientStatusHelper.fromString(map['checked']),
      );

  Map<String, dynamic> toMap() => {
        'name': name,
        'quantity': quantity,
        'unit': unit,
        'checked': checked.toString(),
      };
}
