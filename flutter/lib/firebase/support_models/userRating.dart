import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:meta/meta.dart';

class UserRating {
  final DocumentReference userReference;
  int rating;

  UserRating({
    @required this.userReference,
    @required this.rating,
  });

  static List<UserRating> fromMapMany(
    List<Map<String, dynamic>> userRatingsMap,
  ) =>
      userRatingsMap.map((item) => UserRating.fromMap(item)).toList();

  factory UserRating.fromMap(Map<String, dynamic> map) => UserRating(
        userReference: map['ref'],
        rating: map['rating'],
      );

  Map<String, dynamic> toMap() => {
        'ref': userReference,
        'rating': rating,
      };
}
