
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mealmonkey/firebase/firebase_util.dart';
import 'package:mealmonkey/firebase/support_models/author.dart';
import 'package:mealmonkey/firebase/support_models/department.dart';
import 'package:mealmonkey/firebase/support_models/userRating.dart';
import 'package:meta/meta.dart';

class Recipe {
  final DocumentReference recipeReference;
  
  final String name;
  final String link;
  final String picture;
  final int time;
  final int difficulty;
  final int servings;
  final double rating;
  final List<UserRating> userRatings;
  final Author author;
  final List<String> labels;
  final List<Department> departments;

  Recipe({
    @required this.recipeReference,
    @required this.name,
    @required this.link,
    @required this.picture,
    @required this.time,
    @required this.difficulty,
    @required this.servings,
    @required this.rating,
    @required this.userRatings,
    @required this.author,
    @required this.labels,
    @required this.departments,
  });

  UserRating getUserRating(DocumentReference userReference) {
    for(UserRating rating in this.userRatings) {
      if(rating.userReference == userReference) {
        return rating;
      }
    }
    return null;
  }

  factory Recipe.fromMap(Map<String, dynamic> map) => Recipe(
    recipeReference: map['ref'],
    name: map['name'],
    link: map['link'],
    picture: map['picture'],
    time: map['time'].toInt(),
    difficulty: map['difficulty'].toInt(),
    servings: map['servings'].toInt(),
    rating: map['rating'].toDouble(),
    userRatings: UserRating.fromMapMany(FirebaseUtil.toTypedListMap(map['user_ratings'])),
    author: Author.fromMap(FirebaseUtil.toTypedMap(map['author'])),
    labels: List<String>.from(map['labels']),
    departments: Department.fromMapMany(FirebaseUtil.toTypedListMap(map['departments'])),
  );

  Map<String, dynamic> toMap() => {
    'ref': recipeReference,
    'name': name,
    'link': link,
    'picture': picture,
    'time': time,
    'difficulty': difficulty,
    'servings': servings,
    'rating': rating,
    'user_ratings': userRatings.map((userRating) => userRating.toMap()).toList(),
    'author': author.toMap(),
    'labels': labels,
    'departments': departments.map((department) => department.toMap()).toList(),
  };
}