import 'package:meta/meta.dart';

class Author {
  final String name;
  final String website;

  Author({
    @required this.name,
    @required this.website,
  });

  factory Author.fromMap(Map<String, dynamic> map) => Author(
        name: map['name'],
        website: map['website'],
      );

  Map<String, dynamic> toMap() => {
        'name': name,
        'website': website,
      };
}
