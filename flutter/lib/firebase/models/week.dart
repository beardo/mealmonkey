import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mealmonkey/firebase/firebase_util.dart';
import 'package:mealmonkey/firebase/models/firestoreModel.dart';
import 'package:mealmonkey/firebase/models/meal.dart';
import 'package:mealmonkey/firebase/support_models/department.dart';
import 'package:mealmonkey/firebase/support_models/ingredient.dart';
import 'package:mealmonkey/firebase/support_models/ingredientStatus.dart';

class Week extends FirestoreModel {
  static final String collectionName = "weeks";
  final DocumentReference reference;

  Timestamp startDate;
  Timestamp endDate;
  List<Department> departments;
  CollectionReference meals;

  Week({
    this.reference,
    this.startDate,
    this.endDate,
    this.departments,
    this.meals,
  }) : super(reference) {
    this.startDate ??= Timestamp.now();
    this.endDate ??= Timestamp.now();
    this.departments ??= new List<Department>();
    if (this.reference != null) {
      meals = this.reference.collection(Meal.collectionName);
    }
  }

  Future<List<Meal>> getMeals() async {
    QuerySnapshot mealQuerySnapshot = await this.meals.getDocuments();
    return mealQuerySnapshot.documents.map((mealSnapshot) => Meal.from(mealSnapshot)).toList();
  }

  Future save() async {
    Map<String, dynamic> updateData = this.toMap();
    await this.reference.updateData(updateData);
  }

  Future delete() async {
    await this.reference.delete();
  }

  void checkIngredient(Ingredient ingredient, int count) {
    for (Department department in this.departments) {
      for (Ingredient departmentIngredient in department.ingredients) {
        if (ingredient.name == departmentIngredient.name) {
          if (count == 0) {
            departmentIngredient.checked = ingredient.checked;
          } else {
            departmentIngredient.checked = IngredientStatus.DASHED;
          }
        }
      }
    }
  }

  factory Week.from(DocumentSnapshot snapshot) => Week(
        reference: snapshot.reference,
        startDate: snapshot.data['start_date'],
        endDate: snapshot.data['end_date'],
        departments: Department.fromMapMany(
          FirebaseUtil.toTypedListMap(
            snapshot.data['departments'],
          ),
        ),
      );

  Map<String, dynamic> toMap() => {
        'departments': departments.map((department) => department.toMap()).toList(),
      };
}
