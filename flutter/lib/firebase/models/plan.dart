import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:mealmonkey/firebase/firebase_util.dart';
import 'package:mealmonkey/firebase/models/firestoreModel.dart';
import 'package:mealmonkey/firebase/models/user.dart';
import 'package:mealmonkey/firebase/models/week.dart';
import 'package:mealmonkey/firebase/support_models/daysConfig.dart';
import 'package:mealmonkey/firebase/support_models/planUser.dart';

class Plan extends FirestoreModel {
  static final String collectionName = "plans";
  final DocumentReference reference;

  String code;
  int budget;
  List<PlanUser> users;
  List<String> labels;
  DaysConfig daysConfig;
  CollectionReference weeks;

  Plan({
    this.reference,
    this.budget,
    this.code,
    this.labels,
    this.daysConfig,
    this.weeks,
    this.users,
  }) : super(reference) {
    // If a field isn't set in Firestore it returns null, the below is shorthand for checking if
    // is null and setting it to a sensible default if so.
    // this.code ??= "ERROR";
    // this.budget ??= 0;
    this.labels ??= new List<String>();
    this.daysConfig ??= new DaysConfig();
    this.users ??= new List<PlanUser>();
    if (this.reference != null) {
      this.weeks = this.reference.collection(Week.collectionName);
    }
  }

  Future save() async {
    await this.reference.updateData(this.toMap());
  }

  Future delete() async {
    await this.reference.delete();
  }

  static Future<Plan> create(User user, Plan plan) async {
    String planCode = FirebaseUtil.randomCode(6);
    DocumentReference newPlanReference =
        Firestore.instance.collection(Plan.collectionName).document(planCode);

    DocumentSnapshot newPlan = await newPlanReference.get();

    if (newPlan.exists) {
      return Plan.create(user, plan);
    } else {
      // Create the new plan
      newPlanReference.setData(plan.toMap());
    }

    // Return the new plan
    Plan test = Plan.from(await newPlanReference.get());
    return test;
  }

  static Future<DocumentSnapshot> checkPlanCode(String code) async {
    DocumentSnapshot plan =
        await Firestore.instance.collection(Plan.collectionName).document(code).get();

    return plan;
  }

  Future<DocumentSnapshot> getCurrentWeek() async {
    // Check end_date first as more efficient
    QuerySnapshot currentWeekQuerySnapshot = await this
        .weeks
        .where('end_date', isGreaterThanOrEqualTo: DateTime.now().toUtc())
        .where('end_date', isLessThanOrEqualTo: DateTime.now().add(Duration(days: 7)).toUtc())
        .getDocuments();

    if(currentWeekQuerySnapshot.documents.length != 1) {
      await FirebaseAuth.instance.signOut();
    }

    return currentWeekQuerySnapshot.documents[0];
  }

  PlanUser getPlanUserFromRef(DocumentReference ref) {
    for (PlanUser planUser in this.users) {
      if (planUser.userReference == ref) {
        return planUser;
      }
    }
    return null;
  }

  void removeUserFromPlan(DocumentReference ref) {
    int i = 0;
    for (PlanUser planUser in this.users) {
      if (planUser.userReference == ref) {
        this.users.removeAt(i);
        break;
      }
      i++;
    }
  }

  void addFCMTokenToPlanUser(DocumentReference ref, String fcmToken) {
    for (PlanUser planUser in this.users) {
      if (planUser.userReference == ref) {
        planUser.fcmToken.add(fcmToken);
      }      
    }
  }

  factory Plan.blankPlan(User user) => Plan(
        budget: null,
        code: null,
        labels: new List<String>(),
        daysConfig: new DaysConfig(),
        users: <PlanUser>[PlanUser.fromRealUser(user)],
      );

  factory Plan.from(DocumentSnapshot document) => Plan(
        reference: document.reference,
        budget: document.data['budget'],
        code: document.documentID,
        labels: List<String>.from(document.data['labels']),
        daysConfig: DaysConfig.fromMap(FirebaseUtil.toTypedMap(document.data['days_config'])),
        users: PlanUser.fromMapMany(FirebaseUtil.toTypedListMap(document.data['users'])),
      );

  Map<String, dynamic> toMap() => {
        'budget': budget,
        'labels': labels,
        'days_config': daysConfig.toMap(),
        'users': users.map((planUser) => planUser.toMap()).toList(),
      };
}
