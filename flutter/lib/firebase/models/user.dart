import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mealmonkey/firebase/models/firestoreModel.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:meta/meta.dart';

class User extends FirestoreModel {
  static final String collectionName = "users";
  final DocumentReference reference;

  // Think about reusing this class for embedded user objects.
  String username;
  // Perhaps replace with ImageProvider.
  String picture;
  String email;
  List<String> fcmToken;
  final Timestamp dateJoined;
  Timestamp lastLogin;
  bool setup;
  bool providerAccount;
  DocumentReference plan;

  User(
      {this.reference,
      this.username,
      this.picture,
      this.email,
      this.fcmToken,
      this.dateJoined,
      this.lastLogin,
      this.setup,
      this.providerAccount,
      this.plan})
      : super(reference) {
    // If a field isn't set in Firestore it returns null, the below is shorthand for checking if
    // is null and setting it to a sensible default if so.
    this.username ??= "None";
    this.email ??= "none";
    this.lastLogin ??= Timestamp.now();
  }

  Future save() async {
    // setData == delete all data and give it this new data
    // updateData == overwrites given data and preserves other fields
    await this.reference.setData(this.toMap());
  }

  Future delete() async {
    await this.reference.delete();
  }

  static Future<User> create({@required User user, String id}) async {
    String newDocumentId = id ?? (await FirebaseAuth.instance.currentUser()).uid;

    DocumentReference newUserDocumentReference =
        Firestore.instance.collection(User.collectionName).document(newDocumentId);

    await newUserDocumentReference.setData(user.toMap());

    FirebaseAnalytics().logSignUp(signUpMethod: "custom");

    return User.from(await newUserDocumentReference.get());
  }

  factory User.initialUser({
    String username,
    String email,
    String picture,
    String fcmToken,
    bool providerAccount,
  }) =>
      User(
        username: username,
        email: email,
        picture: picture,
        fcmToken: [fcmToken],
        dateJoined: Timestamp.now(),
        lastLogin: Timestamp.now(),
        setup: false,
        providerAccount: providerAccount,
      );

  factory User.from(DocumentSnapshot document) => User(
        reference: document.reference,
        username: document.data['username'],
        picture: document.data['picture'],
        email: document.data['email'],
        fcmToken: List<String>.from(document.data['fcm_token']),
        dateJoined: document.data['date_joined'],
        lastLogin: document.data['last_login'],
        setup: document.data['setup'],
        plan: document.data['plan'],
        providerAccount: document.data['provider_account'],
      );

  Map<String, dynamic> toMap() => {
        'username': username,
        'picture': picture,
        'email': email,
        'fcm_token': fcmToken,
        'date_joined': dateJoined ?? DateTime.now(),
        'last_login': lastLogin,
        'setup': setup,
        'plan': plan,
        'provider_account': providerAccount,
      };
}
