import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mealmonkey/firebase/firebase_util.dart';
import 'package:mealmonkey/firebase/models/firestoreModel.dart';
import 'package:mealmonkey/firebase/support_models/department.dart';
import 'package:mealmonkey/firebase/support_models/ingredient.dart';
import 'package:mealmonkey/firebase/support_models/recipe.dart';
import 'package:mealmonkey/firebase/support_models/userRating.dart';

class Meal extends FirestoreModel {
  static final String collectionName = "meals";
  final DocumentReference reference;

  final Timestamp date;
  final List<DocumentReference> users;
  final double price;
  final int totalTimesCooked;
  Recipe recipe;

  Meal({
    this.reference,
    this.date,
    this.users,
    this.price,
    this.totalTimesCooked,
    this.recipe,
  }) : super(reference);

  Future save() async {
    this.reference.updateData(this.toMap());
  }

  Future delete() async {
    this.reference.delete();
  }

  bool isUserAssigned(DocumentReference ref) {
    return this.users.contains(ref);
  }

  void addUser(DocumentReference ref) {
    this.users.add(ref);
  }

  void removeUser(DocumentReference ref) {
    this.users.remove(ref);
  }

  Future<void> addUserRating(DocumentReference userReference) async {
    DocumentSnapshot recipeHistory = await userReference
        .collection('histories')
        .document(this.recipe.recipeReference.documentID)
        .get();

    // Check if user has history object for recipe
    if (recipeHistory.exists) {
      // It does it exist, use data to create new user rating
      UserRating newUserRating = UserRating(
        rating: recipeHistory.data['rating'],
        userReference: userReference,
      );
      this.recipe.userRatings.add(newUserRating);
    } else {
      // It doesn't exist so create it
      await recipeHistory.reference.setData({
        'rating': 0,
        'ref': this.recipe.recipeReference,
        'total_times_cooked': 0,
      });

      // Store new blank rating in user ratings
      UserRating newUserRating = UserRating(
        rating: 0,
        userReference: userReference,
      );
      this.recipe.userRatings.add(newUserRating);
    }
  }

  Future<void> removeUserRating(DocumentReference userReference) async {
    // Get the users history for the recipe associated with the meal
    DocumentSnapshot recipeHistory = await userReference
        .collection('histories')
        .document(this.recipe.recipeReference.documentID)
        .get();

    // Check to see if the user has never cooked the meal
    if (recipeHistory.data['total_times_cooked'] == 0) {
      await recipeHistory.reference.delete();
    }

    // Remove the users ratings from the recipes
    int i = 0;
    for (UserRating userRating in this.recipe.userRatings) {
      if (userRating.userReference == userReference) {
        this.recipe.userRatings.removeAt(i);
        break;
      }
      i++;
    }
  }

  void checkMealIngredient(Ingredient ingredient) {
    List<Department> recipeDepartments = this.recipe.departments;

    for (Department department in recipeDepartments) {
      for (Ingredient departmentIngredient in department.ingredients) {
        if (ingredient.name == departmentIngredient.name) {
          departmentIngredient.checked = ingredient.checked;
        }
      }
    }
  }

  int checkMainIngredient(Ingredient ingredient) {
    int count = 0;

    List<Department> recipeDepartments = this.recipe.departments;

    for (Department department in recipeDepartments) {
      for (Ingredient departmentIngredient in department.ingredients) {
        if (ingredient.name == departmentIngredient.name) {
          if (departmentIngredient.checked != ingredient.checked) {
            count++;
          }
        }
      }
    }

    return count;
  }

  factory Meal.from(DocumentSnapshot snapshot) => Meal(
        reference: snapshot.reference,
        date: snapshot.data['date'],
        users: List<DocumentReference>.from(
          snapshot.data['users'],
        ),
        price: snapshot.data['price'].toDouble(),
        totalTimesCooked: snapshot.data['total_times_cooked'].toInt(),
        recipe: Recipe.fromMap(FirebaseUtil.toTypedMap(snapshot.data['recipe'])),
      );

  Map<String, dynamic> toMap() => {
        'recipe': recipe.toMap(),
        'users': users,
      };
}
