import 'dart:math';

class FirebaseUtil {
  static Map<String, dynamic> toTypedMap(Map<dynamic, dynamic> map) {
    return Map<String, dynamic>.from(map);
  }

  static List<Map<String, dynamic>> toTypedListMap(List<dynamic> listMap) {
    List<Map<String, dynamic>> returnList = new List<Map<String, dynamic>>();

    for (Map<dynamic, dynamic> map in listMap) {
      returnList.add(FirebaseUtil.toTypedMap(map));
    }

    return returnList;
  }
  

  static String randomCode(int length) {
    const chars = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789";
    Random rnd = new Random(new DateTime.now().millisecondsSinceEpoch);
    String result = "";
    for (var i = 0; i < length; i++) {
      result += chars[rnd.nextInt(chars.length)];
    }
    return result;
  }
}
