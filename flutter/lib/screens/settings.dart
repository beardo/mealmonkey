import 'dart:async';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:mealmonkey/firebase/auth/auth.dart';
import 'package:mealmonkey/firebase/models/plan.dart';
import 'package:mealmonkey/firebase/models/user.dart';
import 'package:mealmonkey/screens/settings/account.dart';
import 'package:mealmonkey/screens/settings/credits.dart';
import 'package:mealmonkey/screens/settings/plan.dart';
import 'package:mealmonkey/screens/util/colours.dart';
import 'package:mealmonkey/screens/util/designElements.dart';
import 'package:mealmonkey/screens/util/flushbar.dart';
import 'package:mealmonkey/screens/util/fonts.dart';
import 'package:mealmonkey/state/action.dart';
import 'package:mealmonkey/state/appstate.dart';
import 'package:mealmonkey/util.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';

class SettingsScreen extends StatefulWidget {
  @override
  createState() => new SettingsScreenState();
}

class SettingsScreenState extends State<SettingsScreen> {
  Future<File> getImage() async {
    return await ImagePicker.pickImage(source: ImageSource.gallery);
  }

  /// Builds the recipe screen using the build method with [context].
  /// Documentation can be found here:
  /// https://docs.flutter.io/flutter/widgets/State/build.html
  @override
  Widget build(BuildContext context) {
    final FlushbarUtil flushbarHelper = FlushbarUtil.getInstance(StoreProvider.of(context));

    return new StoreConnector<AppState, _ViewModel>(
      converter: (store) => _ViewModel(
            logout: () => store.dispatch(
                  LogoutUserAction(),
                ),
            updateUser: (user) => store.dispatch(
                  UpdateUserAction(user: user),
                ),
            updatePlan: (plan) => store.dispatch(
                  UpdatePlanAction(plan: plan),
                ),
            user: store.state.user,
            plan: store.state.plan,
          ),
      builder: (context, viewModel) => Scaffold(
            appBar: AppBar(
              toolbarOpacity: 0.0,
              automaticallyImplyLeading: false,
              backgroundColor: MealMonkeyColours.primary,
              centerTitle: false,
              elevation: 0.0,
              title: new FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  'Settings',
                  style: MealMonkeyTextStyles.mainBold(Colors.white, 28.0),
                ),
              ),
            ),
            body: viewModel.user == null
                ? new Container()
                : new Container(
                    decoration: new BoxDecoration(
                      image: new DecorationImage(
                        image: new AssetImage("assets/images/background_graphics.png"),
                        fit: BoxFit.fill,
                      ),
                    ),
                    child: ListView(
                      padding: EdgeInsets.all(Util.pageWidthPadding),
                      children: <Widget>[
                        _userCard(context, viewModel, flushbarHelper),
                        _settingsCard(context, viewModel),
                      ],
                    ),
                  ),
          ),
    );
  }

  /// Returns a [GestureDetector] containing a list tile with the users information on it.
  /// On tap it will navigate to the user account settings page.
  Widget _userCard(
    BuildContext context,
    _ViewModel viewModel,
    FlushbarUtil flushbarHelper,
  ) {
    return GestureDetector(
      child: new Container(
        height: 115.0,
        margin: EdgeInsets.only(bottom: 5.0),
        decoration: DesignUtil.boxDecoration,
        child: new Card(
          elevation: 0.0,
          child: new Center(
            child: ListTile(
              leading: Container(
                padding: EdgeInsets.only(left: 0.0, top: 10.0, bottom: 10.0),
                child: new RawMaterialButton(
                  shape: CircleBorder(),
                  child: new Stack(
                    children: <Widget>[
                      viewModel.user.picture == null
                          ? new CircleAvatar(
                              backgroundColor: MealMonkeyColours.primary,
                              child: new Text(
                                viewModel.user.username[0].toUpperCase(),
                                style: MealMonkeyTextStyles.mainBold(Colors.white, 30.0),
                              ),
                              radius: 40.0,
                            )
                          : new CircleAvatar(
                              backgroundImage:
                                  new CachedNetworkImageProvider(viewModel.user.picture),
                              backgroundColor: Colors.grey,
                              radius: 40.0,
                            ),
                      new Positioned(
                        bottom: 0.0,
                        child: new Container(
                          height: 30.0,
                          width: 30.0,
                          decoration: new BoxDecoration(
                            borderRadius: new BorderRadius.all(const Radius.circular(100.0)),
                            color: Color.fromRGBO(0, 0, 0, 0.4),
                          ),
                          child: new Icon(
                            Icons.camera_alt,
                            size: 20.0,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                  onPressed: () async {
                    try {
                      // Get the file
                      File profilePicture = await getImage();
                      StorageReference firebaseStorageRef = FirebaseStorage.instance.ref().child(
                          viewModel.user.fcmToken[0].substring(
                              viewModel.user.fcmToken[0].length ~/ 2,
                              viewModel.user.fcmToken[0].length - 1));

                      if (profilePicture != null) {
                        // Upload the file
                        flushbarHelper
                            .create(
                              type: FlushbarType.INFO,
                              message: "Updating your profile picture.",
                              flushbarDuration: FlushbarDuration.UNLIMITED,
                            )
                            .show(context);
                        StorageUploadTask uploadTask = firebaseStorageRef.putFile(profilePicture);

                        await uploadTask.onComplete;

                        // Get the link to the file
                        String pictureUrl = (await firebaseStorageRef.getDownloadURL()).toString();

                        // Set the user profile picture to the uploaded picture
                        viewModel.user.picture = pictureUrl;
                        viewModel.updateUser(viewModel.user);

                        // Set the plan user picture to the uploaded picture
                        viewModel.plan.users.forEach(
                          (planUser) {
                            if (planUser.userReference == viewModel.user.reference) {
                              planUser.picture = pictureUrl;
                            }
                          },
                        );
                        viewModel.updatePlan(viewModel.plan);

                        flushbarHelper.dismissAll();
                      }
                    } catch (e) {
                      flushbarHelper.dismissAll();

                      flushbarHelper
                          .create(
                            type: FlushbarType.ERROR,
                            message:
                                "An error occured while trying to update your profile picture.",
                            flushbarDuration: FlushbarDuration.MEDIUM,
                          )
                          .show(context);
                    }
                  },
                ),
              ),
              title: new Text(viewModel.user.username,
                  style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.heading1, 21.0)),
              subtitle: FittedBox(
                alignment: Alignment.centerLeft,
                fit: BoxFit.scaleDown,
                child: new Text(viewModel.user.email,
                    style: MealMonkeyTextStyles.mainRegular(MealMonkeyColours.subtext, 12.0)),
              ),
              trailing: new Icon(const IconData(0xe817, fontFamily: 'IonIcons')),
            ),
          ),
        ),
      ),
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) => AccountSettings()));
      },
    );
  }

  /// Returns a [Container] with navigation links to the credits and plan pages, and a logout
  /// function.
  Widget _settingsCard(BuildContext context, _ViewModel viewModel) {
    return Container(
      decoration: DesignUtil.boxDecoration,
      margin: EdgeInsets.only(top: 10.0),
      child: new Card(
        color: Colors.white,
        elevation: 0.0,
        child: new Column(
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                border: new Border(
                  bottom: new BorderSide(color: MealMonkeyColours.divider),
                ),
              ),
              // Plan settings page
              child: new ListTile(
                title: new Text("Plan",
                    style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.heading1, 17.0)),
                subtitle: new Text("Change meal plan settings",
                    style: MealMonkeyTextStyles.mainRegular(MealMonkeyColours.subtext, 12.0)),
                trailing: new Icon(const IconData(0xe817, fontFamily: 'IonIcons')),
                onTap: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => UserPlanScreen()));
                },
              ),
            ),
            new Container(
              decoration: new BoxDecoration(
                border: new Border(
                  bottom: new BorderSide(color: MealMonkeyColours.divider),
                ),
              ),
              // Credits settings page
              child: new ListTile(
                title: new Text("Credits",
                    style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.heading1, 17.0)),
                subtitle: new Text("View Meal Monkey credits",
                    style: MealMonkeyTextStyles.mainRegular(MealMonkeyColours.subtext, 12.0)),
                trailing: new Icon(const IconData(0xe817, fontFamily: 'IonIcons')),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => CreditsScreen()));
                },
              ),
            ),
            new Container(
              decoration: new BoxDecoration(
                border: new Border(
                  bottom: new BorderSide(color: MealMonkeyColours.divider),
                ),
              ),
              // Logout tile.
              child: new ListTile(
                title: new Text(
                  "Logout",
                  style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.heading1, 17.0),
                ),
                trailing: new Icon(const IconData(0xe817, fontFamily: 'IonIcons')),
                onTap: () async {
                  await _logoutConfirmation(context, viewModel);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// Returns a [showDialog] prompting the user to confirm if they want to logout. Calls [_logout]
  /// if confirmed.
  Future<Null> _logoutConfirmation(BuildContext context, _ViewModel viewModel) async {
    return showDialog<Null>(
      context: context,
      builder: (BuildContext context) {
        return new AlertDialog(
          title: new Text('Are you sure you want to logout?'),
          actions: <Widget>[
            new FlatButton(
              child: new Text(
                "Cancel",
                style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.primary, 17.0),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text(
                'Logout',
                style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.primary, 17.0),
              ),
              onPressed: () {
                _logout(context, viewModel);
              },
            ),
          ],
        );
      },
    );
  }

  /// Naviages the user to the login screen
  _logout(BuildContext context, _ViewModel viewModel) async {
    // Sign the user out of Firebase
    FirebaseAuth.instance.signOut();

    // Signout the Google user
    if (await Auth.googleSignIn.isSignedIn()) {
      Auth.googleSignIn.signOut();
    }

    // Signout the Facebook user
    if (await Auth.facebookSignIn.isLoggedIn) {
      Auth.facebookSignIn.logOut();
    }

    // Reset the Redux store to the state of the redux
    viewModel.logout();

    // Send the user back to the login page
    Navigator.of(context).pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
  }
}

typedef Logout();
typedef UpdateUser(User user);
typedef UpdatePlan(Plan plan);

/// Class used by the store connector to connect firebase and
/// the local state of the application.
class _ViewModel {
  final Logout logout;
  final UpdateUser updateUser;
  final UpdatePlan updatePlan;
  User user;
  Plan plan;

  _ViewModel({this.logout, this.updateUser, this.updatePlan, this.user, this.plan});
}
