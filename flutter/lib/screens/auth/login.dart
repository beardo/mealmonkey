import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:mealmonkey/firebase/auth/auth.dart';
import 'package:mealmonkey/firebase/auth/types.dart';
import 'package:mealmonkey/main.dart';
import 'package:mealmonkey/screens/auth/forgot.dart';
import 'package:mealmonkey/screens/auth/register.dart';
import 'package:mealmonkey/screens/setup/start.dart';
import 'package:mealmonkey/screens/util/colours.dart';
import 'package:mealmonkey/screens/util/designElements.dart';
import 'package:mealmonkey/screens/util/flushbar.dart';
import 'package:mealmonkey/screens/util/fonts.dart';
import 'package:mealmonkey/state/action.dart';
import 'package:mealmonkey/state/appstate.dart';

class LoginScreen extends StatefulWidget {
  @override
  createState() => new LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _enableSubmit;
  bool _emailFilled;
  bool _passwordFilled;
  StreamController<bool> _enabledStreamController;

  TextEditingController emailTextFieldController = new TextEditingController();
  TextEditingController passwordTextFieldController = new TextEditingController();

  @override
  void initState() {
    _enableSubmit = true;
    _emailFilled = false;
    _passwordFilled = false;
    _enabledStreamController = new StreamController();
    super.initState();
  }

  // Form fields
  String _email;
  String _password;
  Icon person = new Icon(const IconData(0xe8d7, fontFamily: "IonIcons"));
  Icon lock = new Icon(const IconData(0xe8b1, fontFamily: "IonIcons"));

  /// Authenticates the user and sets the users viewModel on success.
  _submit(BuildContext context, AuthViewModel viewModel, AuthType type) async {
    final FlushbarUtil flushbarHelper = FlushbarUtil.getInstance(StoreProvider.of(context));
    final form = _formKey.currentState;

    FocusScope.of(context).requestFocus(new FocusNode());

    try {
      AuthAction actionPerformed;

      if (type == AuthType.google) {
        // Authenticate the user
        actionPerformed = await Auth.authenticateUser(
          type: AuthType.google,
          action: AuthAction.login,
          viewModel: viewModel,
          context: context,
        );
      } else if (type == AuthType.facebook) {
        actionPerformed = await Auth.authenticateUser(
          type: AuthType.facebook,
          action: AuthAction.login,
          viewModel: viewModel,
          context: context,
        );
      } else if (type == AuthType.custom) {
        if (form.validate()) {
          form.save();

          viewModel.setLoading(true);

          // Authenticate the user
          actionPerformed = await Auth.authenticateUser(
              email: _email,
              password: _password,
              type: AuthType.custom,
              action: AuthAction.login,
              viewModel: viewModel,
              context: context);

          // Disable loading
          viewModel.setLoading(false);
        }
      }

      // Go to the main page now that login is complete
      if (actionPerformed == AuthAction.register) {
        Navigator.pushReplacement(
          context,
          new MaterialPageRoute(builder: (context) => new SetupStart()),
        );
      } else if (actionPerformed == AuthAction.login) {
        Navigator.pushReplacement(
          context,
          new MaterialPageRoute(builder: (context) => new MainPage()),
        );
      }
    } catch (e) {
      // Show the error message in a snackbar
      try {
        // Show the error message in a snackbar
        await flushbarHelper.dismissAll();

        String errorMessage = e.message.toString().startsWith('FIR') ? e.details : e.message;

        flushbarHelper
            .create(
              type: FlushbarType.WARNING,
              message: errorMessage,
              flushbarDuration: FlushbarDuration.SHORT,
            )
            .show(context);
      } catch (e) {
        // Show generic message if the error has no error message
        String errorMessage = 'An error occured during login.';

        flushbarHelper
            .create(
              type: FlushbarType.ERROR,
              message: errorMessage,
              flushbarDuration: FlushbarDuration.SHORT,
            )
            .show(context);
      }

      viewModel.setLoading(false);
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  ///Describes the part of the user interface represented by this widget.
  ///More information can be found here: https://docs.flutter.io/flutter/widgets/State/build.html
  Widget build(BuildContext context) {
    Size phoneSize = MediaQuery.of(context).size;
    double verticalPadding = phoneSize.height * 0.04;

    setState(() {
      emailTextFieldController.addListener(() {
        emailTextFieldController.text.isEmpty ? _emailFilled = false : _emailFilled = true;
        _emailFilled && _passwordFilled ? _enabledStreamController.add(true) : _enabledStreamController.add(false);
      });

      passwordTextFieldController.addListener(() {
        passwordTextFieldController.text.isEmpty ? _passwordFilled = false : _passwordFilled = true;
        _emailFilled && _passwordFilled ? _enabledStreamController.add(true) : _enabledStreamController.add(false);
      });
    });

    TextFormField emailTextField = new TextFormField(
      controller: emailTextFieldController,
      keyboardType: TextInputType.text,
      decoration: const InputDecoration(
        border: InputBorder.none,
        icon: Icon(Icons.email),
        hintText: 'Email',
      ),
      validator: (val) {
        if (val.isEmpty) return 'Email can\'t be empty.';
        return null;
      },
      onSaved: (val) => _email = val,
      autocorrect: false,
    );

    TextFormField passwordTextField = new TextFormField(
      controller: passwordTextFieldController,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        border: InputBorder.none,
        icon: Icon(Icons.lock),
        hintText: 'Password',
        labelStyle: MealMonkeyTextStyles.textFieldStyle,
      ),
      validator: (val) {
        if (val.isEmpty) return 'Password can\'t be empty.';
        return null;
      },
      onSaved: (val) => _password = val,
      obscureText: true,
    );

    return StoreConnector<AppState, AuthViewModel>(
      converter: (store) => AuthViewModel(
            setUser: (user, action) {
              if (action == AuthAction.login) {
                store.dispatch(UpdateUserAction(user: user));
              } else if (action == AuthAction.register) {
                store.dispatch(UpdateUserAction(user: user));
              }
            },
            setPlan: (plan, action) {
              if (action == AuthAction.login) {
                store.dispatch(UpdatePlanAction(plan: plan));
              } else if (action == AuthAction.register) {
                store.dispatch(UpdatePlanAction(plan: plan));
              }
            },
            setWeek: (week) => store.dispatch(
                  UpdateLocalWeekAction(week: week),
                ),
            setMeals: (meals) => store.dispatch(
                  UpdateLocalMealsAction(meals: meals),
                ),
            setLoading: (loading) => store.dispatch(
                  SetLoadingAction(loading: loading),
                ),
            user: store.state.user,
            plan: store.state.plan,
            loading: store.state.loading,
          ),
      builder: (context, viewModel) => Scaffold(
            key: _scaffoldKey,
            resizeToAvoidBottomPadding: true,
            body: Stack(children: <Widget>[
              Positioned(
                top: 0.0,
                child: Image.asset(
                  'assets/images/background_graphics.png',
                  fit: BoxFit.fill,
                  height: phoneSize.height,
                  width: phoneSize.width,
                ),
              ),
              new ListView(
                children: <Widget>[
                  new Center(
                    child: new Container(
                      margin: EdgeInsets.only(top: 10.0),
                      height: phoneSize.height * 0.20,
                      child: new Image.asset(
                        'assets/images/logo.png',
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  new Container(
                    padding: EdgeInsets.fromLTRB(0.0, verticalPadding, 0.0, verticalPadding / 2),
                    child: new Text(
                      "Sign In",
                      textAlign: TextAlign.center,
                      style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.darkGrey, 38.0),
                    ),
                  ),
                  new Form(
                    key: this._formKey,
                    child: new Column(
                      children: <Widget>[
                        DesignUtil.textFieldGenerator(
                          tff: emailTextField,
                          context: context,
                        ),
                        DesignUtil.textFieldGenerator(
                          tff: passwordTextField,
                          context: context,
                        ),
                        //login button
                        new Center(
                          child: StreamBuilder(
                            stream: _enabledStreamController.stream,
                            initialData: false,
                            builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
                              return new Container(
                                // width: phoneSize.width - Util.pageWidthPadding,
                                width: MediaQuery.of(context).size.width * 0.80,
                                margin: EdgeInsets.only(top: verticalPadding / 2),
                                child: new RaisedButton(
                                  key: new Key("login"),
                                  padding: EdgeInsets.only(top: 13.0, bottom: 13.0),
                                  elevation: 0.0,
                                  color: snapshot.data && _enableSubmit
                                      ? MealMonkeyColours.primary
                                      : MealMonkeyColours.greyButton,
                                  child: new Text(
                                    'LOGIN',
                                    textAlign: TextAlign.center,
                                    style: MealMonkeyTextStyles.mainBold(Colors.white, 18.0),
                                  ),
                                  onPressed: () async {
                                    if (snapshot.data && _enableSubmit) {
                                      _enableSubmit = false;
                                      await _submit(context, viewModel, AuthType.custom);
                                      _enableSubmit = true;
                                    }
                                  },
                                ),
                              );
                            },
                          ),
                        ),
                        new Container(
                          padding: EdgeInsets.only(top: verticalPadding),
                          child: new Center(
                            child: new Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                new Container(
                                  height: 45.0,
                                  margin: EdgeInsets.only(right: 10.0),
                                  child: new InkWell(
                                    child: new Image.asset(
                                      'assets/images/google.png',
                                      fit: BoxFit.contain,
                                    ),
                                    onTap: () {
                                      _submit(context, viewModel, AuthType.google);
                                    },
                                  ),
                                ),
                                new Container(width: 10.0),
                                new Container(
                                  height: 45.0,
                                  child: new InkWell(
                                    child: new Image.asset(
                                      'assets/images/fb.png',
                                      fit: BoxFit.contain,
                                    ),
                                    onTap: () {
                                      _submit(context, viewModel, AuthType.facebook);
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Center(
                          child: new Container(
                            margin: EdgeInsets.only(top: 10.0),
                            padding: EdgeInsets.only(top: verticalPadding * 1.4),
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                new Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new Text(
                                      "New to Meal Monkey?",
                                      style: MealMonkeyTextStyles.mainRegular(
                                          MealMonkeyColours.heading2, 18.0),
                                    ),
                                    new FlatButton(
                                      padding: EdgeInsets.only(right: 0.0),
                                      child: new Text(
                                        "Sign Up",
                                        style: MealMonkeyTextStyles.mainRegular(
                                            MealMonkeyColours.primary, 18.0),
                                      ),
                                      onPressed: () {
                                        Navigator.pushAndRemoveUntil(
                                            context,
                                            PageRouteBuilder(
                                              pageBuilder: (BuildContext context,
                                                  Animation<double> animation,
                                                  Animation<double> secondaryAnimation) {
                                                return RegisterScreen();
                                              },
                                              transitionsBuilder: (BuildContext context,
                                                  Animation<double> animation,
                                                  Animation<double> secondaryAnimation,
                                                  Widget child) {
                                                return SlideTransition(
                                                  position: new Tween<Offset>(
                                                    begin: const Offset(-1.0, 0.0),
                                                    end: Offset.zero,
                                                  ).animate(animation),
                                                  child: new SlideTransition(
                                                    position: new Tween<Offset>(
                                                      begin: Offset.zero,
                                                      end: const Offset(-1.0, 0.0),
                                                    ).animate(secondaryAnimation),
                                                    child: child,
                                                  ),
                                                );
                                              },
                                            ),
                                            (_) => false);
                                      },
                                    )
                                  ],
                                ),
                                new FlatButton(
                                  padding: EdgeInsets.only(top: 0.0),
                                  child: new Text(
                                    "Fogot Password",
                                    style: MealMonkeyTextStyles.mainRegular(
                                        MealMonkeyColours.primary, 18.0),
                                  ),
                                  onPressed: () {
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (context) => ForgotScreen()));
                                  },
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ]),
          ),
    );
  }
}
