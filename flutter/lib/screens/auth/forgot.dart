import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:mealmonkey/firebase/auth/auth.dart';
import 'package:mealmonkey/firebase/auth/types.dart';
import 'package:mealmonkey/screens/util/colours.dart';
import 'package:mealmonkey/screens/util/designElements.dart';
import 'package:mealmonkey/screens/util/flushbar.dart';
import 'package:mealmonkey/screens/util/fonts.dart';
import 'package:mealmonkey/state/action.dart';
import 'package:mealmonkey/state/appstate.dart';

class ForgotScreen extends StatefulWidget {
  @override
  createState() => new ForgotScreenState();
}

class ForgotScreenState extends State<ForgotScreen> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final TextEditingController _emailController = new TextEditingController();

  bool _submitEnabled;
  bool _emailFilled;
  StreamController<bool> _submitStreamController;

  @override
  void initState() {
    _submitEnabled = true;
    _emailFilled = false;
    _submitStreamController = new StreamController();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  // Form fields
  List<String> _formFields = new List(2);
  Icon person = new Icon(const IconData(0xe8d7, fontFamily: "IonIcons"));
  Icon lock = new Icon(const IconData(0xe8b1, fontFamily: "IonIcons"));

  _submit(BuildContext context, AuthViewModel viewModel, AuthType type) async {
    // Flushbar
    final FlushbarUtil flushbarHelper = FlushbarUtil.getInstance(StoreProvider.of(context));

    final form = _formKey.currentState;

    try {
      if (form.validate()) {
        form.save();

        String _email = _formFields[0];

        List<String> providers = await FirebaseAuth.instance.fetchProvidersForEmail(email: _email);

        if (providers.isEmpty) {
          flushbarHelper
              .create(
                type: FlushbarType.WARNING,
                message: "No user's found with that email.",
                flushbarDuration: FlushbarDuration.MEDIUM,
              )
              .show(context);
        } else {
          if (providers.contains('password')) {
            await FirebaseAuth.instance.sendPasswordResetEmail(email: _email);

            Flushbar flushbar = flushbarHelper.create(
              type: FlushbarType.INFO,
              message: "A password reset email has been sent to your email address.",
              flushbarDuration: FlushbarDuration.MEDIUM,
            );

            flushbar
              ..onStatusChanged = (FlushbarStatus status) {
                if (status == FlushbarStatus.DISMISSED) {
                  Navigator.pushNamedAndRemoveUntil(context, '/login', (_) => false);
                }
              }
              ..show(context);
          } else {
            String providerName;

            if (providers.contains('google.com')) {
              providerName = "Google";
            } else if (providers.contains('facebook.com')) {
              providerName = "Facebook";
            }

            Flushbar flushbar = flushbarHelper.create(
              type: FlushbarType.INFO,
              message: _email +
                  " was registered using " +
                  providerName +
                  ". Please use the " +
                  providerName +
                  " button to login.",
              flushbarDuration: FlushbarDuration.MEDIUM,
            );

            flushbar
              ..onStatusChanged = (FlushbarStatus status) {
                if (status == FlushbarStatus.DISMISSED) {
                  Navigator.pushNamedAndRemoveUntil(context, '/login', (_) => false);
                }
              }
              ..show(context);
          }
        }
      }
    } catch (e) {
      try {
        // Show the error message in a snackbar
        await flushbarHelper.dismissAll();

        flushbarHelper
            .create(
              type: FlushbarType.WARNING,
              message: e.message,
              flushbarDuration: FlushbarDuration.SHORT,
            )
            .show(context);
      } catch (e) {
        // Show generic message if the error has no error message
        String errorMessage = 'An error occured during password reset.';

        flushbarHelper
            .create(
              type: FlushbarType.ERROR,
              message: errorMessage,
              flushbarDuration: FlushbarDuration.SHORT,
            )
            .show(context);
      }

      viewModel.setLoading(false);
    }
  }

  ///Describes the part of the user interface represented by this widget.
  ///More information can be found here: https://docs.flutter.io/flutter/widgets/State/build.html
  Widget build(BuildContext context) {
    Size phoneSize = MediaQuery.of(context).size;
    double verticalPadding = phoneSize.height * 0.04;

    setState(() {
      _emailController.addListener(() {
        _emailController.text.isEmpty ? _emailFilled = false : _emailFilled = true;
        _emailFilled ? _submitStreamController.add(true) : _submitStreamController.add(false);
      });
    });

    TextFormField emailTextField = new TextFormField(
      controller: _emailController,
      keyboardType: TextInputType.text,
      decoration: const InputDecoration(
        border: InputBorder.none,
        icon: Icon(Icons.email),
        hintText: 'Email',
      ),
      validator: (val) {
        if (val.isEmpty) return 'Email can\'t be empty.';

        return null;
      },
      onSaved: (val) => _formFields[0] = val,
      autocorrect: false,
    );

    return StoreConnector<AppState, AuthViewModel>(
      converter: (store) => AuthViewModel(
            setLoading: (loading) => store.dispatch(
                  SetLoadingAction(loading: loading),
                ),
            loading: store.state.loading,
          ),
      builder: (context, viewModel) => Scaffold(
            resizeToAvoidBottomPadding: false,
            key: _scaffoldKey,
            body: new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage("assets/images/background_graphics.png"),
                  fit: BoxFit.fill,
                ),
              ),
              child: new ListView(
                children: <Widget>[
                  new Center(
                    child: new Container(
                      margin: EdgeInsets.only(top: 10.0),
                      height: phoneSize.height * 0.20,
                      child: new Image.asset(
                        'assets/images/logo.png',
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  new Container(
                    padding: EdgeInsets.fromLTRB(0.0, verticalPadding, 0.0, verticalPadding / 2),
                    child: new Text(
                      "Forgot",
                      textAlign: TextAlign.center,
                      style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.darkGrey, 38.0),
                    ),
                  ),
                  new Form(
                    key: this._formKey,
                    child: new Column(
                      children: <Widget>[
                        new Column(
                          children: <Widget>[
                            DesignUtil.textFieldGenerator(
                              tff: emailTextField,
                              context: context,
                            ),
                            //login button
                            new Center(
                              child: StreamBuilder(
                                initialData: false,
                                stream: _submitStreamController.stream,
                                builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
                                  return new Container(
                                    width: phoneSize.width * 0.80,
                                    margin: EdgeInsets.only(top: verticalPadding),
                                    child: new RaisedButton(
                                      key: new Key("login"),
                                      padding: EdgeInsets.only(top: 13.0, bottom: 13.0),
                                      elevation: 0.0,
                                      color: snapshot.data && _submitEnabled
                                          ? MealMonkeyColours.primary
                                          : MealMonkeyColours.greyButton,
                                      child: new Text(
                                        'RESET PASSWORD',
                                        style: MealMonkeyTextStyles.mainBold(Colors.white, 18.0),
                                      ),
                                      onPressed: () async {
                                        if (snapshot.data && _submitEnabled) {
                                          _submitEnabled = false;
                                          await _submit(context, viewModel, AuthType.custom);
                                          _submitEnabled = true;
                                        }
                                      },
                                    ),
                                  );
                                },
                              ),
                            ),
                            new Center(
                              child: new Container(
                                padding: EdgeInsets.only(top: verticalPadding * 1.2),
                                child: new Center(
                                  child: new Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      new Text(
                                        "Want to login?",
                                        style: MealMonkeyTextStyles.mainRegular(
                                            MealMonkeyColours.heading2, 18.0),
                                      ),
                                      new FlatButton(
                                        padding: EdgeInsets.only(right: 0.0),
                                        child: new Text(
                                          "Log In",
                                          style: MealMonkeyTextStyles.mainRegular(
                                              MealMonkeyColours.primary, 18.0),
                                        ),
                                        onPressed: () {
                                          Navigator.pushNamedAndRemoveUntil(
                                              context, '/login', (_) => false);
                                        },
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        viewModel.loading
                            ? new Container(
                                color: Colors.white,
                                width: MediaQuery.of(context).size.width,
                                height: MediaQuery.of(context).size.height,
                                child: new Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 30.0, vertical: verticalPadding),
                                  child: new Image(
                                    image: new AssetImage("assets/images/giphy.gif"),
                                  ),
                                ),
                              )
                            : new Container(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
    );
  }

  Widget textFields(double ypos, String type, int index, Icon icon, Size size) {
    return new Container(
      padding: EdgeInsets.only(top: ypos),
      child: new Center(
        child: new Container(
          width: size.width * 0.8,
          decoration: new BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5.0),
              border: new Border.all(color: MealMonkeyColours.textField)),
          child: new ListTile(
            leading: icon,
            title: new TextFormField(
              keyboardType: TextInputType.text,
              obscureText: type == "Password",
              decoration: new InputDecoration(
                  labelText: type,
                  labelStyle: MealMonkeyTextStyles.textFieldStyle,
                  filled: true,
                  isDense: true,
                  fillColor: Color.fromRGBO(255, 255, 255, 1.0),
                  border: InputBorder.none),
              validator: (val) {
                if (val.isEmpty) {
                  return type + ' can\'t be empty.';
                }

                return null;
              },
              onSaved: (val) => _formFields[index] = val,
              autocorrect: false,
            ),
          ),
        ),
      ),
    );
  }
}
