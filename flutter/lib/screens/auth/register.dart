import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mealmonkey/firebase/auth/auth.dart';
import 'package:mealmonkey/firebase/auth/types.dart';
import 'package:mealmonkey/main.dart';
import 'package:mealmonkey/screens/auth/login.dart';
import 'package:mealmonkey/screens/setup/start.dart';
import 'package:mealmonkey/screens/util/colours.dart';
import 'package:mealmonkey/screens/util/designElements.dart';
import 'package:mealmonkey/screens/util/flushbar.dart';
import 'package:mealmonkey/screens/util/fonts.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:mealmonkey/state/action.dart';
import 'package:mealmonkey/state/appstate.dart';
import 'dart:async';

/// Register.dart
/// Represents the registeration page/create account page of the app.
class RegisterScreen extends StatefulWidget {
  @override
  createState() => new RegisterScreenState();
}

class RegisterScreenState extends State<RegisterScreen> {
  Icon person = new Icon(const IconData(0xe8d7, fontFamily: "IonIcons"));
  Icon lock = new Icon(const IconData(0xe8b1, fontFamily: "IonIcons"));
  Icon mail = new Icon(const IconData(0xea30, fontFamily: "IonIcons"));

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  // Controllers for text fields
  final TextEditingController nameController = new TextEditingController();
  final TextEditingController emailController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();
  final TextEditingController repeatPasswordController = new TextEditingController();

  // Booleans for whether or not text fields are filled
  bool _submitEnabled;
  bool _nameFilled;
  bool _emailFilled;
  bool _passwordFilled;
  bool _repeatPasswordFilled;

  // StreamController to track register buttons enabled status
  StreamController<bool> _submitEnabledController;

  // Form fields
  String _name;
  String _email;
  String _password;
  // ignore: unused_field
  String _repeatPassword;

  @override
  void initState() {
    _submitEnabled = true;
    _nameFilled = false;
    _emailFilled = false;
    _passwordFilled = false;
    _repeatPasswordFilled = false;
    _submitEnabledController = new StreamController();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  ///submit called when register successfully called
  Future _submit(
    BuildContext context,
    AuthViewModel viewModel,
    AuthType type,
  ) async {
    final form = _formKey.currentState;
    final FlushbarUtil flushbarHelper = FlushbarUtil.getInstance(StoreProvider.of(context));
    AuthAction actionPerformed;

    FocusScope.of(context).requestFocus(new FocusNode());

    try {
      if (type == AuthType.google) {
        actionPerformed = await Auth.authenticateUser(
          type: AuthType.google,
          action: AuthAction.register,
          viewModel: viewModel,
          context: context,
        );
      } else if (type == AuthType.facebook) {
        actionPerformed = await Auth.authenticateUser(
          type: AuthType.facebook,
          action: AuthAction.register,
          viewModel: viewModel,
          context: context,
        );
      } else if (type == AuthType.custom) {
        // Set the store to show loading animations
        viewModel.setLoading(true);

        if (form.validate()) {
          form.save();

          actionPerformed = await Auth.authenticateUser(
            email: _email,
            username: _name,
            password: _password,
            type: AuthType.custom,
            action: AuthAction.register,
            viewModel: viewModel,
            context: context,
          );
        }
      }

      // Stop loading animation
      viewModel.setLoading(false);

      // Redirect user to appropriate page
      if (actionPerformed == AuthAction.register) {
        Navigator.pushReplacement(
          context,
          new MaterialPageRoute(builder: (context) => new SetupStart()),
        );
      } else if (actionPerformed == AuthAction.login) {
        Navigator.pushReplacement(
          context,
          new MaterialPageRoute(builder: (context) => new MainPage()),
        );
      }
    } catch (e) {
      try {
        // Show the error message in a snackbar
        String errorMessage = e.message.toString().startsWith('FIR') ? e.details : e.message;
        flushbarHelper
            .create(
              type: FlushbarType.WARNING,
              message: errorMessage,
              flushbarDuration: FlushbarDuration.SHORT,
            )
            .show(context);
      } catch (e) {
        // Show generic message if the error has no error message
        String errorMessage = 'An error occured during registration.';
        flushbarHelper
            .create(
              type: FlushbarType.ERROR,
              message: errorMessage,
              flushbarDuration: FlushbarDuration.SHORT,
            )
            .show(context);
      }

      viewModel.setLoading(false);
    }
  }

  void submitEnabled() {
    _nameFilled && _emailFilled && _passwordFilled && _repeatPasswordFilled
        ? _submitEnabledController.add(true)
        : _submitEnabledController.add(false);
  }

  ///Describes the part of the user interface represented by this widget.
  ///More information can be found here: https://docs.flutter.io/flutter/widgets/State/build.html
  Widget build(BuildContext context) {
    Size phoneSize = MediaQuery.of(context).size;
    double verticalPadding = phoneSize.height * 0.02;

    setState(() {
      nameController.addListener(() {
        nameController.text.isEmpty ? _nameFilled = false : _nameFilled = true;
        submitEnabled();
      });

      emailController.addListener(() {
        emailController.text.isEmpty ? _emailFilled = false : _emailFilled = true;
        submitEnabled();
      });

      passwordController.addListener(() {
        passwordController.text.isEmpty ? _passwordFilled = false : _passwordFilled = true;
        submitEnabled();
      });

      repeatPasswordController.addListener(() {
        repeatPasswordController.text.isEmpty
            ? _repeatPasswordFilled = false
            : _repeatPasswordFilled = true;
        submitEnabled();
      });
    });

    TextFormField nameTextField = new TextFormField(
      controller: nameController,
      keyboardType: TextInputType.text,
      decoration: const InputDecoration(
        border: InputBorder.none,
        icon: Icon(Icons.person),
        hintText: 'Name',
      ),
      validator: (val) {
        if (val.isEmpty) return 'Username can\'t be empty.';
        return null;
      },
      onSaved: (val) => _name = val,
      autocorrect: false,
    );

    TextFormField emailTextField = new TextFormField(
      controller: emailController,
      keyboardType: TextInputType.emailAddress,
      decoration: const InputDecoration(
        border: InputBorder.none,
        icon: Icon(Icons.email),
        hintText: 'Email',
      ),
      validator: (val) => val.isEmpty ? 'Email can\'t be empty.' : null,
      onSaved: (val) => _email = val,
    );

    TextFormField passwordTextField = new TextFormField(
      controller: passwordController,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        border: InputBorder.none,
        icon: Icon(Icons.lock),
        hintText: 'Password',
        labelStyle: MealMonkeyTextStyles.textFieldStyle,
      ),
      validator: (val) {
        if (val.isEmpty) return 'Password can\'t be empty.';
        return null;
      },
      onSaved: (val) => _password = val,
      obscureText: true,
    );

    TextFormField repeatPasswordTextField = new TextFormField(
      controller: repeatPasswordController,
      keyboardType: TextInputType.text,
      decoration: const InputDecoration(
        border: InputBorder.none,
        icon: Icon(Icons.lock),
        hintText: 'Repeat password',
      ),
      validator: (val) {
        if (val.isEmpty) return 'Repeat password can\'t be empty.';
        if (val != passwordController.text) return 'Repeat password must match password.';
        return null;
      },
      onSaved: (val) => _repeatPassword = val,
      obscureText: true,
    );

    return StoreConnector<AppState, AuthViewModel>(
      converter: (store) => AuthViewModel(
            setUser: (user, action) {
              if (action == AuthAction.login) {
                store.dispatch(UpdateLocalUserAction(user: user));
              } else if (action == AuthAction.register) {
                store.dispatch(UpdateUserAction(user: user));
              }
            },
            setPlan: (plan, action) {
              if (action == AuthAction.login) {
                store.dispatch(UpdateLocalPlanAction(plan: plan));
              } else if (action == AuthAction.register) {
                store.dispatch(UpdatePlanAction(plan: plan));
              }
            },
            setWeek: (week) => store.dispatch(
                  UpdateLocalWeekAction(week: week),
                ),
            setMeals: (meals) => store.dispatch(
                  UpdateLocalMealsAction(meals: meals),
                ),
            setLoading: (loading) => store.dispatch(
                  SetLoadingAction(loading: loading),
                ),
            user: store.state.user,
            plan: store.state.plan,
            loading: store.state.loading,
          ),
      builder: (context, viewModel) => Scaffold(
            resizeToAvoidBottomPadding: true,
            key: _scaffoldKey,
            body: Stack(
              children: <Widget>[
                Positioned(
                  top: 0.0,
                  child: Image.asset(
                    'assets/images/background_graphics.png',
                    fit: BoxFit.fill,
                    height: phoneSize.height,
                    width: phoneSize.width,
                  ),
                ),
                ListView(
                  children: <Widget>[
                    new Center(
                      child: new Container(
                        margin: EdgeInsets.only(top: 10.0),
                        height: phoneSize.height * 0.20,
                        child: new Image.asset(
                          'assets/images/logo.png',
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    new Container(
                      padding: EdgeInsets.fromLTRB(0.0, verticalPadding, 0.0, verticalPadding / 2),
                      child: new Text(
                        "Register",
                        textAlign: TextAlign.center,
                        style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.darkGrey, 38.0),
                      ),
                    ),
                    new Form(
                      key: this._formKey,
                      child: new Stack(
                        children: <Widget>[
                          new Column(
                            children: <Widget>[
                              //text field for username
                              DesignUtil.textFieldGenerator(
                                tff: nameTextField,
                                context: context,
                              ),
                              DesignUtil.textFieldGenerator(
                                tff: emailTextField,
                                context: context,
                              ),
                              DesignUtil.textFieldGenerator(
                                tff: passwordTextField,
                                context: context,
                              ),
                              DesignUtil.textFieldGenerator(
                                tff: repeatPasswordTextField,
                                context: context,
                              ),
                              new Center(
                                child: StreamBuilder(
                                  initialData: false,
                                  stream: _submitEnabledController.stream,
                                  builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
                                    return new Container(
                                      // width: phoneSize.width - Util.pageWidthPadding,
                                      width: MediaQuery.of(context).size.width * 0.80,
                                      margin: EdgeInsets.only(top: verticalPadding / 2),
                                      child: new RaisedButton(
                                        key: new Key("login"),
                                        padding: EdgeInsets.only(top: 13.0, bottom: 13.0),
                                        elevation: 0.0,
                                        color: snapshot.data && _submitEnabled
                                            ? MealMonkeyColours.primary
                                            : MealMonkeyColours.greyButton,
                                        child: new Text(
                                          'REGISTER',
                                          textAlign: TextAlign.center,
                                          style: MealMonkeyTextStyles.mainBold(Colors.white, 18.0),
                                        ),
                                        onPressed: () async {
                                          if (snapshot.data && _submitEnabled) {
                                            _submitEnabled = false;
                                            await _submit(context, viewModel, AuthType.custom);
                                            _submitEnabled = true;
                                          }
                                        },
                                      ),
                                    );
                                  },
                                ),
                              ),

                              new Container(
                                padding: EdgeInsets.only(top: verticalPadding),
                                child: new Center(
                                  child: new Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      new Container(
                                        height: 45.0,
                                        margin: EdgeInsets.only(right: 10.0),
                                        child: new InkWell(
                                          child: new Image.asset(
                                            'assets/images/google.png',
                                            fit: BoxFit.contain,
                                          ),
                                          onTap: () {
                                            _submit(context, viewModel, AuthType.google);
                                          },
                                        ),
                                      ),
                                      new Container(width: 10.0),
                                      new Container(
                                        height: 45.0,
                                        child: new InkWell(
                                          child: new Image.asset(
                                            'assets/images/fb.png',
                                            fit: BoxFit.contain,
                                          ),
                                          onTap: () {
                                            _submit(context, viewModel, AuthType.facebook);
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              new Center(
                                child: new Container(
                                  child: new Center(
                                    child: new Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        new Text(
                                          "Already have an account?",
                                          style: MealMonkeyTextStyles.mainRegular(
                                              MealMonkeyColours.heading2, 17.0),
                                        ),
                                        new FlatButton(
                                          padding: EdgeInsets.only(right: 0.0),
                                          child: new Text(
                                            "Sign In",
                                            style: MealMonkeyTextStyles.mainRegular(
                                                MealMonkeyColours.primary, 18.0),
                                          ),
                                          onPressed: () {
                                            Navigator.pushAndRemoveUntil(
                                                context,
                                                PageRouteBuilder(
                                                  pageBuilder: (BuildContext context,
                                                      Animation<double> animation,
                                                      Animation<double> secondaryAnimation) {
                                                    return LoginScreen();
                                                  },
                                                  transitionsBuilder: (BuildContext context,
                                                      Animation<double> animation,
                                                      Animation<double> secondaryAnimation,
                                                      Widget child) {
                                                    return SlideTransition(
                                                      position: new Tween<Offset>(
                                                        begin: const Offset(1.0, 0.0),
                                                        end: Offset.zero,
                                                      ).animate(animation),
                                                      child: new SlideTransition(
                                                        position: new Tween<Offset>(
                                                          begin: Offset.zero,
                                                          end: const Offset(1.0, 0.0),
                                                        ).animate(secondaryAnimation),
                                                        child: child,
                                                      ),
                                                    );
                                                  },
                                                ),
                                                (_) => false);
                                          },
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
    );
  }
}
