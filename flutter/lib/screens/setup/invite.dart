import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:mealmonkey/firebase/models/plan.dart';
import 'package:mealmonkey/main.dart';
import 'package:mealmonkey/screens/util/colours.dart';
import 'package:mealmonkey/screens/util/designElements.dart';
import 'package:mealmonkey/screens/util/fonts.dart';
import 'dart:async';

import 'package:mealmonkey/state/appstate.dart';
import 'package:share/share.dart';

class SetupInvite extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new SetupInviteState();
  }
}

class SetupInviteState extends State<SetupInvite> {
  @override

  /// Builds the invite screen using the build method with [context].
  /// Documentation can be found here:
  /// https://docs.flutter.io/flutter/widgets/State/build.html
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: (store) => _ViewModel(
            plan: store.state.plan,
          ),
      builder: (context, viewModel) => WillPopScope(
            onWillPop: () => Future<bool>.value(false),
            child: Scaffold(
              backgroundColor: Colors.white,
              body: new Center(
                child: new Container(
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      // Image
                      new Image.asset(
                        'assets/images/group.png',
                        fit: BoxFit.cover,
                        height: MediaQuery.of(context).size.height * 0.37,
                      ),
                      // Page text
                      new Container(
                        padding: EdgeInsets.symmetric(vertical: 30.0),
                        child: new Text('Invite others to your plan',
                            textAlign: TextAlign.center,
                            style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.black, 22.0)),
                      ),
                      new Container(
                        child: new Text(
                          'Share this code with friends so they\n can cook with you',
                          textAlign: TextAlign.center,
                          style: MealMonkeyTextStyles.mainRegular(MealMonkeyColours.grey, 18.0),
                        ),
                      ),
                      // Listile containing the newly generated plan code.
                      new Container(
                        padding: EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
                        child: new Container(
                          decoration: DesignUtil.boxDecoration,
                          child: new Card(
                            elevation: 0.0,
                            child: new ListTile(
                              leading: new Icon(IconData(0xea25, fontFamily: 'IonIcons')),
                              title: new Text(viewModel.plan.code),
                              // Share button
                              trailing: Container(
                                width: 40.0,
                                child: new FlatButton(
                                  onPressed: () {
                                    Share.share("Join my plan on Meal Monkey, using the plan code " +
                                        viewModel.plan.code.toString() +
                                        ".\n\nMeal Monkey makes meal planning easy. Download at https://ios.mealmonkey.nz on iOS or https://android.mealmonkey.nz on Android.");
                                  },
                                  color: Colors.white,
                                  padding: EdgeInsets.only(right: 3.0),
                                  child: new Icon(
                                    Platform.isIOS ? CupertinoIcons.share_solid : Icons.share,
                                    color: MealMonkeyColours.darkGrey,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      // Finish button
                      new Container(
                        padding: EdgeInsets.only(top: 40.0, bottom: 20.0),
                        child: new Center(
                          child: new MaterialButton(
                            padding: EdgeInsets.all(13.0),
                            elevation: 0.0,
                            minWidth: 146.0,
                            color: MealMonkeyColours.primary,
                            child: new Text(
                              'FINISH',
                              textAlign: TextAlign.center,
                              style: MealMonkeyTextStyles.mainBold(Colors.white, 18.0),
                            ),
                            onPressed: () async {
                              Navigator.pushReplacement(
                                context,
                                new MaterialPageRoute(builder: (context) => new MainPage()),
                              );
                            },
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
    );
  }
}

/// Class used by the store connector to connect the store and
/// the local state of the application.
class _ViewModel {
  Plan plan;

  _ViewModel({this.plan});
}
