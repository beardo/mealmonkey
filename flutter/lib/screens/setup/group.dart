import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:mealmonkey/firebase/models/plan.dart';
import 'package:mealmonkey/firebase/models/user.dart';
import 'package:mealmonkey/screens/setup/budget.dart';
import 'package:mealmonkey/screens/setup/joinGroup.dart';
import 'package:mealmonkey/screens/util/colours.dart';
import 'package:mealmonkey/screens/util/flushbar.dart';
import 'package:mealmonkey/screens/util/fonts.dart';
import 'package:mealmonkey/state/action.dart';
import 'package:mealmonkey/state/appstate.dart';

class SetupGroup extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new SetupGroupState();
  }
}

class SetupGroupState extends State<SetupGroup> {
  bool _joinSelected;
  bool _createSelected;
  StreamController<bool> _submitController;

  @override
  void initState() {
    _joinSelected = false;
    _createSelected = false;
    _submitController = new StreamController();
    super.initState();
  }

  /// Builds the group setup screen using the build method with [context].
  /// Documentation can be found here:
  /// https://docs.flutter.io/flutter/widgets/State/build.html
  @override
  Widget build(BuildContext context) {
    final FlushbarUtil flushbarHelper = FlushbarUtil.getInstance(StoreProvider.of(context));

    return new StoreConnector<AppState, _ViewModel>(
      converter: (store) => _ViewModel(
            setPlan: (plan) => store.dispatch(
                  UpdateLocalPlanAction(plan: plan),
                ),
            user: store.state.user,
          ),
      builder: (context, viewModel) => Scaffold(
            backgroundColor: Colors.white,
            body: new Center(
              child: Padding(
                padding: EdgeInsets.only(top: 20.0),
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    // Image
                    new Image.asset(
                      'assets/images/prefer.png',
                      fit: BoxFit.cover,
                      height: MediaQuery.of(context).size.height * 0.37,
                    ),
                    // Page text
                    new Container(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      child: new Text(
                        'Enter your preferences',
                        textAlign: TextAlign.center,
                        style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.black, 22.0),
                      ),
                    ),
                    new Container(
                      padding: EdgeInsets.only(top: 10.0, bottom: 0.0),
                      child: new Text(
                        'Please tell us whether you\'d like\n to join a group or start a new one',
                        textAlign: TextAlign.center,
                        style: MealMonkeyTextStyles.mainRegular(MealMonkeyColours.grey, 18.0),
                      ),
                    ),
                    // Join and create icon selection
                    new Container(
                      padding: EdgeInsets.only(top: 20.0),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Column(
                            children: <Widget>[
                              _createSelected
                                  ? _iconButton(
                                      0xe868,
                                      Colors.white,
                                      MealMonkeyColours.primary,
                                      'Create',
                                    )
                                  : _iconButton(
                                      0xe868,
                                      MealMonkeyColours.primary,
                                      Colors.white,
                                      'Create',
                                    ),
                              new Container(
                                padding: EdgeInsets.only(top: 10.0),
                                child: new Text('Create'),
                              ),
                            ],
                          ),
                          new Container(width: 50.0),
                          new Column(
                            children: <Widget>[
                              _joinSelected
                                  ? _iconButton(
                                      0xe805,
                                      Colors.white,
                                      MealMonkeyColours.primary,
                                      'Join',
                                    )
                                  : _iconButton(
                                      0xe805,
                                      MealMonkeyColours.primary,
                                      Colors.white,
                                      'Join',
                                    ),
                              new Container(
                                padding: EdgeInsets.only(top: 10.0),
                                child: new Text('Join'),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    // Next page button
                    new Container(
                      padding: EdgeInsets.symmetric(vertical: 30.0),
                      child: StreamBuilder(
                        initialData: false,
                        stream: _submitController.stream,
                        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
                          return new MaterialButton(
                            padding: EdgeInsets.all(13.0),
                            elevation: 0.0,
                            color: snapshot.data
                                ? MealMonkeyColours.primary
                                : MealMonkeyColours.greyButton,
                            child: new Text(
                              'NEXT',
                              textAlign: TextAlign.center,
                              style: MealMonkeyTextStyles.mainBold(Colors.white, 18.0),
                            ),
                            onPressed: () async {
                              if (snapshot.data) {
                                _submit(context, viewModel, flushbarHelper);
                              }
                            },
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
    );
  }

  /// Creates an icon button based on the input [type], [iconColor], [fillColor], and the
  /// location of the icon using the [iconData]. Only one can be selected at a time.
  Widget _iconButton(int iconData, Color iconColor, Color fillColor, String type) {
    bool create = false;
    bool join = false;

    if (type == 'Create') {
      create = true;
    } else {
      join = true;
    }
    return new Container(
      decoration: new BoxDecoration(
        color: fillColor,
        border: new Border.all(color: iconColor),
        borderRadius: new BorderRadius.circular(100.0),
      ),
      child: new IconButton(
          icon: new Icon(
            IconData(iconData, fontFamily: 'IonIcons'),
            color: iconColor,
          ),
          onPressed: () {
            setState(() {
              _joinSelected = join;
              _createSelected = create;

              _submitController.add(_joinSelected || _createSelected);
            });
          }),
    );
  }

  /// Submit method for the next button. Either navigates to the join group page or creates
  /// a blank plan for the [viewModel] and navigates to the setup budget screen using the
  /// [context].
  void _submit(BuildContext context, _ViewModel viewModel, FlushbarUtil flushbarHelper) async {
    if (_joinSelected) {
      Navigator.push(
        context,
        new MaterialPageRoute(builder: (context) => new JoinGroup()),
      );
    } else if (_createSelected) {
      // Create new plan
      Plan newPlan = Plan.blankPlan(viewModel.user);

      // Create the plan on Firestore
      viewModel.plan = newPlan;
      viewModel.setPlan(viewModel.plan);

      Navigator.push(
        context,
        new MaterialPageRoute(builder: (context) => new SetupBudget()),
      );
    } else {
      flushbarHelper
          .create(
            type: FlushbarType.WARNING,
            message: 'Please select either "Create" or "Join" to create or join a group.',
            flushbarDuration: FlushbarDuration.SHORT,
          )
          .show(context);
    }
  }
}

typedef SetPlan(Plan plan);

/// Class used by the store connector to connect firebase and
/// the local state of the application.
class _ViewModel {
  final SetPlan setPlan;
  User user;
  Plan plan;

  _ViewModel({
    this.setPlan,
    this.user,
    this.plan,
  });
}
