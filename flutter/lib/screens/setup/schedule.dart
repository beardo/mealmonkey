import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:mealmonkey/firebase/models/plan.dart';
import 'package:mealmonkey/screens/setup/labels.dart';
import 'package:mealmonkey/screens/util/colours.dart';
import 'package:mealmonkey/screens/util/flushbar.dart';
import 'package:mealmonkey/screens/util/fonts.dart';
import 'package:mealmonkey/state/action.dart';
import 'package:mealmonkey/state/appstate.dart';

class SetupSchedule extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new SetupScheduleState();
  }
}

class SetupScheduleState extends State<SetupSchedule> {
  StreamController<bool> _submitController;

  @override
  void initState() {
    _submitController = new StreamController();
    super.initState();
  }

  /// Builds the budget setup screen using the build method with [context].
  /// Documentation can be found here:
  /// https://docs.flutter.io/flutter/widgets/State/build.html
  @override
  Widget build(BuildContext context) {
    final FlushbarUtil flushbarHelper = FlushbarUtil.getInstance(StoreProvider.of(context));

    return StoreConnector<AppState, _ViewModel>(
      converter: (store) => _ViewModel(
            updatePlan: (plan) => store.dispatch(
                  UpdateLocalPlanAction(plan: plan),
                ),
            plan: store.state.plan,
          ),
      builder: (context, viewModel) => Scaffold(
            backgroundColor: Colors.white,
            body: viewModel.plan == null
                ? new Container()
                : new Center(
                    child: new Container(
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          // Image
                          new Image.asset(
                            'assets/images/schedule.png',
                            fit: BoxFit.cover,
                            height: MediaQuery.of(context).size.height * 0.37,
                          ),
                          // Page text
                          new Container(
                            padding: EdgeInsets.symmetric(vertical: 10.0),
                            child: new Text(
                              'Enter your schedule',
                              textAlign: TextAlign.center,
                              style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.black, 22.0),
                            ),
                          ),
                          new Container(
                            padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                            child: new Text(
                              'Please select the day you\'d like\nto cook each week',
                              textAlign: TextAlign.center,
                              style: MealMonkeyTextStyles.mainRegular(MealMonkeyColours.grey, 18.0),
                            ),
                          ),
                          // Day icons
                          new Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: MediaQuery.of(context).size.width * 0.20),
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                createDayIcon(
                                  0xe808,
                                  0xe803,
                                  viewModel.plan.daysConfig.mondayIsSelected,
                                  "Monday",
                                  viewModel,
                                ),
                                createDayIcon(
                                  0xe805,
                                  0xe801,
                                  viewModel.plan.daysConfig.tuesdayIsSelected,
                                  "Tuesday",
                                  viewModel,
                                ),
                                createDayIcon(
                                  0xe804,
                                  0xe800,
                                  viewModel.plan.daysConfig.wednesdayIsSelected,
                                  "Wednesday",
                                  viewModel,
                                ),
                                createDayIcon(
                                  0xe805,
                                  0xe801,
                                  viewModel.plan.daysConfig.thursdayIsSelected,
                                  "Thursday",
                                  viewModel,
                                ),
                                createDayIcon(
                                  0xe807,
                                  0xe806,
                                  viewModel.plan.daysConfig.fridayIsSelected,
                                  "Friday",
                                  viewModel,
                                ),
                              ],
                            ),
                          ),
                          new Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: MediaQuery.of(context).size.width * 0.38,
                                vertical: 10.0),
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                createDayIcon(
                                  0xe809,
                                  0xe802,
                                  viewModel.plan.daysConfig.saturdayIsSelected,
                                  "Saturday",
                                  viewModel,
                                ),
                                createDayIcon(
                                  0xe809,
                                  0xe802,
                                  viewModel.plan.daysConfig.sundayIsSelected,
                                  "Sunday",
                                  viewModel,
                                ),
                              ],
                            ),
                          ),
                          // Bottom buttons
                          new Builder(
                            builder: (BuildContext context) {
                              return Container(
                                width: MediaQuery.of(context).size.width * 0.8,
                                padding: EdgeInsets.only(top: 25.0),
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    new MaterialButton(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 13.0,
                                          horizontal: MediaQuery.of(context).size.width * 0.11),
                                      elevation: 0.0,
                                      minWidth: MediaQuery.of(context).size.width * 0.38,
                                      color: MealMonkeyColours.greyButton,
                                      child: new Text(
                                        'BACK',
                                        textAlign: TextAlign.center,
                                        style: MealMonkeyTextStyles.mainBold(Colors.white, 18.0),
                                      ),
                                      onPressed: () async {
                                        Navigator.pop(context);
                                      },
                                    ),
                                    new StreamBuilder(
                                      initialData: false,
                                      stream: _submitController.stream,
                                      builder:
                                          (BuildContext context, AsyncSnapshot<bool> snapshot) {
                                        return new MaterialButton(
                                          padding: EdgeInsets.symmetric(
                                              vertical: 13.0,
                                              horizontal: MediaQuery.of(context).size.width * 0.11),
                                          elevation: 0.0,
                                          minWidth: MediaQuery.of(context).size.width * 0.38,
                                          color: snapshot.data
                                              ? MealMonkeyColours.primary
                                              : MealMonkeyColours.greyButton,
                                          child: new Text(
                                            'NEXT',
                                            textAlign: TextAlign.center,
                                            style:
                                                MealMonkeyTextStyles.mainBold(Colors.white, 18.0),
                                          ),
                                          onPressed: () async {
                                            if (snapshot.data) {
                                              _submit(viewModel, context, flushbarHelper);
                                            }
                                          },
                                        );
                                      },
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
          ),
    );
  }

  /// Returns a new [IconButton] using the icon font codes [iconCodeChecked] and [iconCodeBlank],
  /// based on the input [day].
  Widget createDayIcon(
    int iconCodeChecked,
    int iconCodeBlank,
    bool dayIsSelected,
    String dayName,
    _ViewModel viewModel,
  ) {
    return new Stack(
      children: <Widget>[
        new Container(
          height: 36.0,
          width: 36.0,
          decoration: new BoxDecoration(
              border: new Border.all(color: MealMonkeyColours.primary),
              borderRadius: new BorderRadius.circular(100.0)),
        ),
        new Positioned(
          right: -7.7,
          bottom: -7.7,
          child: IconButton(
            icon: new Icon(
              dayIsSelected
                  ? IconData(iconCodeChecked, fontFamily: 'icomoon')
                  : IconData(iconCodeBlank, fontFamily: 'icomoon'),
              color: dayIsSelected ? MealMonkeyColours.primary : MealMonkeyColours.primary,
            ),
            iconSize: 36.0,
            onPressed: () {
              dayIsSelected = !dayIsSelected;
              // Switch case for which day has been selected
              switch (dayName) {
                case "Monday":
                  if (dayIsSelected)
                    viewModel.plan.daysConfig.monday = new List<DocumentReference>();
                  else
                    viewModel.plan.daysConfig.monday = null;
                  break;
                case "Tuesday":
                  if (dayIsSelected)
                    viewModel.plan.daysConfig.tuesday = List<DocumentReference>();
                  else
                    viewModel.plan.daysConfig.tuesday = null;
                  break;
                case "Wednesday":
                  if (dayIsSelected)
                    viewModel.plan.daysConfig.wednesday = List<DocumentReference>();
                  else
                    viewModel.plan.daysConfig.wednesday = null;
                  break;
                case "Thursday":
                  if (dayIsSelected)
                    viewModel.plan.daysConfig.thursday = List<DocumentReference>();
                  else
                    viewModel.plan.daysConfig.thursday = null;
                  break;
                case "Friday":
                  if (dayIsSelected)
                    viewModel.plan.daysConfig.friday = List<DocumentReference>();
                  else
                    viewModel.plan.daysConfig.friday = null;
                  break;
                case "Saturday":
                  if (dayIsSelected)
                    viewModel.plan.daysConfig.saturday = List<DocumentReference>();
                  else
                    viewModel.plan.daysConfig.saturday = null;
                  break;
                case "Sunday":
                  if (dayIsSelected)
                    viewModel.plan.daysConfig.sunday = List<DocumentReference>();
                  else
                    viewModel.plan.daysConfig.sunday = null;
                  break;
                default:
              }
              // Updates the viewModel with the schedule.
              viewModel.updatePlan(viewModel.plan);

              // Update the submit button state
              setState(() {
                viewModel.plan.daysConfig.anyDaySelected
                    ? _submitController.add(true)
                    : _submitController.add(false);
              });
            },
          ),
        ),
      ],
    );
  }

  /// Ensures that one day has been selected and then navigates the user to the setup labels page
  /// using the input [context]. Sets the schedule into the [viewModel].
  void _submit(
    _ViewModel viewModel,
    BuildContext context,
    FlushbarUtil flushbarHelper,
  ) {
    if (viewModel.plan.daysConfig.anyDaySelected) {
      // Success
      Navigator.push(context, new MaterialPageRoute(builder: (context) => new SetupLabels()));
    } else {
      // Failure
      flushbarHelper
          .create(
            type: FlushbarType.WARNING,
            message: 'Please select at least one day you wish to cook on.',
            flushbarDuration: FlushbarDuration.SHORT,
          )
          .show(context);
    }
  }
}

typedef UpdatePlan(Plan plan);

/// Class used by the store connector to connect the store and
/// the local state of the application.
class _ViewModel {
  final UpdatePlan updatePlan;
  Plan plan;

  _ViewModel({
    this.updatePlan,
    this.plan,
  });
}
