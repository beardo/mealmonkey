import 'package:cloud_functions/cloud_functions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'dart:async';
import 'package:mealmonkey/firebase/models/meal.dart';
import 'package:mealmonkey/firebase/models/plan.dart';
import 'package:mealmonkey/firebase/models/user.dart';
import 'package:mealmonkey/firebase/models/week.dart';
import 'package:mealmonkey/screens/setup/invite.dart';
import 'package:mealmonkey/screens/util/colours.dart';
import 'package:mealmonkey/screens/util/flushbar.dart';
import 'package:mealmonkey/screens/util/fonts.dart';
import 'package:mealmonkey/state/action.dart';
import 'package:mealmonkey/state/appstate.dart';

class SetupLabels extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new SetupLabelsState();
  }
}

class SetupLabelsState extends State<SetupLabels> {
  bool _isSubmitting;

  @override
  void initState() {
    _isSubmitting = false;
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override

  /// Builds the budget setup screen using the build method with [context].
  /// Documentation can be found here:
  /// https://docs.flutter.io/flutter/widgets/State/build.html
  Widget build(BuildContext context) {
    final FlushbarUtil flushbarHelper = FlushbarUtil.getInstance(StoreProvider.of(context));

    return StoreConnector<AppState, _ViewModel>(
      converter: (store) => _ViewModel(
            updateUser: (user) => store.dispatch(
                  UpdateUserAction(user: user),
                ),
            updatePlan: (plan) => store.dispatch(
                  UpdatePlanAction(plan: plan),
                ),
            updateLocalPlan: (plan) => store.dispatch(
                  UpdateLocalPlanAction(plan: plan),
                ),
            setWeek: (week) => store.dispatch(
                  UpdateLocalWeekAction(week: week),
                ),
            setMeals: (meals) => store.dispatch(
                  UpdateLocalMealsAction(meals: meals),
                ),
            setLoading: (loading) => store.dispatch(
                  SetLoadingAction(loading: loading),
                ),
            loading: store.state.loading,
            user: store.state.user,
            plan: store.state.plan,
          ),
      builder: (context, viewModel) => Scaffold(
            backgroundColor: Colors.white,
            body: new Center(
              child: Container(
                padding: EdgeInsets.only(top: 10.0),
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    // Image
                    new Image.asset(
                      'assets/images/dietery.png',
                      fit: BoxFit.cover,
                      height: MediaQuery.of(context).size.height * 0.37,
                    ),
                    // Page text
                    new Container(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      child: new Text(
                        'Select your requirements',
                        textAlign: TextAlign.center,
                        style: MealMonkeyTextStyles.mainBold(Colors.black, 22.0),
                      ),
                    ),
                    new Container(
                      padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                      child: new Text(
                        'Please tell us whether you or your\ngroup members have any dietary\nrequirements',
                        textAlign: TextAlign.center,
                        style: MealMonkeyTextStyles.mainRegular(MealMonkeyColours.grey, 18.0),
                      ),
                    ),
                    // Dietary labels
                    new Container(
                      width: MediaQuery.of(context).size.width * 0.8,
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      // Top row
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          buildLabel(viewModel, "Vegetarian"),
                          buildLabel(viewModel, "Vegan"),
                          buildLabel(viewModel, "Gluten Free"),
                        ],
                      ),
                    ),
                    new Container(
                      width: MediaQuery.of(context).size.width * 0.8,
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      // Bottom row
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          buildLabel(viewModel, "Dairy Free"),
                          buildLabel(viewModel, "Nut Free"),
                          buildLabel(viewModel, "Soy Free"),
                        ],
                      ),
                    ),
                    // Bottom buttons
                    new Builder(builder: (BuildContext context) {
                      return new Container(
                        width: MediaQuery.of(context).size.width * 0.8,
                        padding: EdgeInsets.only(top: 10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            new MaterialButton(
                              padding: EdgeInsets.symmetric(
                                  vertical: 13.0,
                                  horizontal: MediaQuery.of(context).size.width * 0.11),
                              elevation: 0.0,
                              minWidth: MediaQuery.of(context).size.width * 0.38,
                              color: MealMonkeyColours.greyButton,
                              child: new Text(
                                'BACK',
                                textAlign: TextAlign.center,
                                style: MealMonkeyTextStyles.mainBold(Colors.white, 18.0),
                              ),
                              onPressed: () async {
                                Navigator.pop(context);
                              },
                            ),
                            new MaterialButton(
                              padding: EdgeInsets.symmetric(
                                  vertical: 13.0,
                                  horizontal: MediaQuery.of(context).size.width * 0.08),
                              elevation: 0.0,
                              minWidth: MediaQuery.of(context).size.width * 0.38,
                              color: _isSubmitting
                                  ? MealMonkeyColours.greyButton
                                  : MealMonkeyColours.primary,
                              child: new Text(
                                'CREATE',
                                textAlign: TextAlign.center,
                                style: MealMonkeyTextStyles.mainBold(Colors.white, 18.0),
                              ),
                              onPressed: () async {
                                if (!_isSubmitting) {
                                  _isSubmitting = true;
                                  await _submit(context, viewModel, flushbarHelper);
                                  _isSubmitting = false;
                                }
                              },
                            ),
                          ],
                        ),
                      );
                    }),
                  ],
                ),
              ),
            ),
          ),
    );
  }

  /// Returns an [Inkwell] displaying the input [label] which can set
  /// the dietary requirement into the [viewModel].
  Widget buildLabel(_ViewModel viewModel, String label) {
    bool labelIsChecked = viewModel.plan.labels.contains(label);

    return new InkWell(
      borderRadius: new BorderRadius.circular(100.0),
      onTap: () {
        setState(() {
          labelIsChecked = !labelIsChecked;

          if (labelIsChecked) {
            viewModel.plan.labels.add(label);
          } else {
            viewModel.plan.labels.removeWhere((item) => item == label);
          }

          viewModel.updateLocalPlan(viewModel.plan);
        });
      },
      child: new Container(
        height: 36.0,
        width: 80.0,
        decoration: new BoxDecoration(
          border: new Border.all(color: MealMonkeyColours.primary),
          borderRadius: new BorderRadius.circular(100.0),
          color: labelIsChecked ? MealMonkeyColours.primary : null,
        ),
        child: new Center(
          child: new Text(label,
              style: MealMonkeyTextStyles.mainRegular(
                  labelIsChecked ? Colors.white : MealMonkeyColours.primary, 12.0)),
        ),
      ),
    );
  }

  /// Validates the confirmed settings, then calls [_performSettingsUpdate]
  Future _submit(
    BuildContext context,
    _ViewModel viewModel,
    FlushbarUtil flushbarHelper,
  ) async {
    viewModel.setLoading(true);

    flushbarHelper
        .create(
          type: FlushbarType.INFO,
          message: "Creating your meal plan...",
        )
        .show(context);

    // Create Firestore plan with code and use values from Redux plan
    viewModel.plan = await Plan.create(viewModel.user, viewModel.plan);
    viewModel.updatePlan(viewModel.plan);

    // Generate meals on Cloud Firestore
    try {
      await CloudFunctions.instance.call(
        functionName: 'generateMeals',
        parameters: <String, dynamic>{
          'labels': viewModel.plan.labels,
          'required_meals': viewModel.plan.daysConfig.numberOfDays,
          'plan_id': viewModel.plan.reference.documentID,
          'total_budget': viewModel.plan.budget,
        },
      );

      // Update user with new plan reference
      viewModel.user.plan = viewModel.plan.reference;
      viewModel.updateUser(viewModel.user);

      // Update the week in the Redux store
      viewModel.week = Week.from(await viewModel.plan.getCurrentWeek());
      viewModel.setWeek(viewModel.week);

      // Update the meals in the Redux store
      viewModel.meals = await viewModel.week.getMeals();
      viewModel.setMeals(viewModel.meals);

      // Dismiss the logging in status
      viewModel.setLoading(false);
      flushbarHelper.dismissAll();

      // Update the users setup status to true
      viewModel.user.setup = true;
      viewModel.updateUser(viewModel.user);

      Navigator.pushReplacement(
        context,
        new MaterialPageRoute(builder: (context) => new SetupInvite()),
      );
    } on CloudFunctionsException catch (e) {
      viewModel.setLoading(false);
      flushbarHelper.dismissAll();

      flushbarHelper
          .create(
            type: FlushbarType.ERROR,
            message: e.message,
            flushbarDuration: FlushbarDuration.MEDIUM,
          )
          .show(context);
    } catch (e) {
      print(e);

      viewModel.setLoading(false);
      flushbarHelper.dismissAll();

      flushbarHelper
          .create(
            type: FlushbarType.ERROR,
            message: 'Something went wrong generating your recipes.',
            flushbarDuration: FlushbarDuration.MEDIUM,
          )
          .show(context);
    }
  }
}

typedef UpdateUser(User user);
typedef UpdatePlan(Plan plan);
typedef UpdateLocalPlan(Plan plan);
typedef SetWeek(Week week);
typedef SetMeals(List<Meal> meals);
typedef SetLoading(bool loading);

/// Class used by the store connector to connect the store and
/// the local state of the application.
class _ViewModel {
  final UpdateUser updateUser;
  final UpdatePlan updatePlan;
  final UpdateLocalPlan updateLocalPlan;
  final SetWeek setWeek;
  final SetMeals setMeals;
  final SetLoading setLoading;
  bool loading;
  User user;
  Plan plan;
  Week week;
  List<Meal> meals;

  _ViewModel({
    this.updateUser,
    this.updatePlan,
    this.updateLocalPlan,
    this.setWeek,
    this.setMeals,
    this.setLoading,
    this.loading,
    this.user,
    this.plan,
  });
}
