import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:mealmonkey/firebase/models/plan.dart';
import 'package:mealmonkey/screens/setup/schedule.dart';
import 'package:mealmonkey/screens/util/colours.dart';
import 'package:mealmonkey/screens/util/designElements.dart';
import 'package:mealmonkey/screens/util/fonts.dart';
import 'package:mealmonkey/state/action.dart';
import 'package:mealmonkey/state/appstate.dart';

class SetupBudget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new SetupBudgetState();
  }
}

class SetupBudgetState extends State<SetupBudget> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  FocusNode _focusNodeBudget = new FocusNode();

  int _budget;
  TextEditingController _budgetController;
  StreamController<bool> _submitController;

  @override
  void initState() {
    _budgetController = new TextEditingController();
    _submitController = new StreamController();
    super.initState();
  }

  /// Navigates to the next page in the setup process and saves the date into
  /// the local [viewModel] using the [context].
  void _nextPage(BuildContext context, _ViewModel viewModel) {
    final form = _formKey.currentState;

    if (form.validate()) {
      form.save();

      // Retrieve member from store and combine with new settings
      viewModel.plan.budget = _budget;
      viewModel.updatePlan(viewModel.plan);

      Navigator.push(
        context,
        new MaterialPageRoute(builder: (context) => new SetupSchedule()),
      );
    }
  }

  /// Builds the budget setup screen using the build method with [context].
  /// Documentation can be found here:
  /// https://docs.flutter.io/flutter/widgets/State/build.html
  @override
  Widget build(BuildContext context) {
    setState(() {
      _budgetController.addListener(() {
        String budgetText = _budgetController.text;

        budgetText.isNotEmpty && int.tryParse(budgetText) is int
            ? _submitController.add(true)
            : _submitController.add(false);
      });
    });

    return StoreConnector<AppState, _ViewModel>(
      converter: (store) => _ViewModel(
            updatePlan: (plan) => store.dispatch(
                  UpdateLocalPlanAction(plan: plan),
                ),
            plan: store.state.plan,
          ),
      builder: (context, _viewModel) => Scaffold(
            resizeToAvoidBottomPadding: true,
            backgroundColor: Colors.white,
            body: Center(
              child: Padding(
                padding: EdgeInsets.only(top: 20.0),
                child: _viewModel.plan == null
                    ? new Container()
                    : SingleChildScrollView(
                        // Fix for #34 found at https://github.com/flutter/flutter/issues/20485
                        physics: PageScrollPhysics(),
                        child: new Form(
                          key: this._formKey,
                          child: new Container(
                            child: new Column(
                              children: <Widget>[
                                // Image
                                new Image.asset(
                                  'assets/images/budget.png',
                                  fit: BoxFit.cover,
                                  height: MediaQuery.of(context).size.height * 0.37,
                                ),
                                // Text
                                new Container(
                                  padding: EdgeInsets.symmetric(vertical: 10.0),
                                  child: new Text(
                                    'Enter your budget',
                                    textAlign: TextAlign.center,
                                    style: MealMonkeyTextStyles.mainBold(
                                        MealMonkeyColours.black, 22.0),
                                  ),
                                ),
                                new Container(
                                  padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                  child: new Text(
                                    'Please enter how much you want\nto spend on food each week.',
                                    textAlign: TextAlign.center,
                                    style: MealMonkeyTextStyles.mainRegular(
                                      MealMonkeyColours.grey,
                                      18.0,
                                    ),
                                  ),
                                ),
                                // Budget text field
                                DesignUtil.textFieldGenerator(
                                  tff: TextFormField(
                                    controller: _budgetController,
                                    scrollPadding: EdgeInsets.only(top: 1000.0),
                                    keyboardType: TextInputType.number,
                                    focusNode: _focusNodeBudget,
                                    decoration: const InputDecoration(
                                        border: InputBorder.none,
                                        icon: Icon(Icons.attach_money),
                                        hintText: 'Budget'),
                                    validator: (val) {
                                      if (val.isEmpty) {
                                        return 'Budget can\'t be empty.';
                                      }
                                      if (val.contains('.')) {
                                        return 'Budget can\'t include a decimal point.';
                                      }
                                    },
                                    onSaved: (val) => _budget = int.parse(val),
                                    autocorrect: false,
                                  ),
                                  context: context,
                                  width: true,
                                ),
                                // Bottom row buttons
                                new Container(
                                  width: MediaQuery.of(context).size.width * 0.8,
                                  padding: EdgeInsets.symmetric(vertical: 30.0),
                                  child: new Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      new MaterialButton(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 13.0,
                                            horizontal: MediaQuery.of(context).size.width * 0.11),
                                        elevation: 0.0,
                                        minWidth: MediaQuery.of(context).size.width * 0.38,
                                        color: MealMonkeyColours.greyButton,
                                        child: new Text(
                                          'BACK',
                                          textAlign: TextAlign.center,
                                          style: MealMonkeyTextStyles.mainBold(Colors.white, 18.0),
                                        ),
                                        onPressed: () async {
                                          Navigator.pop(context);
                                        },
                                      ),
                                      StreamBuilder(
                                        initialData: false,
                                        stream: _submitController.stream,
                                        builder:
                                            (BuildContext context, AsyncSnapshot<bool> snapshot) {
                                          return new MaterialButton(
                                            padding: EdgeInsets.symmetric(
                                                vertical: 13.0,
                                                horizontal:
                                                    MediaQuery.of(context).size.width * 0.11),
                                            elevation: 0.0,
                                            minWidth: MediaQuery.of(context).size.width * 0.38,
                                            color: snapshot.data ? MealMonkeyColours.primary : MealMonkeyColours.greyButton,
                                            child: new Text(
                                              'NEXT',
                                              textAlign: TextAlign.center,
                                              style:
                                                  MealMonkeyTextStyles.mainBold(Colors.white, 18.0),
                                            ),
                                            onPressed: () async {
                                              if (snapshot.data) {
                                                _nextPage(context, _viewModel);
                                              }
                                            },
                                          );
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
              ),
            ),
          ),
    );
  }
}

typedef UpdatePlan(Plan plan);

/// Class used by the store connector to connect the store and
/// the local state of the application.
class _ViewModel {
  final UpdatePlan updatePlan;
  Plan plan;

  _ViewModel({
    this.updatePlan,
    this.plan,
  });
}
