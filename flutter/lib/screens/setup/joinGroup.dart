import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:mealmonkey/firebase/models/meal.dart';
import 'package:mealmonkey/firebase/models/plan.dart';
import 'package:mealmonkey/firebase/models/user.dart';
import 'package:mealmonkey/firebase/models/week.dart';
import 'package:mealmonkey/firebase/support_models/planUser.dart';
import 'package:mealmonkey/main.dart';
import 'package:mealmonkey/screens/util/colours.dart';
import 'package:mealmonkey/screens/util/designElements.dart';
import 'package:mealmonkey/screens/util/flushbar.dart';
import 'package:mealmonkey/screens/util/fonts.dart';
import 'package:mealmonkey/state/action.dart';
import 'package:mealmonkey/state/appstate.dart';

class JoinGroup extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new JoinGroupState();
  }
}

class JoinGroupState extends State<JoinGroup> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  String _code;
  TextEditingController _codeController;
  StreamController<bool> _submitController;

  @override
  void initState() {
    _codeController = new TextEditingController();
    _submitController = new StreamController();
    super.initState();
  }

  /// Builds the join group setup screen using the build method with [context].
  /// Documentation can be found here:
  /// https://docs.flutter.io/flutter/widgets/State/build.html
  @override
  Widget build(BuildContext context) {
    final FlushbarUtil flushbarHelper = FlushbarUtil.getInstance(StoreProvider.of(context));

    setState(() {
      _codeController.addListener(() {
        _codeController.text.length < 6 ? _submitController.add(false) : _submitController.add(true);
      });
    });

    return StoreConnector<AppState, _ViewModel>(
      converter: (store) => _ViewModel(
            updateUser: (user) => store.dispatch(new UpdateUserAction(user: user)),
            setPlan: (plan) => store.dispatch(new UpdatePlanAction(plan: plan)),
            setWeek: (week) => store.dispatch(new UpdateWeekAction(week: week)),
            setMeals: (meals) => store.dispatch(new UpdateMealsAction(meals: meals)),
            user: store.state.user,
            plan: store.state.plan,
          ),
      builder: (context, _viewModel) => Scaffold(
            resizeToAvoidBottomPadding: true,
            backgroundColor: Colors.white,
            body: Center(
              child: Padding(
                padding: EdgeInsets.only(top: 20.0),
                child: SingleChildScrollView(
                  child: new Form(
                    key: this._formKey,
                    child: new Container(
                      child: new Column(
                        children: <Widget>[
                          // Image
                          new Image.asset(
                            'assets/images/group.png',
                            fit: BoxFit.cover,
                            height: MediaQuery.of(context).size.height * 0.37,
                          ),
                          // Page text
                          new Container(
                            padding: EdgeInsets.symmetric(vertical: 10.0),
                            child: new Text(
                              'Enter your plan code',
                              textAlign: TextAlign.center,
                              style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.black, 22.0),
                            ),
                          ),
                          new Container(
                            padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                            child: new Text(
                              'Please code of the plan\nyou want to join',
                              textAlign: TextAlign.center,
                              style: MealMonkeyTextStyles.mainRegular(MealMonkeyColours.grey, 18.0),
                            ),
                          ),
                          // Plan code text field
                          DesignUtil.textFieldGenerator(
                            tff: new TextFormField(
                              controller: _codeController,
                              decoration: const InputDecoration(
                                border: InputBorder.none,
                                icon: Icon(IconData(0xea25, fontFamily: 'IonIcons')),
                                hintText: 'Plan Code',
                              ),
                              validator: (val) {
                                if (val.isEmpty) {
                                  return 'Code can\'t be empty.';
                                }
                                if (val.contains('.')) {
                                  return 'Code can\'t include a decimal point.';
                                }
                                if (val.length != 6) {
                                  return 'Plan code\'s are 6 characters in length.';
                                }
                              },
                              onSaved: (val) => _code = val,
                              autocorrect: false,
                            ),
                            context: context,
                            width: true,
                          ),
                          // Bottom row buttons
                          new Builder(
                            builder: (BuildContext context) {
                              return new Container(
                                width: MediaQuery.of(context).size.width * 0.8,
                                padding: EdgeInsets.symmetric(
                                  vertical: 20.0,
                                ),
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    new MaterialButton(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 13.0,
                                          horizontal: MediaQuery.of(context).size.width * 0.11),
                                      elevation: 0.0,
                                      minWidth: MediaQuery.of(context).size.width * 0.38,
                                      color: MealMonkeyColours.greyButton,
                                      child: new Text(
                                        'BACK',
                                        textAlign: TextAlign.center,
                                        style: MealMonkeyTextStyles.mainBold(Colors.white, 18.0),
                                      ),
                                      onPressed: () async {
                                        Navigator.pop(context);
                                      },
                                    ),
                                    StreamBuilder(
                                      initialData: false,
                                      stream: _submitController.stream,
                                      builder:
                                          (BuildContext context, AsyncSnapshot<bool> snapshot) {
                                        return new MaterialButton(
                                          padding: EdgeInsets.symmetric(
                                              vertical: 13.0,
                                              horizontal: MediaQuery.of(context).size.width * 0.11),
                                          elevation: 0.0,
                                          minWidth: MediaQuery.of(context).size.width * 0.38,
                                          color: snapshot.data
                                              ? MealMonkeyColours.primary
                                              : MealMonkeyColours.greyButton,
                                          child: new Text(
                                            'JOIN',
                                            textAlign: TextAlign.center,
                                            style:
                                                MealMonkeyTextStyles.mainBold(Colors.white, 18.0),
                                          ),
                                          onPressed: () async {
                                            if (snapshot.data) {
                                              _submit(context, _viewModel, flushbarHelper);
                                            }
                                          },
                                        );
                                      },
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
    );
  }

  /// Submit method for the join plan button. Verifys the code and sets the [viewModel] to
  /// contain the plan if it exists. Navigates to the recipes screen on completion using the
  /// [context].
  _submit(
    BuildContext context,
    _ViewModel viewModel,
    FlushbarUtil flushbarHelper,
  ) async {
    final form = _formKey.currentState;
    // viewModel.setLoading(true);

    if (form.validate()) {
      form.save();

      flushbarHelper
          .create(
            type: FlushbarType.INFO,
            message: "Joining group plan...",
          )
          .show(context);

      // Check that code is a code of a plan
      DocumentSnapshot codeResult = await Plan.checkPlanCode(_code);

      if (codeResult.exists) {
        // Update the plan's list of users to include the new user (the current one)
        Plan newPlan = Plan.from(codeResult);
        newPlan.users.add(PlanUser.fromRealUser(viewModel.user));

        // Update the stored plan to be the one requested by code
        viewModel.plan = newPlan;
        viewModel.setPlan(viewModel.plan);

        // Update the stored user to have this new plan
        viewModel.user.plan = codeResult.reference;
        viewModel.updateUser(viewModel.user);

        // Update the week in the Redux store
        viewModel.week = Week.from(await viewModel.plan.getCurrentWeek());
        viewModel.setWeek(viewModel.week);

        // Update the meals in the Redux store
        viewModel.meals = await viewModel.week.getMeals();
        viewModel.setMeals(viewModel.meals);

        // Update the new meals to include the new user ratings
        for (Meal meal in viewModel.meals) {
          await meal.addUserRating(viewModel.user.reference);
        }
        viewModel.setMeals(viewModel.meals);

        // Update the users setup status to true
        viewModel.user.setup = true;
        viewModel.updateUser(viewModel.user);

        // Send user to the main recipes page
        Navigator.pushReplacement(
          context,
          new MaterialPageRoute(builder: (context) => new MainPage()),
        );
        // viewModel.setLoading(false);
        flushbarHelper.dismissAll();
      } else {
        flushbarHelper.dismissAll();

        flushbarHelper
            .create(
              type: FlushbarType.WARNING,
              message: "Plan doesn't exist.",
              flushbarDuration: FlushbarDuration.SHORT,
            )
            .show(context);
      }
    }
  }
}

typedef UpdateUser(User user);
typedef SetPlan(Plan plan);
typedef SetWeek(Week week);
typedef SetMeals(List<Meal> meals);

/// Class used by the store connector to connect the store and
/// the local state of the application.
class _ViewModel {
  final UpdateUser updateUser;
  final SetPlan setPlan;
  final SetWeek setWeek;
  final SetMeals setMeals;
  bool loading;
  User user;
  Plan plan;
  Week week;
  List<Meal> meals;

  _ViewModel({
    this.updateUser,
    this.setPlan,
    this.setWeek,
    this.setMeals,
    this.user,
    this.plan,
    this.week,
    this.meals,
  });
}
