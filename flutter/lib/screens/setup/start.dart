import 'package:flutter/material.dart';
import 'package:mealmonkey/screens/setup/group.dart';
import 'package:mealmonkey/screens/util/colours.dart';
import 'package:mealmonkey/screens/util/fonts.dart';

class SetupStart extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new SetupStartState();
  }
}

class SetupStartState extends State<SetupStart> {
  @override

  /// Builds the budget setup screen using the build method with [context].
  /// Documentation can be found here:
  /// https://docs.flutter.io/flutter/widgets/State/build.html
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            // Image
            new Image.asset(
              'assets/images/main.png',
              fit: BoxFit.cover,
              height: MediaQuery.of(context).size.height * 0.37,
            ),
            // Page text
            new Container(
              padding: EdgeInsets.symmetric(vertical: 30.0),
              child: new Text(
                'Set up your Meal\nMonkey account in 4\n easy steps!',
                textAlign: TextAlign.center,
                style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.black, 22.0),
              ),
            ),
            new Container(
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: new Text(
                'Its never been so easy to plan\nmeals and shop. Enjoy the\n convenience of Meal Monkey',
                textAlign: TextAlign.center,
                style: MealMonkeyTextStyles.mainRegular(MealMonkeyColours.grey, 18.0),
              ),
            ),
            // Naviagation button to start setup process.
            new Container(
              padding: EdgeInsets.symmetric(vertical: 30.0),
              child: new MaterialButton(
                padding: EdgeInsets.all(13.0),
                elevation: 0.0,
                color: MealMonkeyColours.primary,
                child: new Text(
                  'GET STARTED',
                  textAlign: TextAlign.center,
                  style: MealMonkeyTextStyles.mainBold(Colors.white, 18.0),
                ),
                onPressed: () async {
                  Navigator.push(
                    context,
                    new MaterialPageRoute(
                      builder: (context) => new SetupGroup(),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
