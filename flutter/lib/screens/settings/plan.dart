//Packages
import 'dart:async';
import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter/material.dart';
import 'package:mealmonkey/firebase/models/meal.dart';
import 'package:mealmonkey/firebase/models/plan.dart';
import 'package:mealmonkey/firebase/models/user.dart';
import 'package:mealmonkey/firebase/models/week.dart';
import 'package:mealmonkey/firebase/support_models/planUser.dart';
import 'package:mealmonkey/screens/setup/group.dart';
import 'package:mealmonkey/screens/util/colours.dart';
import 'package:mealmonkey/screens/util/designElements.dart';
import 'package:mealmonkey/screens/util/flushbar.dart';
import 'package:mealmonkey/screens/util/fonts.dart';
import 'package:mealmonkey/screens/util/helpDialogs.dart';
import 'package:mealmonkey/state/action.dart';
import 'package:mealmonkey/state/appstate.dart';
import 'package:mealmonkey/util.dart';
import 'package:share/share.dart';

class UserPlanScreen extends StatefulWidget {
  @override
  createState() => new UserPlanScreenState();
}

class UserPlanScreenState extends State<UserPlanScreen> {
  int _budget;
  bool flushbarOpen = false;
  bool hasUnsaved = false;
  // ignore: unused_field
  ScaffoldFeatureController _bottomSheet;
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  /// Builds the recipe screen using the build method with [context].
  /// Documentation can be found here:
  /// https://docs.flutter.io/flutter/widgets/State/build.html
  Widget build(BuildContext context) {
    final FlushbarUtil flushbarHelper = FlushbarUtil.getInstance(StoreProvider.of(context));

    return StoreConnector<AppState, _ViewModel>(
      converter: (store) => _ViewModel(
            updateUser: (user) => store.dispatch(
                  UpdateUserAction(user: user),
                ),
            updatePlan: (plan) => store.dispatch(
                  UpdatePlanAction(plan: plan),
                ),
            updateLocalPlan: (plan) => store.dispatch(
                  UpdateLocalPlanAction(plan: plan),
                ),
            updateLocalWeek: (week) => store.dispatch(
                  UpdateLocalWeekAction(week: week),
                ),
            updateLocalMeals: (meals) => store.dispatch(
                  UpdateLocalMealsAction(meals: meals),
                ),
            setLoading: (loading) => store.dispatch(
                  SetLoadingAction(loading: loading),
                ),
            user: store.state.user,
            plan: store.state.plan,
            week: store.state.week,
            meals: store.state.meals,
            loading: store.state.loading,
            displayHelp: store.state.displayHelp,
          ),
      onInit: (store) => store.dispatch(new RequestPlanDataEventsAction()),
      onDispose: (store) => store.dispatch(new CancelPlanDataEventsAction()),
      builder: (context, viewModel) {
        return new WillPopScope(
          onWillPop: () => _hasSaved(
                context,
                viewModel,
                flushbarHelper,
              ), // Displays an alert dialog if there is unsaved data on the page
          child: Scaffold(
            appBar: new AppBar(
              centerTitle: false,
              title: new Text(
                'Plan',
                textAlign: TextAlign.left,
                style: MealMonkeyTextStyles.mainBold(Colors.white, 28.0),
              ),
              automaticallyImplyLeading: false,
              backgroundColor: MealMonkeyColours.primary,
              elevation: 0.0,
              leading: new IconButton(
                icon: new Icon(Icons.arrow_back_ios),
                onPressed: () {
                  final form = _formKey.currentState;
                  if (form.validate()) {
                    form.save();
                    if (_budget != viewModel.plan.budget) {
                      somethingChanged(viewModel, flushbarHelper);
                    }
                    Navigator.maybePop(context);
                  }
                },
              ),
              actions: <Widget>[
                // Save button
                new FlatButton(
                  padding: EdgeInsets.only(top: 10.0, right: 15.0),
                  child: new Text(
                    'Save',
                    style: MealMonkeyTextStyles.mainSemiBold(MealMonkeyColours.white, 16.0),
                  ),
                  onPressed: () async {
                    await _submit(
                        context, viewModel, flushbarHelper); // Saves the changed data on the page

                    // Flush bar informing the user about plan settings.
                    flushbarHelper
                        .create(
                          type: FlushbarType.INFO,
                          message:
                              "Changes to the plan\'s settings have been saved and will be reflected in next week\'s meals.",
                          flushbarDuration: FlushbarDuration.MEDIUM,
                        )
                        .show(context);
                  },
                )
              ],
            ),
            body: new Container(
              // Background Image
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage("assets/images/background_graphics.png"),
                  fit: BoxFit.fill,
                ),
              ),
              child: Form(
                key: this._formKey,
                child: new Stack(
                  children: <Widget>[
                    // Builds the plan screen using these different elements
                    new ListView(
                      children: <Widget>[
                        _invite(viewModel),
                        _members(viewModel, flushbarHelper),
                        _budgetTextField(viewModel, flushbarHelper),
                        _days(viewModel, flushbarHelper),
                        _labels(viewModel, flushbarHelper),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  /// Returns a [Container] with the plan code and the share button functionality.
  Widget _invite(_ViewModel viewModel) {
    return new Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child: new Column(
        children: <Widget>[
          new Container(
            alignment: Alignment(-1.0, 0.0),
            padding: EdgeInsets.symmetric(vertical: 10.0),
            child: new Text(
              'Invite',
              style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.black, 19.0),
            ),
          ),
          new Container(
            decoration: DesignUtil.boxDecoration,
            child: new Card(
              elevation: 0.0,
              child: new ListTile(
                leading: new Icon(IconData(0xea25, fontFamily: 'IonIcons')),
                title: new Text(viewModel.plan.code),
                trailing: Container(
                  width: 40.0,
                  child: new FlatButton(
                    onPressed: () {
                      Share.share("Join my plan on Meal Monkey, using the plan code " +
                          viewModel.plan.code.toString() +
                          ".\n\nMeal Monkey makes meal planning easy. Download at https://ios.mealmonkey.nz on iOS or https://android.mealmonkey.nz on Android.");
                    },
                    color: Colors.white,
                    padding: EdgeInsets.only(right: 3.0),
                    child: new Icon(
                      Platform.isIOS
                          ? CupertinoIcons.share
                          : Icons.share, // Changes the icon based on OS
                      color: MealMonkeyColours.darkGrey,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  /// Returns a [Container] with a list of all users in the plan.
  Widget _members(
    _ViewModel viewModel,
    FlushbarUtil flushbarHelper,
  ) {
    return new Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child: new Column(
        children: <Widget>[
          new Container(
            alignment: Alignment(-1.0, 0.0),
            padding: EdgeInsets.symmetric(vertical: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Text(
                  'Members',
                  style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.black, 19.0),
                ),
                viewModel.displayHelp
                    ? GestureDetector(
                        child: new Padding(
                          padding: EdgeInsets.only(right: 5.0),
                          child: new Icon(
                            Icons.help,
                            color: MealMonkeyColours.primary,
                          ),
                        ),
                        onTap: () {
                          // Displays help dialog for the plan page.
                          HelpDialogUtil.planSettingsDialog(context);
                        },
                      )
                    : new Container(),
              ],
            ),
          ),
          new Container(
            decoration: DesignUtil.boxDecoration,
            child: new Card(
              elevation: 0.0,
              child: new Column(
                  // Calls create member tile for each user in the plan.
                  children: viewModel.plan.users.map(
                (PlanUser planUser) {
                  return new Container(
                    decoration: new BoxDecoration(
                      border: new Border(bottom: new BorderSide(color: MealMonkeyColours.divider)),
                    ),
                    child: createMemberTile(viewModel, planUser, flushbarHelper),
                  );
                },
              ).toList()),
            ),
          )
        ],
      ),
    );
  }

  /// Returns a new [ListTile] containing the details of a [planUser].
  Widget createMemberTile(
    _ViewModel viewModel,
    PlanUser planUser,
    FlushbarUtil flushbarHelper,
  ) {
    return new ListTile(
      title: new Text(planUser.username),
      leading: Util.showPic(planUser), // Profile picture
      subtitle: new Text(planUser.email),
      onTap: () {
        // Action sheet
        showModalBottomSheet<void>(
          context: context,
          builder: (BuildContext context) {
            return new Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new ListTile(
                  title: new Text(
                    'Edit assigned days',
                    textAlign: TextAlign.center,
                    style: new TextStyle(fontSize: 20.0),
                  ),
                  onTap: () {
                    // Second action sheet displaying the days you can assign someone to.
                    showModalBottomSheet<void>(
                      context: context,
                      builder: (BuildContext context) {
                        return CreateStatefulSheet(
                          viewModel: viewModel,
                          user: planUser.userReference,
                          update:
                              (bool assigned, DocumentReference user, List<DocumentReference> day) {
                            setState(
                              () {
                                somethingChanged(viewModel, flushbarHelper);
                                assigned
                                    ? viewModel.plan.daysConfig.getDay(day).remove(user)
                                    : viewModel.plan.daysConfig.getDay(day).add(user);
                              },
                            );
                          },
                        );
                      },
                    );
                  },
                ),
                viewModel.user.email == planUser.email
                    ? Container(
                        height: DesignUtil.isIPhoneX(MediaQuery.of(context))
                            ? 100.0
                            : 60.0, // Adds a padding for iPhone X users
                        child: new ListTile(
                          title: new Text(
                            'Create your own plan',
                            textAlign: TextAlign.center,
                            style: new TextStyle(fontSize: 20.0, color: MealMonkeyColours.primary),
                          ),
                          onTap: () {
                            _leavePlanConfirmation(context, viewModel);
                          },
                        ),
                      )
                    : new Container(),
              ],
            );
          },
        );
      },
    );
  }

  /// Removes the user from their current plan.
  void _leavePlan(_ViewModel viewModel, BuildContext context) {
    // Set the users plan to null in the user object
    viewModel.user.plan = null;

    // Remove them from the plan for other users
    viewModel.plan.removeUserFromPlan(viewModel.user.reference);
    viewModel.updatePlan(viewModel.plan);

    // Remove the users rating from the week
    for (Meal meal in viewModel.meals) {
      meal.removeUserRating(viewModel.user.reference);
    }

    // Set their setup to false so if they exit the app
    viewModel.user.setup = false;
    viewModel.updateUser(viewModel.user);

    // Set their plan to null
    viewModel.plan = null;
    viewModel.updateLocalPlan(viewModel.plan);

    viewModel.week = null;
    viewModel.updateLocalWeek(viewModel.week);

    // Set the users meals to blank
    viewModel.meals = [];
    viewModel.updateLocalMeals(viewModel.meals);

    Navigator.pushAndRemoveUntil(
      context,
      new MaterialPageRoute(builder: (context) => new SetupGroup()),
      (_) => false,
    );
  }

  /// Returns a [showDialog] that displays an alert dialog prompting the user to confirm if
  /// they want to leave their current plan. Calls [_leavePlan] on confirmation.
  Future<Null> _leavePlanConfirmation(BuildContext context, _ViewModel viewModel) async {
    return showDialog<Null>(
      context: context,
      builder: (BuildContext context) {
        return new AlertDialog(
          title: new Text('Are you sure you want to create a new plan?'),
          content: new Text(
              'You can only be part of one plan at a time. Creating a new one will remove you from your current plan.'),
          actions: <Widget>[
            new FlatButton(
              child: new Text(
                "Cancel",
                style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.primary, 17.0),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text(
                'Leave Plan',
                style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.primary, 17.0),
              ),
              onPressed: () {
                _leavePlan(viewModel, context);
              },
            ),
          ],
        );
      },
    );
  }

  /// Checks to see if the user has unsaved changes before leaving the page. Calls
  /// [_prompt] if so.
  Future<bool> _hasSaved(
    BuildContext context,
    _ViewModel viewModel,
    FlushbarUtil flushbarHelper,
  ) async {
    if (!hasUnsaved) {
      return new Future<bool>.value(true);
    } else {
      return await _prompt(context, viewModel, flushbarHelper);
    }
  }

  /// Returns a [showDialog] confirming if the user wants to leave the page with unsaved changes.
  Future<bool> _prompt(
    BuildContext context,
    _ViewModel viewModel,
    FlushbarUtil flushbarHelper,
  ) {
    return showDialog(
      context: context,
      builder: (_) => new AlertDialog(
            title: new Text('You have unsaved changes in your plan.'),
            content: new Text('Do you want to save the changes made to your plan?'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(true), // Closes the dialog.
                child: new Text(
                  'No',
                  style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.primary, 17.0),
                ),
              ),
              new FlatButton(
                child: new Text(
                  'Yes',
                  style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.primary, 17.0),
                ),
                onPressed: () async {
                  await _submit(
                      context, viewModel, flushbarHelper); // Saves the changes on the plan
                  Navigator.of(context).pop(true);

                  // Flush bar informing the user about plan settings.
                  flushbarHelper
                      .create(
                        type: FlushbarType.INFO,
                        message:
                            "Changes to the plan\'s settings have been saved and will be refelcted in next week\'s meals.",
                        flushbarDuration: FlushbarDuration.MEDIUM,
                      )
                      .show(context);
                },
              ),
            ],
          ),
    );
  }

  /// Validates the changes and saves them to the [viewModel].
  Future _submit(
    BuildContext context,
    _ViewModel viewModel,
    FlushbarUtil flushbarHelper,
  ) async {
    final form = _formKey.currentState;

    Flushbar savingBar =
        flushbarHelper.create(type: FlushbarType.INFO, message: "Saving plan settings...");

    FocusScope.of(context).requestFocus(new FocusNode());

    if (form.validate()) {
      form.save();

      viewModel.plan.budget = _budget;
    }

    viewModel.updatePlan(viewModel.plan);
    savingBar.dismiss();

    if (flushbarOpen) {
      Navigator.of(context).pop(false);
      Navigator.of(context).pop(false);
    } else {
      Navigator.of(context).pop(false);
    }
  }

  /// Returns a [Container] with a list of all dietary requirements. Calls [_buildLabel] on
  /// each dietary requirement.
  Widget _labels(_ViewModel viewModel, FlushbarUtil flushbarHelper) {
    // FIXME: Improve into a class
    List<String> allLabels = [
      "Vegetarian",
      "Vegan",
      "Gluten Free",
      "Nut Free",
      "Soy Free",
      "Dairy Free",
    ];

    return new Container(
      padding: EdgeInsets.all(10.0),
      child: new Column(
        children: <Widget>[
          new Container(
            alignment: Alignment(-1.0, 0.0),
            padding: EdgeInsets.symmetric(vertical: 10.0),
            child: new Text('Dietary Requirements',
                style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.black, 19.0)),
          ),
          new Container(
            decoration: DesignUtil.boxDecoration,
            child: new Card(
              elevation: 0.0,
              child: new Column(
                children: allLabels
                    .map<Widget>((item) => createLabel(viewModel, item, flushbarHelper))
                    .toList(),
              ),
            ),
          )
        ],
      ),
    );
  }

  /// Returns a [Container] with a list tile for the input [label].
  Widget createLabel(_ViewModel viewModel, String label, FlushbarUtil flushbarHelper) {
    bool labelIsChecked = viewModel.plan.labels.contains(label);

    return Container(
      decoration: new BoxDecoration(
        border: new Border(bottom: new BorderSide(color: MealMonkeyColours.divider)),
      ),
      child: new ListTile(
        title: new Text(
          label,
          style: labelIsChecked
              ? MealMonkeyTextStyles.mainRegular(MealMonkeyColours.primary, 15.0)
              : MealMonkeyTextStyles.mainRegular(MealMonkeyColours.black, 15.0),
        ),
        trailing: labelIsChecked
            ? new Icon(
                const IconData(0xe849, fontFamily: "IonIcons"),
                color: MealMonkeyColours.primary,
              )
            : null,
        onTap: () {
          setState(
            () {
              FocusScope.of(context).requestFocus(new FocusNode());
              labelIsChecked = !labelIsChecked;

              if (labelIsChecked) {
                viewModel.plan.labels.add(label);
              } else {
                viewModel.plan.labels.removeWhere((item) => item == label);
              }
              somethingChanged(viewModel, flushbarHelper);
            },
          );
        },
      ),
    );
  }

  /// Displays a [Flushbar] the first time anything is changed on the plan screen.
  somethingChanged(
    _ViewModel viewModel,
    FlushbarUtil flushbarHelper,
  ) {
    if (!hasUnsaved) {
      Flushbar flushbar = flushbarHelper.create(
        type: FlushbarType.INFO,
        message: 'Changes to the plan\'s settings will be reflected in next week\'s meals.',
        flushbarDuration: FlushbarDuration.MEDIUM,
      );

      flushbar
        ..onStatusChanged = (FlushbarStatus status) {
          if (status == FlushbarStatus.DISMISSED) {
            flushbarOpen = false;
          }
        }
        ..show(context);

      hasUnsaved = true;
      flushbarOpen = true;
    }
  }

  /// Returns a [Container] with a list of selectable days. Calls [createDay] for each
  /// day of the week.
  Widget _days(_ViewModel viewModel, FlushbarUtil flushbarHelper) {
    return new Container(
      padding: EdgeInsets.all(10.0),
      child: new Column(
        children: <Widget>[
          new Container(
            alignment: Alignment(-1.0, 0.0),
            padding: EdgeInsets.symmetric(vertical: 10.0),
            child: new Text('Days',
                style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.black, 19.0)),
          ),
          new Container(
            decoration: DesignUtil.boxDecoration,
            child: new Card(
              elevation: 0.0,
              child: new Column(
                children: <Widget>[
                  createDay(viewModel, viewModel.plan.daysConfig.mondayIsSelected, "Monday",
                      flushbarHelper),
                  createDay(viewModel, viewModel.plan.daysConfig.tuesdayIsSelected, "Tuesday",
                      flushbarHelper),
                  createDay(viewModel, viewModel.plan.daysConfig.wednesdayIsSelected, "Wednesday",
                      flushbarHelper),
                  createDay(viewModel, viewModel.plan.daysConfig.thursdayIsSelected, "Thursday",
                      flushbarHelper),
                  createDay(viewModel, viewModel.plan.daysConfig.fridayIsSelected, "Friday",
                      flushbarHelper),
                  createDay(viewModel, viewModel.plan.daysConfig.saturdayIsSelected, "Saturday",
                      flushbarHelper),
                  createDay(viewModel, viewModel.plan.daysConfig.sundayIsSelected, "Sunday",
                      flushbarHelper),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  /// Returns a [Container] with a list tile containing the information from the input
  /// [dayName] and [dayIsSelected].
  Widget createDay(
    _ViewModel viewModel,
    bool dayIsSelected,
    String dayName,
    FlushbarUtil flushbarHelper,
  ) {
    return Container(
      decoration: new BoxDecoration(
        border: new Border(
          bottom: new BorderSide(color: MealMonkeyColours.divider),
        ),
      ),
      child: new ListTile(
        title: new Text(
          dayName,
          style: dayIsSelected
              ? MealMonkeyTextStyles.mainRegular(MealMonkeyColours.primary, 15.0)
              : MealMonkeyTextStyles.mainRegular(MealMonkeyColours.black, 15.0),
        ),
        trailing: dayIsSelected
            ? new Icon(
                const IconData(0xe849, fontFamily: "IonIcons"),
                color: MealMonkeyColours.primary,
              )
            : null,
        onTap: () {
          setState(
            () {
              FocusScope.of(context).requestFocus(
                new FocusNode(),
              );
              dayIsSelected = !dayIsSelected;

              switch (dayName) {
                case "Monday":
                  if (dayIsSelected)
                    viewModel.plan.daysConfig.monday = new List<DocumentReference>();
                  else
                    viewModel.plan.daysConfig.monday = null;
                  break;
                case "Tuesday":
                  if (dayIsSelected)
                    viewModel.plan.daysConfig.tuesday = List<DocumentReference>();
                  else
                    viewModel.plan.daysConfig.tuesday = null;
                  break;
                case "Wednesday":
                  if (dayIsSelected)
                    viewModel.plan.daysConfig.wednesday = List<DocumentReference>();
                  else
                    viewModel.plan.daysConfig.wednesday = null;
                  break;
                case "Thursday":
                  if (dayIsSelected)
                    viewModel.plan.daysConfig.thursday = List<DocumentReference>();
                  else
                    viewModel.plan.daysConfig.thursday = null;
                  break;
                case "Friday":
                  if (dayIsSelected)
                    viewModel.plan.daysConfig.friday = List<DocumentReference>();
                  else
                    viewModel.plan.daysConfig.friday = null;
                  break;
                case "Saturday":
                  if (dayIsSelected)
                    viewModel.plan.daysConfig.saturday = List<DocumentReference>();
                  else
                    viewModel.plan.daysConfig.saturday = null;
                  break;
                case "Sunday":
                  if (dayIsSelected)
                    viewModel.plan.daysConfig.sunday = List<DocumentReference>();
                  else
                    viewModel.plan.daysConfig.sunday = null;
                  break;
                default:
              }
              somethingChanged(viewModel, flushbarHelper);
            },
          );
        },
      ),
    );
  }

  /// Returns a [Container] containing the budget text field.
  Widget _budgetTextField(_ViewModel viewModel, FlushbarUtil flushbarHelper) {
    TextEditingController budgetController = new TextEditingController.fromValue(
      new TextEditingValue(
        text: viewModel.plan.budget.toString(),
      ),
    );

    budgetController.addListener(() {
      // print(budgetController.value.text);
      if (int.tryParse(budgetController.value.text) != viewModel.plan.budget) {
        somethingChanged(viewModel, flushbarHelper);
      }
    });

    return new Container(
      padding: EdgeInsets.all(10.0),
      child: new Column(
        children: <Widget>[
          new Container(
            padding: EdgeInsets.symmetric(vertical: 10.0),
            alignment: Alignment(-1.0, 0.0),
            child: new Text(
              'Budget',
              style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.black, 19.0),
            ),
          ),
          new Container(
            child: DesignUtil.textFieldGenerator(
              tff: new TextFormField(
                keyboardType: TextInputType.number,
                decoration: const InputDecoration(
                  border: InputBorder.none,
                  icon: Icon(Icons.attach_money),
                  hintText: 'Budget',
                ),
                validator: (val) {
                  if (val.isEmpty || int.parse(val) <= 0) {
                    return 'You must set a plan budget that is a positive number.';
                  }
                  if (val.contains('.')) {
                    return 'Budget can\'t include a decimal point.';
                  }
                },
                onSaved: (val) {
                  _budget = int.parse(val);

                  if (_budget != viewModel.plan.budget) {
                    hasUnsaved = true;
                  }
                },
                autocorrect: false,
                controller: budgetController,
              ),
              context: context,
              width: false,
            ),
          )
        ],
      ),
    );
  }
}

/// Helper class for adding stateful elements to the modal sheet.
class CreateStatefulSheet extends StatefulWidget {
  CreateStatefulSheet({Key key, this.viewModel, this.user, this.update}) : super(key: key);

  final _ViewModel viewModel;
  final DocumentReference user;
  final void Function(bool assigned, DocumentReference user, List<DocumentReference> day) update;

  @override
  State<StatefulWidget> createState() => new CreateStatefulSheetState();
}

class CreateStatefulSheetState extends State<CreateStatefulSheet> {
  @override
  void initState() {
    super.initState();
  }

  @override

  /// Builds the recipe screen using the build method with [context].
  /// Documentation can be found here:
  /// https://docs.flutter.io/flutter/widgets/State/build.html
  Widget build(BuildContext context) {
    return new ListView(
      children: List.generate(
        7,
        (int index) {
          bool cookingDay = false;

          List<DocumentReference> day = new List<DocumentReference>();
          String dayName = 'no day selected :\'(';
          switch (index) {
            case 0:
              dayName = "Monday";
              if (widget.viewModel.plan.daysConfig.mondayIsSelected) {
                day = widget.viewModel.plan.daysConfig.monday;
                cookingDay = true;
              }
              break;
            case 1:
              dayName = "Tuesday";
              if (widget.viewModel.plan.daysConfig.tuesdayIsSelected) {
                day = widget.viewModel.plan.daysConfig.tuesday;
                cookingDay = true;
              }
              break;
            case 2:
              dayName = "Wednesday";
              if (widget.viewModel.plan.daysConfig.wednesdayIsSelected) {
                day = widget.viewModel.plan.daysConfig.wednesday;
                cookingDay = true;
              }
              break;
            case 3:
              dayName = "Thursday";
              if (widget.viewModel.plan.daysConfig.thursdayIsSelected) {
                day = widget.viewModel.plan.daysConfig.thursday;
                cookingDay = true;
              }
              break;
            case 4:
              dayName = "Friday";
              if (widget.viewModel.plan.daysConfig.fridayIsSelected) {
                day = widget.viewModel.plan.daysConfig.friday;
                cookingDay = true;
              }
              break;
            case 5:
              dayName = "Saturday";
              if (widget.viewModel.plan.daysConfig.saturdayIsSelected) {
                day = widget.viewModel.plan.daysConfig.saturday;
                cookingDay = true;
              }
              break;
            case 6:
              dayName = "Sunday";
              if (widget.viewModel.plan.daysConfig.sundayIsSelected) {
                day = widget.viewModel.plan.daysConfig.sunday;
                cookingDay = true;
              }
              break;
            default:
          }
          bool selected = day.contains(widget.user);
          return new Container(
            decoration: new BoxDecoration(
                border: new Border(bottom: new BorderSide(color: MealMonkeyColours.divider))),
            child: new ListTile(
              title: new Text(
                dayName,
                style: cookingDay
                    ? selected
                        ? MealMonkeyTextStyles.mainRegular(MealMonkeyColours.primary, 15.0)
                        : MealMonkeyTextStyles.mainRegular(MealMonkeyColours.black, 15.0)
                    : MealMonkeyTextStyles.mainRegular(MealMonkeyColours.grey, 15.0),
              ),
              trailing: selected
                  ? new Icon(
                      const IconData(0xe849, fontFamily: "IonIcons"),
                      color: MealMonkeyColours.primary,
                    )
                  : null,
              onTap: () {
                setState(
                  () {
                    // ignore: unnecessary_statements
                    cookingDay ? widget.update(selected, widget.user, day) : null;
                  },
                );
              },
            ),
          );
        },
      ),
    );
  }
}

typedef UpdateUser(User user);
typedef UpdatePlan(Plan plan);
typedef UpdateLocalPlan(Plan plan);
typedef UpdateLocalWeek(Week week);
typedef UpdateLocalMeals(List<Meal> meals);
typedef SetLoading(bool loading);

class _ViewModel {
  final UpdateUser updateUser;
  final UpdatePlan updatePlan;
  final UpdateLocalPlan updateLocalPlan;
  final UpdateLocalWeek updateLocalWeek;
  final UpdateLocalMeals updateLocalMeals;
  final SetLoading setLoading;
  bool loading;
  bool displayHelp;
  User user;
  Plan plan;
  Week week;
  List<Meal> meals;

  /// Class used by the store connector to connect firebase and
  /// the local state of the application.
  _ViewModel({
    this.updateUser,
    this.updatePlan,
    this.updateLocalPlan,
    this.updateLocalWeek,
    this.updateLocalMeals,
    this.setLoading,
    this.loading,
    this.displayHelp,
    this.user,
    this.plan,
    this.week,
    this.meals,
  });
}
