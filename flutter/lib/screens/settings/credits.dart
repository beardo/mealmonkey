import 'package:flutter/material.dart';
import 'package:mealmonkey/screens/util/colours.dart';
import 'package:mealmonkey/screens/util/designElements.dart';
import 'package:mealmonkey/screens/util/fonts.dart';

class CreditsScreen extends StatefulWidget {
  @override
  createState() => new CreditsScreenState();
}

class CreditsScreenState extends State<CreditsScreen> {

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  /// Builds the recipe screen using the build method with [context].
  /// Documentation can be found here:
  /// https://docs.flutter.io/flutter/widgets/State/build.html
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        centerTitle: false,
        title: new Text('Credits',
            textAlign: TextAlign.left, style: MealMonkeyTextStyles.mainBold(Colors.white, 28.0)),
        automaticallyImplyLeading: false,
        backgroundColor: MealMonkeyColours.primary,
        elevation: 0.0,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.maybePop(context);
          },
        ),
      ),
      body: new Container(
        // Background Image
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage("assets/images/background_graphics.png"),
            fit: BoxFit.fill,
          ),
        ),
        child: ListView(
          children: <Widget>[
            _credits(),
          ],
        ),
      ),
    );
  }

  /// Returns a [Container] with text attributions to those who helped
  /// with the development of the application.
  Widget _credits() {
    return new Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
      child: Container(
        width: MediaQuery.of(context).size.width,
        decoration: DesignUtil.boxDecoration,
        child: new Card(
          elevation: 0.0,
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: <Widget>[
                new Container(
                  margin: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 20.0),
                  child: new Center(
                    // Background image
                    child: new Image.asset(
                      'assets/images/main.png',
                      fit: BoxFit.cover,
                      height: MediaQuery.of(context).size.height * 0.17,
                    ),
                  ),
                ),
                new Text(
                  'Made with ☕️ by the Meal Monkey Team\n',
                  style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.darkGrey, 20.0),
                  textAlign: TextAlign.center,
                ),
                new Text(
                  'Recipes kindly sourced from NZ\'s Favourite Recipes and Madam Lu\'s.\n',
                  style: MealMonkeyTextStyles.mainRegular(MealMonkeyColours.darkGrey, 16.0),
                  textAlign: TextAlign.center,
                ),
                new Text(
                  "Special thanks to Doctor Sherlock Licorish, Jeroen Meijer and others from the Flutter Study Group, and Warren Walker.",
                  style: MealMonkeyTextStyles.mainRegular(MealMonkeyColours.darkGrey, 16.0),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
