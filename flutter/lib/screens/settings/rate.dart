import 'package:flutter/material.dart';
import 'package:mealmonkey/screens/util/colours.dart';
import 'package:mealmonkey/screens/util/fonts.dart';

class RateScreen extends StatefulWidget {
  @override
  createState() => new RateScreenState();
}

/// Will connect and implement this page once the app is in full release on the app store.


class RateScreenState extends State<RateScreen> {

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  /// Builds the recipe screen using the build method with [context].
  /// Documentation can be found here:
  /// https://docs.flutter.io/flutter/widgets/State/build.html
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: new AppBar(
        centerTitle: false,
        title: new Text('Rate',
            textAlign: TextAlign.left, style: MealMonkeyTextStyles.mainBold(Colors.white, 28.0)),
        automaticallyImplyLeading: false,
        backgroundColor: MealMonkeyColours.primary,
        elevation: 0.0,
        leading: new IconButton(
            icon: new Icon(Icons.arrow_back_ios),
            onPressed: () {
              Navigator.maybePop(context);
            }),
      ),
      body: new Container(
        // Background Image
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage("assets/images/background_graphics.png"),
            fit: BoxFit.fill,
          ),
        ),
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            children: <Widget>[
                new Card(
                  child: new Text(
                    "Coming Soon...",
                    style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.black, 30.0),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}