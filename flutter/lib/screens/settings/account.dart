import 'package:firebase_auth/firebase_auth.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:mealmonkey/firebase/models/plan.dart';
import 'package:mealmonkey/firebase/models/user.dart';
import 'package:mealmonkey/screens/util/colours.dart';
import 'package:mealmonkey/screens/util/designElements.dart';
import 'package:mealmonkey/screens/util/flushbar.dart';
import 'package:mealmonkey/screens/util/fonts.dart';
import 'package:mealmonkey/state/action.dart';
import 'package:mealmonkey/state/appstate.dart';
import 'package:mealmonkey/util.dart';

class AccountSettings extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new AccountSettingsState();
  }
}

class AccountSettingsState extends State<AccountSettings> {
  // Custom icons
  Icon person = new Icon(const IconData(0xe8d7, fontFamily: "IonIcons"));
  Icon lock = new Icon(const IconData(0xe8b1, fontFamily: "IonIcons"));

  // Form keys
  final GlobalKey<FormState> _usernameFormKey = new GlobalKey<FormState>();
  final GlobalKey<FormState> _emailFormKey = new GlobalKey<FormState>();
  final GlobalKey<FormState> _passwordFormKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  // Local variables
  String _email;
  String _username;
  String _password;

  /// Builds the recipe screen using the build method with [context].
  /// Documentation can be found here:
  /// https://docs.flutter.io/flutter/widgets/State/build.html
  @override
  Widget build(BuildContext context) {
    final FlushbarUtil flushbarHelper = FlushbarUtil.getInstance(StoreProvider.of(context));

    return StoreConnector<AppState, _ViewModel>(
      converter: (store) => _ViewModel(
            updateUser: (user) => store.dispatch(
                  UpdateUserAction(user: user),
                ),
            updatePlan: (plan) => store.dispatch(
                  UpdatePlanAction(plan: plan),
                ),
            updateDisplayHelp: (displayHelp) => store.dispatch(
                  SetDisplayHelpAction(displayHelp: displayHelp),
                ),
            user: store.state.user,
            plan: store.state.plan,
            displayHelp: store.state.displayHelp,
          ),
      builder: (context, viewModel) => Scaffold(
            resizeToAvoidBottomPadding: true,
            key: _scaffoldKey,
            appBar: new AppBar(
              centerTitle: false,
              title: new Text(
                'Account',
                textAlign: TextAlign.left,
                style: MealMonkeyTextStyles.mainBold(Colors.white, 28.0),
              ),
              automaticallyImplyLeading: false,
              backgroundColor: MealMonkeyColours.primary,
              elevation: 0.0,
              leading: new IconButton(
                icon: new Icon(Icons.arrow_back_ios),
                onPressed: () {
                  Navigator.maybePop(context);
                },
              ),
            ),
            body: new Container(
              // Background image
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage("assets/images/background_graphics.png"),
                  fit: BoxFit.fill,
                ),
              ),
              child: new ListView(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(left: Util.pageWidthPadding, top: 20.0),
                    child: Text(
                      "Help Dialogs",
                      style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.black, 19.0),
                    ),
                  ),

                  new Container(
                    padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                    child: new Container(
                      width: MediaQuery.of(context).size.width * 0.80,
                      decoration: BoxDecoration(
                        boxShadow: [
                          DesignUtil.boxShadow,
                        ],
                        color: Color.fromRGBO(255, 255, 255, 1.0),
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      child: SwitchListTile(
                        title: Text('Show help dialogs'),
                        value: viewModel.displayHelp,
                        onChanged: (bool value) {
                          viewModel.updateDisplayHelp(value);
                        },
                        activeColor: MealMonkeyColours.primary,
                        secondary: Icon(
                          Icons.help,
                        ),
                      ),
                    ),
                  ),
                  //Username text field
                  Padding(
                    padding: EdgeInsets.only(left: Util.pageWidthPadding, top: 20.0),
                    child: Text("Change Username",
                        style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.black, 19.0)),
                  ),
                  Form(
                    key: _usernameFormKey,
                    child: DesignUtil.textFieldGenerator(
                      tff: createTextField(type: "username", viewModel: viewModel),
                      context: context,
                    ),
                  ),
                  //Email text field
                  Padding(
                    padding: EdgeInsets.only(left: Util.pageWidthPadding, top: 20.0),
                    child: Text(
                      "Change Email",
                      style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.black, 19.0),
                    ),
                  ),
                  Form(
                    key: _emailFormKey,
                    child: DesignUtil.textFieldGenerator(
                      tff: createTextField(type: "email", viewModel: viewModel),
                      context: context,
                    ),
                  ),
                  !viewModel.user.providerAccount
                      ? displayPasswordChange(
                          context,
                          viewModel.user.email,
                          viewModel,
                        )
                      : new Container(),
                  //Save button
                  Padding(
                    padding: EdgeInsets.only(
                      left: Util.pageWidthPadding,
                      right: Util.pageWidthPadding,
                      top: 10.0,
                    ),
                    child: new RaisedButton(
                      padding: EdgeInsets.only(top: 13.0, bottom: 13.0),
                      elevation: 0.0,
                      color: MealMonkeyColours.primary,
                      child: new Text(
                        'UPDATE DETAILS',
                        textAlign: TextAlign.center,
                        style: MealMonkeyTextStyles.mainBold(Colors.white, 18.0),
                      ),
                      onPressed: () async {
                        _submit(context, viewModel, flushbarHelper);
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
    );
  }

  /// Returns a [TextFormField] depending on the input [type]. Initialises thier default
  /// values from the [viewModel].
  Widget createTextField({
    @required String type,
    @optionalTypeArgs _ViewModel viewModel,
  }) {
    if (type == "email") {
      return TextFormField(
        keyboardType: TextInputType.emailAddress,
        initialValue: viewModel.user.email,
        decoration: const InputDecoration(
          border: InputBorder.none,
          icon: Icon(Icons.email),
          hintText: 'Email',
        ),
        validator: (val) => val.isEmpty ? 'Email can\'t be empty.' : null,
        onSaved: (val) => _email = val,
      );
    } else if (type == "username") {
      return new TextFormField(
        keyboardType: TextInputType.text,
        initialValue: viewModel.user.username,
        decoration: const InputDecoration(
          border: InputBorder.none,
          icon: Icon(Icons.person),
          hintText: 'Username',
        ),
        validator: (val) {
          if (val.isEmpty) return 'Username can\'t be empty.';
          return null;
        },
        onSaved: (val) => _username = val,
        autocorrect: false,
      );
    } else if (type == "password") {
      return new TextFormField(
        keyboardType: TextInputType.text,
        initialValue: "",
        decoration: const InputDecoration(
          border: InputBorder.none,
          icon: Icon(Icons.lock),
          hintText: 'Password',
        ),
        validator: (val) {
          return null;
        },
        obscureText: true,
        onSaved: (val) => _password = val,
        autocorrect: false,
      );
    }
    return Container();
  }

  /// Returns a [FlatButton] that calleds [_changePassword] if the user is not logged with google
  /// or facebook.
  Widget displayPasswordChange(
    BuildContext context,
    String email,
    _ViewModel viewModel,
  ) {
    return new Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        // Password text field
        Padding(
          padding: EdgeInsets.only(left: Util.pageWidthPadding, top: 20.0),
          child: Text("Change Password",
              style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.black, 19.0)),
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          child: Form(
            key: _passwordFormKey,
            child: DesignUtil.textFieldGenerator(
              tff: createTextField(type: "password"),
              context: context,
            ),
          ),
        ),
      ],
    );
  }

  /// Updates the the user with the username and email contained in the local form keys.
  Future<void> _submit(
    BuildContext context,
    _ViewModel viewModel,
    FlushbarUtil flushbarHelper,
  ) async {
    flushbarHelper.dismissAll();
    flushbarHelper
        .create(
          type: FlushbarType.INFO,
          message: "Updating your account details.",
          flushbarDuration: FlushbarDuration.UNLIMITED,
        )
        .show(context);

    final emailForm = _emailFormKey.currentState;
    final usernameForm = _usernameFormKey.currentState;
    final passwordForm = _passwordFormKey.currentState;

    bool usernameChanged = usernameForm.validate();
    bool emailChanged = emailForm.validate();
    bool passwordChanged = passwordForm.validate();

    if (emailChanged || usernameChanged || passwordChanged) {
      // Get current FirebaseAuth user information
      FirebaseUser firebaseUser = await FirebaseAuth.instance.currentUser();

      try {
        if (usernameChanged) {
          usernameForm.save();
          print('username');
          if (_username != null && _username != viewModel.user.username) {
            // Update Redux user
            viewModel.user.username = _username;
            viewModel.updateUser(viewModel.user);

            // Update Plan that user is in
            viewModel.plan.users.forEach((planUser) {
              if (planUser.userReference == viewModel.user.reference) {
                planUser.username = _username;
              }
            });
            viewModel.updatePlan(viewModel.plan);
          }
        }
        if (emailChanged) {
          emailForm.save();
          print('email');
          if (_email != null && _email != firebaseUser.email) {
            // Update FirebaseAuth user
            await firebaseUser.updateEmail(_email);

            // Update Redux user
            viewModel.user.email = _email;
            viewModel.updateUser(viewModel.user);

            // Update Plan that user is in
            viewModel.plan.users.forEach((planUser) {
              if (planUser.userReference == viewModel.user.reference) {
                planUser.email = _email;
              }
            });
            viewModel.updatePlan(viewModel.plan);
          }
        }
        if (passwordChanged) {
          passwordForm.save();
          print('password');
          if (_password.isNotEmpty) {
            // try {
            FirebaseUser currentUser = await FirebaseAuth.instance.currentUser();
            await currentUser.updatePassword(_password);
            // } catch (e) {
            //   print(e);
            // }
          }
        }

        flushbarHelper.dismissAll();

        Flushbar flushbar = flushbarHelper.create(
          type: FlushbarType.SUCCESS,
          message: "Your account has been updated.",
          flushbarDuration: FlushbarDuration.SHORT,
        );

        flushbar
          ..onStatusChanged = (FlushbarStatus status) {
            if (status == FlushbarStatus.DISMISSED) {
              Navigator.pop(context);
            }
          }
          ..show(context);
      } catch (e) {
        flushbarHelper.dismissAll();
        try {
          // Show the error message in a snackbar
          String errorMessage = e.message.toString().startsWith('FIR') ? e.details : e.message;
          flushbarHelper
              .create(
                type: FlushbarType.WARNING,
                message: errorMessage,
                flushbarDuration: FlushbarDuration.SHORT,
              )
              .show(context);
        } catch (e) {
          // Show generic message if the error has no error message
          String errorMessage = "An error occured while updating your account details.";
          flushbarHelper
              .create(
                type: FlushbarType.ERROR,
                message: errorMessage,
                flushbarDuration: FlushbarDuration.SHORT,
              )
              .show(context);
        }
      }
    }
    flushbarHelper.dismissAll();
  }
}

typedef UpdateUser(User user);
typedef UpdatePlan(Plan plan);
typedef UpdateDisplayHelp(bool displayHelp);

/// Class used by the store connector to connect firebase with
/// the local state of the application.
class _ViewModel {
  final UpdateUser updateUser;
  final UpdatePlan updatePlan;
  final UpdateDisplayHelp updateDisplayHelp;
  User user;
  Plan plan;
  bool displayHelp;

  _ViewModel({
    this.updateUser,
    this.updatePlan,
    this.updateDisplayHelp,
    this.user,
    this.plan,
    this.displayHelp,
  });
}
