import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:mealmonkey/firebase/models/meal.dart';
import 'package:mealmonkey/firebase/models/week.dart';
import 'package:mealmonkey/firebase/support_models/department.dart';
import 'package:mealmonkey/firebase/support_models/ingredient.dart';
import 'package:mealmonkey/firebase/support_models/ingredientStatus.dart';
import 'package:mealmonkey/screens/util/colours.dart';
import 'package:mealmonkey/screens/util/designElements.dart';
import 'package:mealmonkey/screens/util/fonts.dart';
import 'package:mealmonkey/screens/util/helpDialogs.dart';
import 'package:mealmonkey/state/action.dart';
import 'package:mealmonkey/state/appstate.dart';
import 'package:mealmonkey/util.dart';

class IngredientScreen extends StatefulWidget {
  createState() => new IngredientScreenState();
}

class IngredientScreenState extends State<IngredientScreen> {
  bool pulled = false;
  bool alternateList = true;

  final currency = new NumberFormat("#,##0.00", "en_US");
  final quantity = new NumberFormat("#,##0", "en_US");

  @override

  /// Builds the ingredients screen using the build method with [context].
  /// Documentation can be found here:
  /// https://docs.flutter.io/flutter/widgets/State/build.html
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: (store) => _ViewModel(
            checkIngredient: (week, meals) {
              store.dispatch(UpdateWeekAction(week: week));
              store.dispatch(UpdateMealsAction(meals: meals));
            },
            setFirstBoxChecked: (firstBoxChecked) {
              store.dispatch(SetFirstBoxCheckedAction(firstBoxChecked: firstBoxChecked));
            },
            week: store.state.week,
            meals: store.state.meals,
            displayHelp: store.state.displayHelp,
            firstBoxChecked: store.state.firstBoxChecked,
          ),
      onInit: (store) => store.dispatch(new RequestWeekDataEventsAction()),
      onDispose: (store) => store.dispatch(new CancelWeekDataEventsAction()),
      builder: (context, viewModel) => Scaffold(
            appBar: new AppBar(
              toolbarOpacity: 0.0,
              backgroundColor: MealMonkeyColours.primary,
              centerTitle: false,
              title: new FittedBox(
                fit: BoxFit.scaleDown,
                child: Text('Grab Some Food',
                    style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.white, 28.0)),
              ),
              automaticallyImplyLeading: false,
              elevation: 0.0,
              actions: <Widget>[
                new Container(
                  padding: EdgeInsets.only(top: 25.0, right: Util.pageWidthPadding),
                  child: new Text(Util.countString(_countIngredients(viewModel), 'ingredient'),
                      style: MealMonkeyTextStyles.mainSemiBold(MealMonkeyColours.white, 14.0)),
                )
              ],
            ),
            body: new Container(
                decoration: new BoxDecoration(
                  image: new DecorationImage(
                    image: new AssetImage("assets/images/background_graphics.png"),
                    fit: BoxFit.fill,
                  ),
                ),
                child: viewModel.week == null ? new Container() : _buildDepartments(viewModel)),
          ),
    );
  }

  /// Returns a [ListView] containing each department using the ingredients from the [viewModel].
  Widget _buildDepartments(_ViewModel viewModel) {
    // Sort departments
    viewModel.week.departments.sort(
      (Department dep1, Department dep2) => Department.sortDepartments(dep1.name, dep2.name),
    );

    return new ListView(
      children: <Widget>[
        new Container(
          padding: EdgeInsets.all(Util.pageWidthPadding),
          child: Container(
            decoration: DesignUtil.boxDecoration,
            child: new Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              elevation: 0.0,
              child: new Stack(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(bottom: 15.0, left: 10.0, right: 10.0),
                    child: new Column(
                        children: viewModel.week.departments.map((Department department) {
                      alternateList = false;
                      return new Column(
                        children: <Widget>[
                          new Container(
                            height: 50.0,
                            child: new ListTile(
                              title: new Text(
                                department.name.toUpperCase(),
                                textAlign: TextAlign.left,
                                style: MealMonkeyTextStyles.mainSemiBold(
                                    MealMonkeyColours.darkGrey, 17.0),
                              ),
                            ),
                          ),
                          new Container(
                            padding: EdgeInsets.symmetric(horizontal: 12.0),
                            decoration: new BoxDecoration(),
                            child: ListView(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                children: <Widget>[
                                  _buildIngredient(department.ingredients, department, viewModel)
                                ]),
                          )
                        ],
                      );
                    }).toList()),
                  ),
                  viewModel.displayHelp
                      ? new Positioned(
                          top: 5.0,
                          right: 10.0,
                          child: new IconButton(
                            icon: new Icon(Icons.help),
                            color: MealMonkeyColours.primary,
                            onPressed: () {
                              HelpDialogUtil.stapleDialog(context);
                            },
                          ),
                        )
                      : new Container(),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  /// Builds each ingredient using the input [ingredient], [department], and the [viewModel].
  Widget _buildIngredient(
    List<Ingredient> ingredients,
    Department department,
    _ViewModel viewModel,
  ) {
    return new ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: ingredients.length,
      itemBuilder: (BuildContext context, index) {
        Icon ingredientIcon = new Icon(
          Icons.help_outline,
          color: MealMonkeyColours.primary,
        );

        if (ingredients[index].checked == IngredientStatus.CHECKED) {
          ingredientIcon = new Icon(
            Icons.check_box,
            color: MealMonkeyColours.primary,
          );
        } else if (ingredients[index].checked == IngredientStatus.NOT_CHECKED) {
          ingredientIcon = new Icon(
            Icons.check_box_outline_blank,
            color: MealMonkeyColours.primary,
          );
        } else if (ingredients[index].checked == IngredientStatus.DASHED) {
          ingredientIcon = new Icon(
            Icons.indeterminate_check_box,
            color: MealMonkeyColours.primary,
          );
        }

        return GestureDetector(
          onTap: () {
            setState(() {
              // Change the checked state of the ingredient
              if (ingredients[index].checked == IngredientStatus.CHECKED) {
                ingredients[index].checked = IngredientStatus.NOT_CHECKED;
              } else {
                ingredients[index].checked = IngredientStatus.CHECKED;
              }

              // Go to each meal and check off the ingredient in it if it exists
              for (Meal meal in viewModel.meals) {
                meal.checkMealIngredient(ingredients[index]);
              }

              // Update the store with the new checked ingredient
              viewModel.checkIngredient(viewModel.week, viewModel.meals);

              // Check to see if firstBoxChecked is false
              if (!viewModel.firstBoxChecked) {
                // Display information popup
                HelpDialogUtil.checkboxDialog(context);

                // Set firstBoxChecked to true
                viewModel.setFirstBoxChecked(true);
              }
            });
          },
          child: Container(
            height: 35.0,
            decoration: new BoxDecoration(
                borderRadius: (index == 0 && index == ingredients.length - 1)
                    ? BorderRadius.circular(10.0)
                    : (index == 0)
                        ? BorderRadius.vertical(top: Radius.circular(10.0))
                        : (index == ingredients.length - 1)
                            ? BorderRadius.vertical(bottom: Radius.circular(10.0))
                            : null,
                color: (index % 2 != 0) ? MealMonkeyColours.dark4 : MealMonkeyColours.dark8),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                //Checkbox
                new Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.only(left: 10.0),
                  child: ingredientIcon,
                ),
                //Title
                new Container(
                  alignment: Alignment.centerLeft,
                  width: MediaQuery.of(context).size.width - 140,
                  child: new FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Container(
                      padding: EdgeInsets.only(left: 10.0),
                      child: ingredients[index].quantity % 1 == 0
                          ? new Text(
                              "${ingredients[index].quantity.toInt()}${ingredients[index].unit} " +
                                  ingredients[index].name,
                              style: MealMonkeyTextStyles.mainRegular(
                                MealMonkeyColours.darkGrey,
                                15.0,
                                ingredients[index].checked,
                              ),
                            )
                          : new Text(
                              "${ingredients[index].quantity}${ingredients[index].unit} " +
                                  ingredients[index].name,
                              style: MealMonkeyTextStyles.mainRegular(
                                MealMonkeyColours.darkGrey,
                                15.0,
                                ingredients[index].checked,
                              ),
                            ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

/// Returns an [int] representing total number of ingredients in the list.
int _countIngredients(_ViewModel viewModel) {
  int count = 0;
  if (viewModel.week == null) return count;
  for (Department department in viewModel.week.departments) {
    count += department.ingredients.length;
  }
  return count;
}

typedef CheckIngredient(Week week, List<Meal> meals);
typedef SetFirstBoxChecked(bool firstBoxChecked);

/// Class used by the store connector to connect firebase and
/// the local state of the application.
class _ViewModel {
  final CheckIngredient checkIngredient;
  final SetFirstBoxChecked setFirstBoxChecked;
  Week week;
  List<Meal> meals;
  bool displayHelp;
  bool firstBoxChecked;

  _ViewModel({
    this.checkIngredient,
    this.setFirstBoxChecked,
    this.week,
    this.meals,
    this.displayHelp,
    this.firstBoxChecked,
  });
}
