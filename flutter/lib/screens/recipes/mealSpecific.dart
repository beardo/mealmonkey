//Packages
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:mealmonkey/firebase/models/meal.dart';
import 'package:mealmonkey/firebase/models/plan.dart';
import 'package:mealmonkey/firebase/models/week.dart';
import 'package:mealmonkey/firebase/models/user.dart';
import 'package:mealmonkey/firebase/support_models/department.dart';
import 'package:mealmonkey/firebase/support_models/ingredient.dart';
import 'package:mealmonkey/firebase/support_models/ingredientStatus.dart';
import 'package:mealmonkey/firebase/support_models/planUser.dart';
import 'package:mealmonkey/firebase/support_models/recipe.dart';
import 'package:mealmonkey/screens/util/colours.dart';
import 'package:mealmonkey/screens/util/designElements.dart';
import 'package:mealmonkey/screens/util/flushbar.dart';
import 'package:mealmonkey/screens/util/fonts.dart';
import 'package:mealmonkey/screens/util/helpDialogs.dart';
import 'package:mealmonkey/state/action.dart';
import 'package:mealmonkey/state/appstate.dart';
import 'package:mealmonkey/util.dart';
import 'dart:async';
import 'package:url_launcher/url_launcher.dart';

class MealSpecificScreen extends StatefulWidget {
  final int mealIndex;
  MealSpecificScreen({Key key, @required this.mealIndex}) : super(key: key);

  createState() => new MealSpecificState();
}

/// Builds the meal screen using the build method with [context].
/// Documentation can be found here:
/// https://docs.flutter.io/flutter/widgets/State/build.html
class MealSpecificState extends State<MealSpecificScreen> {
  int userRating = 0;
  bool hasRated = false;
  bool _isSubmitting;
  final currency = new NumberFormat("#,##0.00", "en_US");
  final quantity = new NumberFormat("#,##0", "en_US");

  @override
  void initState() {
    _isSubmitting = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final FlushbarUtil flushbarHelper = FlushbarUtil.getInstance(StoreProvider.of(context));

    return StoreConnector<AppState, _ViewModel>(
      converter: (store) => _ViewModel(
            updateMeals: (meals) => store.dispatch(
                  UpdateMealsAction(meals: meals),
                ),
            checkPlanUser: (meals) => store.dispatch(
                  UpdateMealsAction(meals: meals),
                ),
            checkIngredient: (meals, week) {
              store.dispatch(UpdateMealsAction(meals: meals));
              store.dispatch(UpdateWeekAction(week: week));
            },
            setFirstBoxChecked: (firstBoxChecked) {
              store.dispatch(SetFirstBoxCheckedAction(firstBoxChecked: firstBoxChecked));
            },
            user: store.state.user,
            plan: store.state.plan,
            week: store.state.week,
            meals: store.state.meals,
            displayHelp: store.state.displayHelp,
            firstBoxChecked: store.state.firstBoxChecked,
          ),
      onInit: (store) {
        store.dispatch(new RequestMealsDataEventsAction());
        this.userRating = store.state.meals[widget.mealIndex].recipe
            .getUserRating(store.state.user.reference)
            .rating;
        this.hasRated = (this.userRating != 0);
      },
      onDispose: (store) => store.dispatch(new CancelMealsDataEventsAction()),
      builder: (context, viewModel) => Scaffold(
            body: NestedScrollView(
              headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
                return <Widget>[
                  // The expanding appbar with the recipe photo
                  SliverAppBar(
                    actions: <Widget>[buttons(viewModel, widget.mealIndex, flushbarHelper)],
                    leading: new IconButton(
                      icon: new Icon(Icons.arrow_back_ios),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                    backgroundColor: MealMonkeyColours.primary,
                    elevation: 0.4,
                    expandedHeight: 200.0,
                    floating: false,
                    pinned: true,
                    flexibleSpace: FlexibleSpaceBar(
                      centerTitle: true,
                      background: new Container(
                        child: Stack(
                          alignment: AlignmentDirectional.bottomStart,
                          children: <Widget>[
                            Container(
                              height: 300.0,
                              width: MediaQuery.of(context).size.width,
                              padding: const EdgeInsets.all(0.0),
                              child: CachedNetworkImage(
                                imageUrl: viewModel.meals[widget.mealIndex].recipe.picture,
                                fit: BoxFit.cover,
                              ),
                            ),
                            Container(
                              height: 100.0,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  begin: Alignment.bottomCenter,
                                  end: Alignment.topCenter,
                                  colors: [
                                    Color.fromRGBO(255, 255, 255, 1.0),
                                    Color.fromRGBO(250, 250, 250, 0.0),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ];
              },
              body: new Container(
                color: Color.fromRGBO(255, 255, 255, 1.0),
                child: ListView(
                  physics: ClampingScrollPhysics(),
                  shrinkWrap: true,
                  children: <Widget>[
                    _details(
                      viewModel.meals[widget.mealIndex].recipe,
                      viewModel.meals[widget.mealIndex].users,
                      viewModel.plan,
                    ),
                    _infoCard(viewModel.meals[widget.mealIndex]),
                    _method(viewModel.meals[widget.mealIndex].recipe),
                    _shoppingCard(
                      viewModel.meals[widget.mealIndex].recipe,
                      viewModel,
                      widget.mealIndex,
                    ),
                    _rate(viewModel, widget.mealIndex, flushbarHelper),
                  ],
                ),
              ),
            ),
          ),
    );
  }

  /// The top section of the page
  /// Includes - name, labels, rating, assigned users, auther name
  /// Takes [recipe], [users], and the [plan]
  Widget _details(Recipe recipe, List<DocumentReference> users, Plan plan) {
    int rating = recipe.rating.toInt();

    return new Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.only(left: 20.0, right: 20.0),
      child: Stack(
        alignment: AlignmentDirectional.bottomEnd,
        children: <Widget>[
          new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //Name
              new Text(recipe.name,
                  textAlign: TextAlign.left,
                  style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.heading1, 28.0)),
              new Container(height: 10.0),

              //Labels
              _buildLabels(recipe.labels),

              //Ratings
              Container(
                height: 30.0,
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new Icon(IconData(0xe911, fontFamily: 'IonIcons'),
                        color: (rating >= 1) ? MealMonkeyColours.gold : MealMonkeyColours.grey,
                        size: 20.0),
                    new Icon(IconData(0xe911, fontFamily: 'IonIcons'),
                        color: (rating >= 2) ? MealMonkeyColours.gold : MealMonkeyColours.grey,
                        size: 20.0),
                    new Icon(IconData(0xe911, fontFamily: 'IonIcons'),
                        color: (rating >= 3) ? MealMonkeyColours.gold : MealMonkeyColours.grey,
                        size: 20.0),
                    new Icon(IconData(0xe911, fontFamily: 'IonIcons'),
                        color: (rating >= 4) ? MealMonkeyColours.gold : MealMonkeyColours.grey,
                        size: 20.0),
                    new Icon(IconData(0xe911, fontFamily: 'IonIcons'),
                        color: (rating >= 5) ? MealMonkeyColours.gold : MealMonkeyColours.grey,
                        size: 20.0),
                  ],
                ),
              ),
            ],
          ),

          //Right aligned
          Container(
            padding: EdgeInsets.only(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                //Assigned users
                _buildImages(users, plan),

                //Author name
                Text(
                  recipe.author.name,
                  style: MealMonkeyTextStyles.mainSemiBold(MealMonkeyColours.darkGrey, 16.0),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  /// Shows the images for all of the assigned [users] from the [plan]
  Widget _buildImages(List<DocumentReference> users, Plan plan) {
    if (users.length != 0) {
      return new Container(
        child: new Stack(
          children: new List.generate(
            users.length,
            (int index) {
              PlanUser planUser = plan.getPlanUserFromRef(users[index]);
              return Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(child: Util.showPic(planUser)),
                  new Container(
                    width: 17.0 * index, // image spacing
                  ),
                ],
              );
            },
          ),
        ),
      );
    } else {
      return new Container();
    }
  }

  /// Shows all of the [labels] for the recipe
  Widget _buildLabels(List<String> labels) {
    if (labels.length != 0) {
      return new Container(
        height: 20.0,
        child: new ListView(
          scrollDirection: Axis.horizontal,
          children: new List.generate(
            labels.length,
            (int index) {
              return new Container(
                child: new Text(
                  (index == labels.length - 1) ? labels[index] : labels[index] + ",",
                  style: MealMonkeyTextStyles.mainItalic(MealMonkeyColours.grey, 14.0),
                ),
              );
            },
          ),
        ),
      );
    } else {
      return new Container();
    }
  }

  ///Creates a button bring up the meal settings dialog. Uses [viewModel] and meal [index]
  Widget buttons(
    _ViewModel viewModel,
    int index,
    FlushbarUtil flushbarHelper,
  ) {
    return new Container(
        padding: EdgeInsets.only(top: 5.0, right: Util.pageWidthPadding + 5),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            new Container(
              height: 30.0,
              width: 30.0,
              decoration: new BoxDecoration(
                color: MealMonkeyColours.white,
                borderRadius: new BorderRadius.all(const Radius.circular(10.0)),
              ),
              child: new IconButton(
                icon: new Icon(
                  const IconData(0xe8c4, fontFamily: 'IonIcons'),
                  size: 25.0,
                  color: MealMonkeyColours.primary,
                ),
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (_) => _editDialog(context, viewModel, index, flushbarHelper),
                  );
                },
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
              ),
            )
          ],
        ));
  }

  /// Creates the edit meal settings dialog
  /// Allows for assigning users and swapping meals
  /// Uses [context] [viewModel] and recipe [index]
  Widget _editDialog(
    BuildContext context,
    _ViewModel viewModel,
    int index,
    FlushbarUtil flushbarHelper,
  ) {
    return new SimpleDialog(children: <Widget>[
      new Column(
        children: <Widget>[
          new Stack(
            children: <Widget>[
              new Container(
                child: Text(
                  'Edit',
                  style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.black, 22.0),
                ),
                padding: EdgeInsets.only(top: 10.0),
                alignment: Alignment.center,
              ),

              //The help dialog button
              viewModel.displayHelp
                  ? Container(
                      padding: EdgeInsets.only(bottom: 10.0, right: 10.0),
                      child: new IconButton(
                        icon: new Icon(
                          Icons.help,
                          color: MealMonkeyColours.primary,
                          size: 30.0,
                        ),
                        onPressed: () {
                          HelpDialogUtil.assigneeDialog(context);
                        },
                      ),
                      alignment: Alignment.topRight,
                      decoration: new BoxDecoration(
                        border:
                            new Border(bottom: new BorderSide(color: MealMonkeyColours.divider)),
                      ),
                    )
                  : new Container(),
            ],
          ),
          new Container(
            padding: EdgeInsets.fromLTRB(20.0, 15.0, 0.0, 5.0),
            alignment: Alignment.centerLeft,
            child: Text(
              "Assignees",
              style: MealMonkeyTextStyles.mainRegular(MealMonkeyColours.darkGrey, 16.0),
            ),
          ),

          // Builds all of the users in the plan
          new Container(
            padding: EdgeInsets.fromLTRB(10.0, 5.0, 5.0, 10.0),
            decoration: new BoxDecoration(
              border: new Border(bottom: new BorderSide(color: MealMonkeyColours.divider)),
            ),
            child: StatefulList(
              viewModel: viewModel,
              mealIndex: widget.mealIndex,
            ),
          ),

          //Swap meal button
          new Container(
            width: MediaQuery.of(context).size.width * 0.70,
            height: 60.0,
            padding: EdgeInsets.only(top: 15.0),
            child: new RaisedButton(
              color: MealMonkeyColours.primary,
              child: Text(
                "SWAP MEAL",
                style: MealMonkeyTextStyles.mainBold(Colors.white, 17.0),
              ),
              onPressed: () {
                Navigator.pop(context);
                _getNewMeal(context, viewModel, index, flushbarHelper);
              },
            ),
          )
        ],
      )
    ]);
  }

  /// Creates a new meal when the swap button is pressed
  Future _getNewMeal(
    BuildContext context,
    _ViewModel viewModel,
    int index,
    FlushbarUtil flushbarHelper,
  ) async {
    flushbarHelper.dismissAll();

    flushbarHelper
        .create(
          type: FlushbarType.INFO,
          message: "Swapping out your recipe!",
          flushbarDuration: FlushbarDuration.UNLIMITED,
        )
        .show(context);

    // Generate meals on Cloud Firestore
    try {
      await CloudFunctions.instance.call(
        functionName: 'swapMeal',
        parameters: <String, dynamic>{
          'rejected_recipe': viewModel.meals[index].recipe.recipeReference.path,
          'rejected_meal': viewModel.meals[index].reference.path,
          'labels': viewModel.plan.labels,
          'budget': viewModel.plan.budget,
          'week': viewModel.week.reference.path,
        },
      );

      flushbarHelper.dismissAll();

      flushbarHelper
          .create(
            type: FlushbarType.SUCCESS,
            message: "We've swapped out your meal!",
            flushbarDuration: FlushbarDuration.SHORT,
          )
          .show(context);
    } on CloudFunctionsException catch (e) {
      flushbarHelper.dismissAll();

      flushbarHelper
          .create(
            type: FlushbarType.ERROR,
            message: e.message,
            flushbarDuration: FlushbarDuration.MEDIUM,
          )
          .show(context);
    } catch (e) {
      print(e);
      flushbarHelper.dismissAll();

      flushbarHelper
          .create(
            type: FlushbarType.ERROR,
            message: "Something went wrong while swapping your recipe.",
            flushbarDuration: FlushbarDuration.MEDIUM,
          )
          .show(context);
    }
  }

  /// Creates both the method and the author buttons for the [recipe]
  Widget _method(Recipe recipe) {
    return new Container(
      padding: EdgeInsets.symmetric(horizontal: Util.pageWidthPadding),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Container(
            //Method
            width: MediaQuery.of(context).size.width / 2 - (Util.pageWidthPadding + 10),
            child: new RaisedButton(
              shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0)),
              onPressed: () => setState(() {
                    _launchURL(recipe.link);
                  }),
              color: MealMonkeyColours.grey,
              child: new Text(
                'Method',
                style: MealMonkeyTextStyles.mainSemiBold(Colors.white, 20.0),
              ),
            ),
          ),

          //Author button
          Container(
            width: MediaQuery.of(context).size.width / 2 - (Util.pageWidthPadding + 10),
            child: new RaisedButton(
              shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0)),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (_) => _showAuthor(recipe),
                );
              },
              color: MealMonkeyColours.grey,
              child: new Text(
                'Author',
                style: MealMonkeyTextStyles.mainSemiBold(Colors.white, 20.0),
              ),
            ),
          ),
        ],
      ),
    );
  }

  /// Creates a small dialog with information pertaining to the [recipe] author
  Widget _showAuthor(Recipe recipe) {
    return new SimpleDialog(
      contentPadding: EdgeInsets.all(15.0),
      // titlePadding: EdgeInsets.all(Util.pageWidthPadding),
      children: <Widget>[
        Text(
          recipe.author.name,
          style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.darkGrey, 23.0),
          textAlign: TextAlign.center,
        ),
        Container(
          height: 15.0,
        ),
        new Center(
            child: new InkWell(
          child: new Text(
            'Website',
            style: MealMonkeyTextStyles.mainSemiBold(
              Colors.blue[600],
              15.0,
            ),
          ),
          onTap: () => setState(() {
                _launchURL(recipe.author.website);
              }),
        )),
      ],
    );
  }

  /// Launches the devices default browser to a defined web pages
  /// based on input [url]
  Future<Null> _launchURL(String url) async {
    if (await canLaunch(url)) {
      await (launch(url));
    } else {
      throw 'Could not launch $url';
    }
  }

  /// Creates the card with the [meal] price, prep time, and servings
  Widget _infoCard(Meal meal) {
    double iconSize = 30.0;
    double textSize = 12.0;
    double iconColWidth =
        ((MediaQuery.of(context).size.width - (Util.pageWidthPadding * 2)) / 3.0) - 10.0;
    double iconColHeight = 90.0;

    return new Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(Util.pageWidthPadding),
      child: new Container(
        decoration: DesignUtil.boxDecoration,
        child: new Card(
          elevation: 0.0,
          child: new Container(
            padding: const EdgeInsets.all(10.0),
            child: new Column(
              children: <Widget>[
                new Row(
                  children: <Widget>[
                    //Prep time
                    new Container(
                      width: iconColWidth,
                      height: iconColHeight,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: new Icon(
                              const IconData(0xe802, fontFamily: "MealSpecificIcons"),
                              color: MealMonkeyColours.darkGrey,
                              size: iconSize,
                            ),
                          ),
                          new Text(
                            "${meal.recipe.time.toString()} mins",
                            textAlign: TextAlign.center,
                            style: MealMonkeyTextStyles.mainSemiBold(
                              MealMonkeyColours.darkGrey,
                              textSize + 3.0,
                            ),
                          )
                        ],
                      ),
                    ),

                    //Servings
                    new Container(
                      width: iconColWidth,
                      height: iconColHeight,
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.fromLTRB(0.0, 3.0, 11.0, 0.0),
                            child: Padding(
                              padding: const EdgeInsets.only(bottom: 8.0),
                              child: new Icon(
                                const IconData(0xe801, fontFamily: "MealSpecificIcons"),
                                color: MealMonkeyColours.darkGrey,
                                size: iconSize - 3.0,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 2.0),
                            child: new Text(
                              '${meal.recipe.servings} serves',
                              style: MealMonkeyTextStyles.mainSemiBold(
                                  MealMonkeyColours.darkGrey, textSize + 3.0),
                            ),
                          ),
                        ],
                      ),
                    ),

                    // Price
                    new Container(
                      width: iconColWidth,
                      height: iconColHeight,
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: new Icon(
                              const IconData(0xe800, fontFamily: "MealSpecificIcons"),
                              color: MealMonkeyColours.darkGrey,
                              size: iconSize,
                            ),
                          ),
                          new Text(
                            "\$${meal.price.toInt()}",
                            style: MealMonkeyTextStyles.mainSemiBold(
                              MealMonkeyColours.darkGrey,
                              textSize + 3.0,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  /// Builds the card contianing all of the ingredients for the [recipe] from the [viewModel]
  Widget _shoppingCard(Recipe recipe, _ViewModel viewModel, int mealIndex) {
    return new Container(
      padding: EdgeInsets.all(Util.pageWidthPadding),
      child: new Container(
        decoration: DesignUtil.boxDecoration,
        child: new Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          elevation: 0.0,
          child:
              new Container(width: 324.0, child: _buildDepartments(recipe, viewModel, mealIndex)),
        ),
      ),
    );
  }

  ///Builds each department using the ingredients from the [recipe] and [viewModel].
  Widget _buildDepartments(Recipe recipe, _ViewModel viewModel, int mealIndex) {
    // Order departments
    recipe.departments.sort(
      (Department dep1, Department dep2) => Department.sortDepartments(dep1.name, dep2.name),
    );

    return new ListView(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      children: <Widget>[
        new Container(
          child: new Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            elevation: 0.0,
            child: new Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(bottom: 15.0, left: 10.0, right: 10.0),
                  child: new Column(
                      children: recipe.departments.map((Department department) {
                    return new Column(
                      children: <Widget>[
                        new Container(
                          height: 50.0,
                          child: new ListTile(
                            title: new Text(
                              department.name.toUpperCase(),
                              textAlign: TextAlign.left,
                              style: MealMonkeyTextStyles.mainSemiBold(
                                  MealMonkeyColours.darkGrey, 17.0),
                            ),
                          ),
                        ),
                        new Container(
                          padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 12.0),
                          decoration: new BoxDecoration(),
                          child: ListView(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            children: <Widget>[
                              _buildIngredient(
                                department.ingredients,
                                department,
                                viewModel,
                                mealIndex,
                              )
                            ],
                          ),
                        )
                      ],
                    );
                  }).toList()),
                ),
                viewModel.displayHelp
                    ? Positioned(
                        top: 5.0,
                        right: 10.0,
                        child: new IconButton(
                          icon: new Icon(Icons.help),
                          color: MealMonkeyColours.primary,
                          onPressed: () {
                            HelpDialogUtil.stapleDialog(context);
                          },
                        ),
                      )
                    : new Container(),
              ],
            ),
          ),
        ),
      ],
    );
  }

  /// Builds each ingredient using the input [ingredient], [department], and the [viewModel].
  Widget _buildIngredient(
    List<Ingredient> ingredients,
    Department department,
    _ViewModel viewModel,
    int mealIndex,
  ) {
    return new ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: ingredients.length,
      itemBuilder: (BuildContext context, index) {
        return GestureDetector(
          onTap: () {
            // Change the checked state of the ingredient
            if (ingredients[index].checked == IngredientStatus.CHECKED) {
              ingredients[index].checked = IngredientStatus.NOT_CHECKED;
            } else {
              ingredients[index].checked = IngredientStatus.CHECKED;
            }

            // Go to each meal and check off the ingredient in it if it exists
            int count = 0;
            for (Meal meal in viewModel.meals) {
              if (meal.reference != viewModel.meals[mealIndex].reference)
                count += meal.checkMainIngredient(ingredients[index]);
            }
            viewModel.week.checkIngredient(ingredients[index], count);

            // Update the store with the new checked ingredient
            viewModel.checkIngredient(viewModel.meals, viewModel.week);

            // Check to see if firstBoxChecked is false
            if (!viewModel.firstBoxChecked) {
              // Display information popup
              HelpDialogUtil.checkboxDialog(context);

              // Set firstBoxChecked to true
              viewModel.setFirstBoxChecked(true);
            }
          },
          child: Container(
            height: 35.0,

            //The rounded borders
            decoration: new BoxDecoration(
                borderRadius: (index == 0 && index == ingredients.length - 1)
                    ? BorderRadius.circular(10.0)
                    : (index == 0)
                        ? BorderRadius.vertical(top: Radius.circular(10.0))
                        : (index == ingredients.length - 1)
                            ? BorderRadius.vertical(bottom: Radius.circular(10.0))
                            : null,
                // The alternating colours
                color: (index % 2 != 0) ? MealMonkeyColours.dark4 : MealMonkeyColours.dark8),
            child: Stack(
              children: <Widget>[
                new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    //Checkbox
                    new Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.only(left: 10.0),
                      child: new Icon(
                          (ingredients[index].checked == IngredientStatus.CHECKED)
                              ? Icons.check_box
                              : Icons.check_box_outline_blank,
                          color: MealMonkeyColours.primary),
                    ),
                    //Title
                    new Container(
                      alignment: Alignment.centerLeft,
                      width: MediaQuery.of(context).size.width - 140,
                      child: new FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Container(
                          padding: EdgeInsets.only(left: 10.0),
                          child: ingredients[index].quantity % 1 == 0
                              ? new Text(
                                  "${ingredients[index].quantity.toInt()}${ingredients[index].unit} " +
                                      ingredients[index].name,
                                  style: MealMonkeyTextStyles.mainRegular(
                                    MealMonkeyColours.darkGrey,
                                    15.0,
                                    ingredients[index].checked,
                                  ),
                                )
                              : new Text(
                                  "${ingredients[index].quantity}${ingredients[index].unit} " +
                                      ingredients[index].name,
                                  style: MealMonkeyTextStyles.mainRegular(
                                    MealMonkeyColours.darkGrey,
                                    15.0,
                                    ingredients[index].checked,
                                  ),
                                ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  /// Creates the rate meal card using [viewModel] and recipe [index]
  Widget _rate(
    _ViewModel viewModel,
    int index,
    FlushbarUtil flushbarHelper,
  ) {
    return new Container(
      padding: EdgeInsets.only(
          left: Util.pageWidthPadding, right: Util.pageWidthPadding, bottom: Util.pageWidthPadding),
      child: Container(
        decoration: DesignUtil.boxDecoration,
        child: new Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          elevation: 0.0,
          color: Colors.white,
          child: new Column(
            children: <Widget>[
              Container(height: 20.0),
              hasRated
                  ? new Container(
                      margin: EdgeInsets.only(bottom: 10.0),
                      child: new Text(
                        'Thanks for rating this meal!',
                        style: MealMonkeyTextStyles.mainSemiBold(MealMonkeyColours.primary, 15.0),
                      ),
                    )
                  : new Container(),

              // Creates the clickable stars
              new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  _createStar(viewModel, 1, index),
                  _createStar(viewModel, 2, index),
                  _createStar(viewModel, 3, index),
                  _createStar(viewModel, 4, index),
                  _createStar(viewModel, 5, index),
                ],
              ),
              hasRated
                  ? new Container(
                      margin: EdgeInsets.only(top: 10.0),
                      child: new RaisedButton(
                        color: MealMonkeyColours.primary,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                        child: new Text(
                          'EDIT RATING',
                          style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.white, 15.0),
                        ),
                        onPressed: () {
                          hasRated = false;
                          viewModel.meals[index].recipe
                              .getUserRating(viewModel.user.reference)
                              .rating = 0;
                          setState(() {});
                        },
                      ),
                    )
                  : new Container(
                      margin: EdgeInsets.only(top: 10.0),
                      child: new RaisedButton(
                        color: _isSubmitting
                            ? MealMonkeyColours.greyButton
                            : MealMonkeyColours.primary,
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(5.0),
                        ),
                        child: new Text(
                          'RATE',
                          style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.white, 15.0),
                        ),
                        onPressed: () async {
                          if (!_isSubmitting) {
                            _isSubmitting = true;
                            await _submitRating(viewModel, index, flushbarHelper);
                            _isSubmitting = false;
                          }
                        },
                      ),
                    ),
              new Container(height: 10.0)
            ],
          ),
        ),
      ),
    );
  }

  /// Submits the rating when button is pressed. Uses [viewModel] and recipe [index]
  Future<void> _submitRating(
    _ViewModel viewModel,
    int index,
    FlushbarUtil flushbarHelper,
  ) async {
    if (userRating > 0) {
      setState(() {});

      //Call the rate meal cloud function
      try {
        await CloudFunctions.instance.call(
          functionName: 'rateMeal',
          parameters: <String, dynamic>{
            'rating': userRating,
            'recipe': viewModel.meals[index].recipe.recipeReference.path,
            'user': viewModel.user.reference.path,
          },
        );

        hasRated = true;
      } on CloudFunctionsException catch (e) {
        flushbarHelper.dismissAll();

        flushbarHelper
            .create(
              type: FlushbarType.ERROR,
              message: e.message,
              flushbarDuration: FlushbarDuration.MEDIUM,
            )
            .show(context);
      } catch (e) {
        // print('caught generic exception');
        print(e);

        flushbarHelper.dismissAll();

        flushbarHelper
            .create(
              type: FlushbarType.ERROR,
              message: "Something went wrong while swapping your recipe.",
              flushbarDuration: FlushbarDuration.MEDIUM,
            )
            .show(context);
      }
    }
  }

  ///Mehtod for building a clickable star using [viewModel], [starIndex], and [recipeIndex]
  Widget _createStar(_ViewModel viewModel, int starIndex, int recipeIndex) {
    // bool hasRated = (userRating != 0);

    bool checked = false;
    checked = (starIndex <= userRating);
    return new GestureDetector(
      onTap: () {
        if (!hasRated) {
          // Set the checked state
          setState(() {
            userRating = starIndex;
          });
        }
      },
      child: new Container(
        padding: EdgeInsets.symmetric(horizontal: 4.0),
        child: new Icon(IconData(0xe911, fontFamily: 'IonIcons'),
            color: checked ? MealMonkeyColours.gold : MealMonkeyColours.grey, size: 40.0),
      ),
    );
  }
}

///Class created to help with assigning the users in the meal settings dialog
///Uses [viewModel] and [mealIndex]
class StatefulList extends StatefulWidget {
  StatefulList({
    Key key,
    this.viewModel,
    this.mealIndex,
  }) : super(key: key);

  final _ViewModel viewModel;
  final int mealIndex;

  @override
  State<StatefulWidget> createState() => new StatefulListState();
}

class StatefulListState extends State<StatefulList> {
  // boo= false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _list();
  }

  /// Returns a list of all the users
  _list() {
    return new Column(
      children: widget.viewModel.plan.users.map((PlanUser user) {
        return _addUser(user);
      }).toList(),
    );
  }

  /// Creates a clickable row containg a [user]
  Widget _addUser(PlanUser user) {
    bool assigned = widget.viewModel.meals[widget.mealIndex].users.contains(user.userReference);
    return new Container(
      padding: EdgeInsets.symmetric(vertical: 8.0),
      child: new InkWell(
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                //pic
                Container(padding: EdgeInsets.only(left: 10.0), child: Util.showPic(user)),
                //Name
                new Container(
                  alignment: Alignment.centerLeft,
                  width: MediaQuery.of(context).size.width - 180,
                  child: new FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Container(
                      padding: EdgeInsets.only(left: 10.0),
                      child: new Text(
                        user.username,
                        style: MealMonkeyTextStyles.mainRegular(
                          assigned ? MealMonkeyColours.primary : MealMonkeyColours.darkGrey,
                          15.0,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            //Tick
            new Container(
              alignment: Alignment.centerLeft,
              width: 40.0,
              child: assigned
                  ? Icon(
                      Icons.check,
                      color: MealMonkeyColours.primary,
                    )
                  : Container(),
            )
          ],
        ),
        onTap: () {
          assigned
              ? widget.viewModel.meals[widget.mealIndex].users.remove(user.userReference)
              : widget.viewModel.meals[widget.mealIndex].users.add(user.userReference);

          widget.viewModel.checkPlanUser(widget.viewModel.meals);
          setState(() {});
        },
      ),
    );
  }
}

typedef UpdateMeals(List<Meal> meals);
typedef CheckPlanUser(List<Meal> meals);
typedef CheckIngredient(List<Meal> meals, Week week);
typedef SetFirstBoxChecked(bool firstBoxChecked);

class _ViewModel {
  final UpdateMeals updateMeals;
  final CheckPlanUser checkPlanUser;
  final CheckIngredient checkIngredient;
  final SetFirstBoxChecked setFirstBoxChecked;
  Plan plan;
  Week week;
  List<Meal> meals;
  User user;
  bool displayHelp;
  bool firstBoxChecked;

  _ViewModel({
    this.updateMeals,
    this.checkPlanUser,
    this.checkIngredient,
    this.setFirstBoxChecked,
    this.plan,
    this.week,
    this.meals,
    this.user,
    this.displayHelp,
    this.firstBoxChecked,
  });
}
