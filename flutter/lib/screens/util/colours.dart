import 'package:flutter/material.dart';

///class that defines MealMonkeys colours for use within the app
class MealMonkeyColours {
  static final Color primary = new Color.fromRGBO(29, 171, 95, 1.0);
  static final Color secondary = new Color.fromRGBO(61, 196, 126, 1.0);
  static final Color heading1 = new Color.fromRGBO(58, 71, 85, 1.0);
  static final Color subtext = new Color.fromRGBO(147, 159, 176, 1.0);
  static final Color divider = new Color.fromRGBO(147, 159, 176, 0.2);

  static final Color darkGrey = new Color.fromRGBO(58, 71, 85, 1.0);
  static final Color grey = new Color.fromRGBO(147, 159, 176, 1.0);
  static final Color lightGrey = new Color.fromRGBO(237, 240, 245, 1.0);
  static final Color strikeGrey = new Color.fromRGBO(148, 155, 162, 1.0);
  static final Color white = new Color.fromRGBO(255, 255, 255, 1.0);
  static final Color dark8 = new Color.fromRGBO(238, 238, 239, 1.0);
  static final Color dark4 = new Color.fromRGBO(246, 247, 247, 1.0);

  static final Color black = new Color.fromRGBO(50, 50, 50, 1.0);
  static final Color fadedblack = new Color.fromRGBO(150, 150, 150, 1.0);

  static final Color heading2 = new Color.fromRGBO(99, 113, 114, 1.0);
  static final Color greyButton = new Color.fromRGBO(189, 189, 189, 1.0);
  static final Color textField = new Color.fromRGBO(241, 241, 241, 1.0);
  static final Color fadedTeal = new Color.fromRGBO(117, 185, 190, 0.5);
  static final Color gold = new Color.fromRGBO(255, 186, 30, 1.0);

  static final Color successGreen = new Color.fromRGBO(35, 209, 96, 1.0);
  static final Color successGreenText = Colors.white;
  static final Color infoBlue = new Color.fromRGBO(50, 115, 220, 1.0);
  static final Color infoBlueText = Colors.white;
  static final Color warningOrange = new Color.fromRGBO(255, 221, 87, 1.0);
  static final Color warningOrangeText = new Color.fromRGBO(0, 0, 0, .7);
  static final Color errorRed = new Color.fromRGBO(255, 56, 96, 1.0);
  static final Color errorRedText = Colors.white;
}
