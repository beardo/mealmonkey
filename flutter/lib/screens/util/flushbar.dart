/*
  Idea of this class is to manage all of the Flushbar logic. Flushbar's will be stored in the
  Redux store and this class will be the only code that interacts with it.
*/

import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:mealmonkey/screens/util/colours.dart';
import 'package:mealmonkey/screens/util/fonts.dart';
import 'package:mealmonkey/state/action.dart';
import 'package:mealmonkey/state/appstate.dart';
import 'package:redux/redux.dart';

enum FlushbarType {
  WARNING,
  SUCCESS,
  INFO,
  ERROR,
  ALL,
}

enum FlushbarDuration {
  SHORT,
  MEDIUM,
  LONG,
  UNLIMITED,
}

class FlushbarDurationUtil {
  static Duration getDuration(FlushbarDuration duration) {
    if (duration == FlushbarDuration.UNLIMITED) {
      return null;
    } else if (duration == FlushbarDuration.SHORT) {
      return new Duration(seconds: 3);
    } else if (duration == FlushbarDuration.MEDIUM) {
      return new Duration(seconds: 5);
    } else if (duration == FlushbarDuration.LONG) {
      return new Duration(seconds: 7);
    }
    return null;
  }
}

class FlushbarUtil {
  Map<FlushbarType, List<Flushbar>> _flushbars = new Map<FlushbarType, List<Flushbar>>();

  static FlushbarUtil getInstance(Store<AppState> store) {
    if (store.state.flushbarHelper == null) {
      FlushbarUtil flushbarHelper = new FlushbarUtil();
      store.dispatch(SetFlushbarHelperAction(flushbarHelper: flushbarHelper));
    }
    return store.state.flushbarHelper;
  }

  // Close all
  Future<void> dismissAll({
    @optionalTypeArgs FlushbarType type = FlushbarType.ALL,
  }) async {
    var toRemove = [];
     
    for (FlushbarType flushbarType in FlushbarType.values) {
      if (type == flushbarType || type == FlushbarType.ALL) {
        if (_flushbars.containsKey(flushbarType) && _flushbars[flushbarType].length > 0) {
          for (Flushbar flushbar in _flushbars[flushbarType]) {
            if (flushbar.isDismissible) {
              // await flushbar.dismiss();
              toRemove.add(flushbar);
            }
          }

          for(Flushbar flushbar in toRemove) {
            flushbar.dismiss();
          }
        }
      }
    }
  }

  // Create
  Flushbar create({
    @required FlushbarType type,
    @required String message,
    @optionalTypeArgs bool loading = false,
    @optionalTypeArgs FlushbarDuration flushbarDuration = FlushbarDuration.UNLIMITED,
  }) {
    Flushbar newFlushbar;

    switch (type) {
      case FlushbarType.SUCCESS:
        newFlushbar = new Flushbar(
          messageText: new Text(
            message,
            style: MealMonkeyTextStyles.mainRegular(MealMonkeyColours.successGreenText, 20.0),
          ),
          backgroundColor: MealMonkeyColours.successGreen,
          
          icon: new Icon(
            Icons.check_circle_outline,
            size: 28.0,
            color: MealMonkeyColours.successGreenText,
          ),
          duration: FlushbarDurationUtil.getDuration(flushbarDuration),
          leftBarIndicatorColor: MealMonkeyColours.successGreen,
        );

        break;

      case FlushbarType.INFO:
        newFlushbar = new Flushbar(
          messageText: new Text(
            message,
            style: MealMonkeyTextStyles.mainRegular(MealMonkeyColours.infoBlueText, 20.0),
          ),
          backgroundColor: MealMonkeyColours.infoBlue,
          icon: new Icon(
            Icons.info_outline,
            size: 28.0,
            color: MealMonkeyColours.infoBlueText,
          ),
          duration: FlushbarDurationUtil.getDuration(flushbarDuration),
          leftBarIndicatorColor: MealMonkeyColours.infoBlue,
        );

        break;

      case FlushbarType.WARNING:
        newFlushbar = new Flushbar(
          messageText: new Text(
            message,
            style: MealMonkeyTextStyles.mainRegular(MealMonkeyColours.warningOrangeText, 20.0),
          ),
          backgroundColor: MealMonkeyColours.warningOrange,
          icon: new Icon(
            Icons.warning,
            size: 28.0,
            color: MealMonkeyColours.warningOrangeText,
          ),
          duration: FlushbarDurationUtil.getDuration(flushbarDuration),
          leftBarIndicatorColor: MealMonkeyColours.warningOrange,
        );

        break;

      case FlushbarType.ERROR:
        newFlushbar = new Flushbar(
          messageText: new Text(
            message,
            style: MealMonkeyTextStyles.mainRegular(MealMonkeyColours.errorRedText, 20.0),
          ),
          backgroundColor: MealMonkeyColours.errorRed,
          icon: new Icon(
            Icons.error_outline,
            size: 28.0,
            color: MealMonkeyColours.errorRedText,
          ),
          duration: FlushbarDurationUtil.getDuration(flushbarDuration),
          leftBarIndicatorColor: MealMonkeyColours.errorRed,
        );

        break;

      default:
        newFlushbar = null;
    }

    assert(newFlushbar != null);

    if (_flushbars.containsKey(type)) {
      _flushbars[type].add(newFlushbar);
    } else {
      List<Flushbar> newFlushbarList = new List<Flushbar>();
      newFlushbarList.add(newFlushbar);
      _flushbars.putIfAbsent(type, () => newFlushbarList);
    }

    return newFlushbar;
  }
}
