import 'package:flutter/material.dart';
import 'package:mealmonkey/screens/util/colours.dart';
import 'package:mealmonkey/screens/util/fonts.dart';

class HelpDialogUtil {
  /// Returns a [showDialog] containing a help dialog informing the user about the meaning
  /// of staple ingredients.
  static stapleDialog(BuildContext context) {
    return showDialog<Null>(
      context: context,
      builder: (BuildContext context) {
        return new SimpleDialog(
          title: new Text('Staple Ingredients'),
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 25.0),
              child: new Column(
                children: <Widget>[
                  new Text(
                      "Staple ingredients are ingredients that we consider as everyday ingredients found in most kitchens."),
                  new Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    child: new Text(
                        "Because of this, we do not consider them when calculating the cost of a meal."),
                  ),
                  new Text(
                      "They will mostly consist of everyday herbs and spices, along with kitchen essentials like Milk and Cheese."),
                  new FlatButton(
                    child: new Text(
                      "Close",
                      style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.primary, 17.0),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  /// Returns a [showDialog] displaying information about assigning users to meals.
  static assigneeDialog(BuildContext context) {
    return showDialog<Null>(
      context: context,
      builder: (BuildContext context) {
        return new SimpleDialog(
          title: new Text('Assigning Meals'),
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 25.0),
              child: new Column(
                children: <Widget>[
                  new Text(
                      "When you assign someone to a meal in the \"Edit\" dialog, they will only be assigned for this week."),
                  new Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    child: new Text(
                        "If you would like to be permamently assign someone to a day of the week, you can edit that in the Plan settings by selecting the user and choosing \"Edit assigned days\"."),
                  ),
                  new FlatButton(
                    child: new Text(
                      "Close",
                      style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.primary, 17.0),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  /// Returns [showDialog] that displays a help dialog containing information about
  /// the plan screen.
  static planSettingsDialog(BuildContext context) {
    return showDialog<Null>(
      context: context,
      builder: (BuildContext context) {
        return new SimpleDialog(
          title: new Text('Plan Settings'),
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 25.0),
              child: new Column(
                children: <Widget>[
                  new Text(
                      "You can assign group members to days of the week by selecting their name. This will automatically assign them to cook the meal on that day at the start of each week."),
                  new Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    child: new Text(
                        "If you would like to assign someone to a specfic meal for the current week only, you can do this by selecting the edit meal button at the top right of the corresponding meal card."),
                  ),
                  new Text(
                      "You can leave this plan to start a new one at any time by selecting your own name from the list below and then choosing \"Create your own plan\"."),
                  new FlatButton(
                    child: new Text(
                      "Close",
                      style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.primary, 17.0),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  /// Returns a [showDialog] containing a help dialog informing the user about the meaning
  /// of staple ingredients.
  static checkboxDialog(BuildContext context) {
    return showDialog<Null>(
      context: context,
      builder: (BuildContext context) {
        return new SimpleDialog(
          title: new Text('Ticking Ingredients'),
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 25.0),
              child: new Column(
                children: <Widget>[
                  new Text(
                    "Ticking an ingredient on the combined ingredients page also ticks them on the " + 
                    "meal specific pages.",
                  ),
                  new Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    child: new Text(
                        "Ticking an ingredient on a meal specific page will be refelcted on the " + 
                        "combined ingredients page. If all occurences of the ingredient are ticked " + 
                        "the combined page will show a normal tick. Otherwise, it will display " + 
                        "a dashed tick to represent only part of the ingreident being ticked."),
                  ),
                  new FlatButton(
                    child: new Text(
                      "Close",
                      style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.primary, 17.0),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }
}
