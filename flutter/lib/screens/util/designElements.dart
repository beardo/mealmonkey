import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

// Util class used to call stape design methods thoughout the application.

class DesignUtil {
  /// Returns a [Container] containing a text field.
  static Widget textFieldGenerator({
    @required TextFormField tff,
    @required BuildContext context,
    @optionalTypeArgs bool width,
  }) {
    if (width == null) width = true;

    return new Container(
      padding: width ? EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0) : null,
      child: new Container(
        padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
        width: width ? MediaQuery.of(context).size.width * 0.80 : null,
        decoration: BoxDecoration(
          boxShadow: [
            DesignUtil.boxShadow,
          ],
          color: Color.fromRGBO(255, 255, 255, 1.0),
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: tff,
      ),
    );
  }

  /// Generates a box shadow
  static final BoxShadow boxShadow = new BoxShadow(
    // color: Colors.red,
    // color: new Color.fromRGBO(40, 47, 54, 0.15),
    color: new Color.fromRGBO(0, 0, 0, 0.1),
    blurRadius: 10.0,
    spreadRadius: 5.0,
    // offset: Offset(0.0, 10.0)
  );

  static final BoxDecoration boxDecoration = new BoxDecoration(
    borderRadius: BorderRadius.circular(5.0),
    boxShadow: [
      new BoxShadow(
        // color: Colors.red,
        color: new Color.fromRGBO(0, 0, 0, 0.1),
        blurRadius: 10.0,
        spreadRadius: 2.0,
      )
    ],
  );

  // Code snippet from https://github.com/flutter/flutter/issues/12099
  static bool isIPhoneX(MediaQueryData mediaQuery) {
    if (Platform.isIOS) {
      var size = mediaQuery.size;
      if (size.height == 812.0 || size.width == 812.0) {
        return true;
      }
    }
    return false;
  }
}
