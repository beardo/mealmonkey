import 'package:flutter/material.dart';
import 'package:mealmonkey/firebase/support_models/ingredientStatus.dart';
import 'colours.dart';
///class that defines MealMonkeys colours for use within the app
class MealMonkeyTextStyles {

  static final TextStyle textFieldStyle = new TextStyle(
      fontFamily: 'SFPro',
      fontSize: 15.0,
      color: MealMonkeyColours.heading1); 


  static TextStyle mainBold(Color color, double fontSize){
    return new TextStyle(color: color, fontSize: fontSize, fontFamily: 'SFPro', fontWeight: FontWeight.w800);
  }

/// The main font to use throughout the app, note that [strike] is set to false by default but can be used if necessary
  static TextStyle mainRegular(Color color, double fontSize, [IngredientStatus checkedStatus = IngredientStatus.NOT_CHECKED]){
    bool strike = false;
    if (checkedStatus == IngredientStatus.CHECKED) {
      strike = true;
    } else if (checkedStatus == IngredientStatus.NOT_CHECKED || checkedStatus == IngredientStatus.DASHED) {
      strike = false;
    }

    return new TextStyle(
      color: strike ? MealMonkeyColours.strikeGrey: color, 
      fontSize: fontSize, 
      fontFamily: 'SFPro', 
      fontWeight: FontWeight.w100,
      decoration: strike ? TextDecoration.lineThrough : TextDecoration.none,
      );
  }

   static TextStyle mainSemiBold(Color color, double fontSize){
    return new TextStyle(color: color, fontSize: fontSize, fontFamily: 'SFPro', fontWeight: FontWeight.w600);
   }
    static TextStyle mainItalic(Color color, double fontSize){
      return new TextStyle(color: color, fontSize: fontSize, fontFamily: 'SFPro', fontStyle: FontStyle.italic, fontWeight: FontWeight.w600);
    }

  

  

}