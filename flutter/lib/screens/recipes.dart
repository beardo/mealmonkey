//Import packages
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:intl/intl.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:mealmonkey/firebase/models/meal.dart';
import 'package:mealmonkey/firebase/models/plan.dart';
import 'package:mealmonkey/firebase/models/week.dart';
import 'package:mealmonkey/firebase/support_models/planUser.dart';
import 'package:mealmonkey/screens/recipes/mealSpecific.dart';
import 'package:mealmonkey/screens/util/colours.dart';
import 'package:mealmonkey/screens/util/designElements.dart';
import 'package:mealmonkey/screens/util/flushbar.dart';
import 'package:mealmonkey/screens/util/fonts.dart';
import 'package:mealmonkey/screens/util/helpDialogs.dart';
import 'package:mealmonkey/state/action.dart';
import 'package:mealmonkey/state/appstate.dart';
import 'package:mealmonkey/util.dart';
import 'package:cached_network_image/cached_network_image.dart';

class RecipeScreen extends StatefulWidget {
  createState() => new RecipeScreenState();
}

class RecipeScreenState extends State<RecipeScreen> {
  /// Builds the recipe screen using the build method with [context].
  /// Documentation can be found here:
  /// https://docs.flutter.io/flutter/widgets/State/build.html
  @override
  Widget build(BuildContext context) {
    final FlushbarUtil flushbarHelper = FlushbarUtil.getInstance(StoreProvider.of(context));

    return StoreConnector<AppState, _ViewModel>(
      converter: (store) => _ViewModel(
            checkPlanUser: (meals) => store.dispatch(
                  UpdateMealsAction(meals: meals),
                ),
            plan: store.state.plan,
            week: store.state.week,
            meals: store.state.meals,
            displayHelp: store.state.displayHelp,
          ),
      onInit: (store) {
        store.dispatch(new RequestMealsDataEventsAction());
        store.dispatch(new RequestWeekDataEventsAction());
      },
      onDispose: (store) {
        store.dispatch(new CancelMealsDataEventsAction());
        store.dispatch(new CancelMealsDataEventsAction());
      },
      builder: (context, viewModel) => Scaffold(
            appBar: new AppBar(
              toolbarOpacity: 0.0,
              automaticallyImplyLeading: false,
              backgroundColor: MealMonkeyColours.primary,
              centerTitle: false,
              elevation: 0.0,
              title: new FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  'Feed the Troop',
                  style: MealMonkeyTextStyles.mainBold(Colors.white, 28.0),
                ),
              ),
              actions: <Widget>[
                new Container(
                  padding: EdgeInsets.only(top: 25.0, right: 15.0),
                  child: new Text(
                    Util.countString(viewModel.meals.length, 'meal'),
                    style: MealMonkeyTextStyles.mainSemiBold(Colors.white, 14.0),
                  ),
                )
              ],
            ),
            body: Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage("assets/images/background_graphics.png"),
                  fit: BoxFit.fill,
                ),
              ),
              child: buildRecipeCards(viewModel.meals, context, viewModel, flushbarHelper),
            ),
          ),
    );
  }

  /// Returns a [ListView.builder] containing recipe cards for
  /// each recipe in a provided list of [recipes]
  Widget buildRecipeCards(
    List<Meal> meals,
    BuildContext context,
    _ViewModel viewModel,
    FlushbarUtil flushbarHelper,
  ) {
    return new ListView.builder(
        padding: new EdgeInsets.only(
          top: 8.0,
          left: 8.0,
          right: 8.0,
        ),
        itemCount: meals.length,
        itemBuilder: (BuildContext context, int index) {
          return new Padding(
            padding: new EdgeInsets.only(top: 8.0, left: 0.0, right: 0.0, bottom: 15.0),
            child: Column(
              children: <Widget>[
                // Builds the recipe card
                recipeCard(meals[index], index, context, viewModel, flushbarHelper),
              ],
            ),
          );
        });
  }

  /// Builds the entire recipe card using the list [index]
  /// and the list of [recipes]
  Widget recipeCard(
    Meal meal,
    int index,
    BuildContext context,
    _ViewModel viewModel,
    FlushbarUtil flushbarHelper,
  ) {
    return new Container(
      decoration: DesignUtil.boxDecoration,
      child: new Card(
        elevation: 0.0,
        child: new InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => MealSpecificScreen(mealIndex: index),
              ),
            );
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Stack(
                children: <Widget>[
                  recipePicture(meal.recipe.picture),
                  buttons(viewModel, index, flushbarHelper),
                ],
              ),
              recipeBottom(viewModel, meal),
            ],
          ),
        ),
      ),
    );
  }

  /// Adds the recipe picture to the card using the list [index]
  /// and the list of [recipes]
  Widget recipePicture(String picture) {
    return Container(
      child: new SizedBox(
        height: 160.0,
        child: new Stack(
          children: <Widget>[
            new Positioned.fill(
              child: new ClipRRect(
                borderRadius: new BorderRadius.vertical(top: Radius.circular(6.0)),
                child: new CachedNetworkImage(
                  imageUrl: picture,
                  fit: BoxFit.cover,
                  placeholder: new Center(child: new CircularProgressIndicator()),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// Returns a [Container] containing a row with the edit recipe buttons.
  Widget buttons(_ViewModel viewModel, int index, FlushbarUtil flushbarHelper) {
    return new Container(
      padding: EdgeInsets.all(5.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          new Container(
            height: 30.0,
            width: 30.0,
            decoration: new BoxDecoration(
              color: MealMonkeyColours.white,
              borderRadius: new BorderRadius.all(const Radius.circular(10.0)),
            ),
            child: new IconButton(
              icon: new Icon(
                const IconData(0xe8c4, fontFamily: 'IonIcons'),
                size: 25.0,
                color: MealMonkeyColours.primary,
              ),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (_) => _editDialog(context, viewModel, index, flushbarHelper),
                );
              },
              padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            ),
          )
        ],
      ),
    );
  }

  /// Returns a [SimpleDialog] that the user can swap meals with and change the assigned users
  /// of the meal.
  Widget _editDialog(
      BuildContext context, _ViewModel viewModel, int index, FlushbarUtil flushbarHelper) {
    return new SimpleDialog(
      children: <Widget>[
        new Column(
          children: <Widget>[
            new Stack(
              children: <Widget>[
                new Container(
                  child: Text(
                    'Edit',
                    style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.black, 22.0),
                  ),
                  padding: EdgeInsets.only(top: 10.0),
                  alignment: Alignment.center,
                ),
                viewModel.displayHelp
                    ? new Container(
                        padding: EdgeInsets.only(bottom: 10.0, right: 10.0),
                        child: new IconButton(
                          icon: new Icon(
                            Icons.help,
                            color: MealMonkeyColours.primary,
                            size: 30.0,
                          ),
                          onPressed: () {
                            HelpDialogUtil.assigneeDialog(context);
                          },
                        ),
                        alignment: Alignment.topRight,
                        decoration: new BoxDecoration(
                          border:
                              new Border(bottom: new BorderSide(color: MealMonkeyColours.divider)),
                        ),
                      )
                    : new Container(),
              ],
            ),
            new Container(
              padding: EdgeInsets.fromLTRB(20.0, 15.0, 0.0, 5.0),
              alignment: Alignment.centerLeft,
              child: Text(
                "Assignees",
                style: MealMonkeyTextStyles.mainRegular(MealMonkeyColours.darkGrey, 16.0),
              ),
            ),
            new Container(
              padding: EdgeInsets.fromLTRB(10.0, 5.0, 5.0, 10.0),
              decoration: new BoxDecoration(
                border: new Border(bottom: new BorderSide(color: MealMonkeyColours.divider)),
              ),
              child: StatefulList(
                viewModel: viewModel,
                mealIndex: index,
              ),
            ),
            new Container(
              width: MediaQuery.of(context).size.width * 0.70,
              height: 60.0,
              padding: EdgeInsets.only(top: 15.0),
              child: new RaisedButton(
                color: MealMonkeyColours.primary,
                child: Text(
                  "SWAP MEAL",
                  style: MealMonkeyTextStyles.mainBold(Colors.white, 17.0),
                ),
                onPressed: () {
                  Navigator.pop(context);
                  _getNewMeal(context, viewModel, index, flushbarHelper);
                },
              ),
            )
          ],
        )
      ],
    );
  }

  /// Swaps the current recipe with another random recipe that fits within the budget.
  Future _getNewMeal(
    BuildContext context,
    _ViewModel viewModel,
    int index,
    FlushbarUtil flushbarHelper,
  ) async {
    flushbarHelper.dismissAll();

    flushbarHelper
        .create(
          type: FlushbarType.INFO,
          message: "Swapping out your recipe!",
          flushbarDuration: FlushbarDuration.UNLIMITED,
        )
        .show(context);

    // Generate meals on Cloud Firestore
    try {
      await CloudFunctions.instance.call(
        functionName: 'swapMeal',
        parameters: <String, dynamic>{
          'rejected_recipe': viewModel.meals[index].recipe.recipeReference.path,
          'rejected_meal': viewModel.meals[index].reference.path,
          'labels': viewModel.plan.labels,
          'budget': viewModel.plan.budget,
          'week': viewModel.week.reference.path,
        },
      );

      flushbarHelper.dismissAll();

      flushbarHelper
          .create(
            type: FlushbarType.SUCCESS,
            message: "We've swapped out your meal!",
            flushbarDuration: FlushbarDuration.SHORT,
          )
          .show(context);
    } on CloudFunctionsException catch (e) {
      flushbarHelper.dismissAll();

      flushbarHelper
          .create(
            type: FlushbarType.ERROR,
            message: e.message,
            flushbarDuration: FlushbarDuration.MEDIUM,
          )
          .show(context);
    } catch (e) {
      print(e);

      flushbarHelper.dismissAll();

      flushbarHelper
          .create(
            type: FlushbarType.ERROR,
            message: "Something went wrong while swapping your recipe.",
            flushbarDuration: FlushbarDuration.MEDIUM,
          )
          .show(context);
    }
    // flushbarHelper.dismissAll();
  }

  /// Returns a [Container] with the profile pictures of each user assigned to a meal.
  Widget _buildImages(List<DocumentReference> refs, Plan plan) {
    if (refs.length != 0) {
      return new Container(
        child: new Stack(
          children: new List.generate(
            refs.length,
            (int index) {
              PlanUser planUser = plan.getPlanUserFromRef(refs[index]);
              return Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(child: Util.showPic(planUser)),
                  new Container(
                    width: 17.0 * index, // image spacing
                  ),
                ],
              );
            },
          ),
        ),
      );
    } else {
      return new Container(
        height: 20.0,
      );
    }
  }

  /// Returns a [Container] with the meal name, price, assigned users, and rating.
  Widget recipeBottom(_ViewModel viewModel, Meal meal) {
    double textSize = 18.0;
    final currency = new NumberFormat("#,##0", "en_US");

    int rating = meal.recipe.rating.toInt();

    return new Container(
      child: new Stack(
        children: <Widget>[
          //left align
          new Container(
            padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                //Day
                Stack(
                  children: <Widget>[
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 3.0),
                        child: new Text(
                          DateFormat.EEEE()
                              .format(meal.date.toDate().toLocal())
                              .toString()
                              .toUpperCase(),
                          style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.fadedblack, 15.0),
                        ),
                      ),
                    ),
                    _buildImages(meal.users, viewModel.plan),
                  ],
                ),

                //Meal name
                Container(
                  margin: EdgeInsets.only(right: 35.0),
                  child: new Text(
                    meal.recipe.name,
                    style: MealMonkeyTextStyles.mainSemiBold(MealMonkeyColours.black, 20.0),
                  ),
                ),
                //Icons
                new Stack(
                  children: <Widget>[
                    new Row(
                      children: <Widget>[
                        new Text(
                          "\$${currency.format(meal.price)}",
                          style: MealMonkeyTextStyles.mainRegular(
                              MealMonkeyColours.fadedblack, textSize),
                        ),
                      ],
                    ),
                    // The star rating.
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        new Icon(IconData(0xe911, fontFamily: 'IonIcons'),
                            color: (rating >= 1) ? MealMonkeyColours.gold : MealMonkeyColours.grey,
                            size: 20.0),
                        new Icon(IconData(0xe911, fontFamily: 'IonIcons'),
                            color: (rating >= 2) ? MealMonkeyColours.gold : MealMonkeyColours.grey,
                            size: 20.0),
                        new Icon(IconData(0xe911, fontFamily: 'IonIcons'),
                            color: (rating >= 3) ? MealMonkeyColours.gold : MealMonkeyColours.grey,
                            size: 20.0),
                        new Icon(IconData(0xe911, fontFamily: 'IonIcons'),
                            color: (rating >= 4) ? MealMonkeyColours.gold : MealMonkeyColours.grey,
                            size: 20.0),
                        new Icon(IconData(0xe911, fontFamily: 'IonIcons'),
                            color: (rating >= 5) ? MealMonkeyColours.gold : MealMonkeyColours.grey,
                            size: 20.0),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

///Class created to help with assigning the users in the meal settings dialog
///Uses [viewModel] and [mealIndex]
class StatefulList extends StatefulWidget {
  StatefulList({
    Key key,
    this.viewModel,
    this.mealIndex,
  }) : super(key: key);

  final _ViewModel viewModel;
  final int mealIndex;

  @override
  State<StatefulWidget> createState() => new StatefulListState();
}

class StatefulListState extends State<StatefulList> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: widget.viewModel.plan.users.map((PlanUser user) {
        return _addUser(user);
      }).toList(),
    );
  }

  /// Returns a [Container] with an inkwell displaying the name and profile picture of the user.
  Widget _addUser(PlanUser user) {
    bool assigned = widget.viewModel.meals[widget.mealIndex].users.contains(user.userReference);
    return new Container(
      padding: EdgeInsets.symmetric(vertical: 8.0),
      child: new InkWell(
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                // Picture
                Container(padding: EdgeInsets.only(left: 10.0), child: Util.showPic(user)),
                // Name
                new Container(
                  alignment: Alignment.centerLeft,
                  width: MediaQuery.of(context).size.width - 180,
                  child: new FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Container(
                      padding: EdgeInsets.only(left: 10.0),
                      child: new Text(
                        user.username,
                        style: MealMonkeyTextStyles.mainRegular(
                          assigned ? MealMonkeyColours.primary : MealMonkeyColours.darkGrey,
                          15.0,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            //Tick
            new Container(
              alignment: Alignment.centerLeft,
              width: 40.0,
              child: assigned
                  ? Icon(
                      Icons.check,
                      color: MealMonkeyColours.primary,
                    )
                  : Container(),
            )
          ],
        ),
        onTap: () {
          assigned
              ? widget.viewModel.meals[widget.mealIndex].users.remove(user.userReference)
              : widget.viewModel.meals[widget.mealIndex].users.add(user.userReference);

          widget.viewModel.checkPlanUser(widget.viewModel.meals);
          setState(() {});
        },
      ),
    );
  }
}

typedef CheckPlanUser(List<Meal> meals);
typedef SetLoading(bool loading);

/// Class used by the store connector to connect firebase and
/// the local state of the application.
class _ViewModel {
  final CheckPlanUser checkPlanUser;
  final SetLoading setLoading;
  Week week;
  List<Meal> meals;
  Plan plan;
  bool displayHelp;

  // Constructs the view model.
  _ViewModel({
    this.checkPlanUser,
    this.plan,
    this.week,
    this.meals,
    this.setLoading,
    this.displayHelp,
  });
}
