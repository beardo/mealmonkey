import 'package:flutter/material.dart';
import 'package:mealmonkey/screens/onboarding/onboardingCooking.dart';
import 'package:mealmonkey/screens/onboarding/onboardingSetup.dart';
import 'package:mealmonkey/screens/onboarding/onboardingShopping.dart';
import 'dart:math';

import 'package:mealmonkey/screens/onboarding/onboardingWelcome.dart';
import 'package:mealmonkey/screens/util/colours.dart';
import 'package:mealmonkey/screens/util/fonts.dart';

class Onboarding extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new OnboardingState();
  }
}

class OnboardingState extends State<Onboarding> {
  PageController _pageController;
  int _page = 0;

  @override
  void initState() {
    super.initState();
    _pageController = new PageController();
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  void onPageChanged(int page) {
    setState(() {
      this._page = page;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: topBar(context, _page),
      body: new Stack(
        alignment: FractionalOffset.bottomCenter,
        children: <Widget>[
          // Pageview contains all the onboarding screens.
          new PageView(
            children: [
              new OnboardingWelcome(),
              new OnboardingSetup(),
              new OnboardingShopping(),
              new OnboardingCooking()
            ],
            controller: _pageController,
            onPageChanged: onPageChanged,
            physics: new ClampingScrollPhysics(),
          ),
          // Dots indictor at the bottom of the screen.
          new Container(
            padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.75),
            child: new Center(
              child: new DotsIndicator(
                controller: _pageController,
                itemCount: 4,
                color: MealMonkeyColours.primary,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

/// An indicator showing the currently selected page of a PageController
class DotsIndicator extends AnimatedWidget {
  DotsIndicator({
    this.controller,
    this.itemCount,
    this.onPageSelected,
    this.color: Colors.white,
  }) : super(listenable: controller);

  /// The PageController that this DotsIndicator is representing.
  final PageController controller;

  /// The number of items managed by the PageController
  final int itemCount;

  /// Called when a dot is tapped
  final ValueChanged<int> onPageSelected;

  /// The color of the dots.
  ///
  /// Defaults to `Colors.white`.
  final Color color;

  // The base size of the dots
  static const double _kDotSize = 8.0;

  // The increase in the size of the selected dot
  static const double _kMaxZoom = 2.0;

  // The distance between the center of each dot
  static const double _kDotSpacing = 25.0;

  Widget _buildDot(int index) {
    double selectedness = Curves.easeOut.transform(
      max(
        0.0,
        1.0 - ((controller.page ?? controller.initialPage) - index).abs(),
      ),
    );
    double zoom = 1.0 + (_kMaxZoom - 1.0) * selectedness;
    return new Container(
      width: _kDotSpacing,
      child: new Center(
        child: new Material(
          color: color,
          type: MaterialType.button,
          child: new Container(
            width: _kDotSize * zoom,
            height: _kDotSize * zoom,
            child: new InkWell(
              onTap: () => onPageSelected(index),
            ),
          ),
        ),
      ),
    );
  }

  Widget build(BuildContext context) {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: new List<Widget>.generate(itemCount, _buildDot),
    );
  }
}

// Top section of the page
Widget topBar(BuildContext context, int _page) {
  return new AppBar(
    backgroundColor: Colors.white,
    centerTitle: false,
    elevation: 0.0,
    actions: <Widget>[
      new FlatButton(
        child: new Row(
          children: <Widget>[
            new Text(
              _page == 3 ?  'Let\'s Go' : 'Skip',
              textAlign: TextAlign.right,
              style: MealMonkeyTextStyles.mainRegular(MealMonkeyColours.primary, 17.0),
            ),
            new Icon(
              Icons.arrow_forward_ios,
              color: MealMonkeyColours.primary,
            ),
          ],
        ),
        onPressed: () {
          Navigator.pushNamedAndRemoveUntil(context, '/register', (_) => false);
        },
      ),
    ],
  );
}
