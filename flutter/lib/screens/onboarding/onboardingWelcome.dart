import 'package:flutter/material.dart';
import 'package:mealmonkey/screens/util/colours.dart';
import 'package:mealmonkey/screens/util/fonts.dart';

class OnboardingWelcome extends StatelessWidget {
  //The main build method
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      // Adds the body of the app
      body: body(context),
    );
  }

// The main body of the page
  Widget body(BuildContext context) {
    Size phoneSize = MediaQuery.of(context).size;
    double verticalPadding = phoneSize.height * 0.02;
    return new Container(
      child: new Column(
        children: <Widget>[
          //image
          new Container(
            margin: EdgeInsets.only(top: verticalPadding),
            child: new Center(
              child: new Image.asset(
                'assets/images/main.png',
                fit: BoxFit.cover,
                height: MediaQuery.of(context).size.height * 0.37,
              ),
            ),
          ),

          //Welcome text
          new Container(
            margin: EdgeInsets.only(top: verticalPadding * 2),
            child: new Center(
              child: new Text(
                "Welcome to\nMeal Monkey",
                style: MealMonkeyTextStyles.mainBold(MealMonkeyColours.darkGrey, 34.0),
                textAlign: TextAlign.center,
              ),
            ),
          ),

          //Description text
          new Container(
            margin: EdgeInsets.only(top: verticalPadding * 3),
            width: phoneSize.width * 0.8,
            child: new Center(
              child: new Text(
                "All your meal plans expertly crafted by us, for you.",
                textAlign: TextAlign.center,
                style: MealMonkeyTextStyles.mainRegular(MealMonkeyColours.darkGrey, 19.0),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
