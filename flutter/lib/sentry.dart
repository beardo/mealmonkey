// Sentry Integration
//
// Sentry lets us report our errors to an online service so we can work out what issues the app
// is encountering.
import 'dart:async';

import 'package:sentry/sentry.dart';

class SentryUtil {
  static final SentryClient _sentry = SentryClient(
    dsn:
        "https://410cc35ffd21480b8d8c80b4cbe5cf36:28bf180ff5c440d79d98602c3e5aca20@sentry.io/1255288");

static bool get isInDebugMode {
  // Assume we're in production mode
  bool inDebugMode = false;

  // Assert expressions are only evaluated during development. They are ignored
  // in production. Therefore, this code will only turn `inDebugMode` to true
  // in our development environments!
  assert(inDebugMode = true);

  return inDebugMode;
}

static Future<Null> reportError(dynamic error, dynamic stackTrace) async {
  // Print the exception to the console
  print('Caught error: $error\nStacktrace: $stackTrace\n');
  if (isInDebugMode) {
    // Print the full stacktrace in debug mode
    print(stackTrace);
    return;
  } else {
    // Send the Exception and Stacktrace to Sentry in Production mode
    SentryUtil._sentry.captureException(
      exception: error,
      stackTrace: stackTrace,
    );
  }
}
}