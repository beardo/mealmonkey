// Util class for holding common properties used through the entire application.
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mealmonkey/firebase/support_models/planUser.dart';
import 'package:mealmonkey/screens/util/colours.dart';
import 'package:mealmonkey/screens/util/fonts.dart';
import 'package:flutter/rendering.dart';


class Util {
  // Defines the base URL to the backend API
  static final String api = "https://api.mealmonkey.nz";

  static final double pageWidthPadding = 10.0;

  ///a method to show the pictures of the assigned members
  static Widget showPic(PlanUser planUser) {
    return planUser.picture == null
        ? Container(
            width: 24.0,
            height: 24.0,
            padding: const EdgeInsets.all(1.0),
            decoration: new BoxDecoration(
              color: MealMonkeyColours.black,
              shape: BoxShape.circle,
            ),
            child: new CircleAvatar(
              maxRadius: 12.0,
              backgroundColor: MealMonkeyColours.primary,
              child: new Text(
                planUser.username[0].toUpperCase(),
                style: MealMonkeyTextStyles.mainBold(Colors.white, 17.0),
              ),
            ),
          )
        : Container(
            width: 24.0,
            height: 24.0,
            padding: const EdgeInsets.all(1.0),
            decoration: new BoxDecoration(
              color: MealMonkeyColours.white,
              shape: BoxShape.circle,
            ),
            child: new CircleAvatar(
              backgroundImage: new CachedNetworkImageProvider(planUser.picture),
              backgroundColor: Colors.grey,
              maxRadius: 12.0,
            ),
          );
  }

  /// Generates the string's with and without an 's' dependent on count
  static String countString(int count, String string) {
    String countString = "";
    countString += count.toString();
    count == 1 ? countString += " $string" : countString += " $string\s";
    return countString;
  }
}


