// Import flutter packages
import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:mealmonkey/firebase/models/plan.dart';
import 'package:mealmonkey/firebase/models/user.dart';
import 'package:mealmonkey/firebase/models/week.dart';
import 'package:mealmonkey/firebase/support_models/planUser.dart';
import 'package:mealmonkey/screens/auth/forgot.dart';
import 'package:mealmonkey/screens/auth/login.dart';
import 'package:mealmonkey/screens/auth/register.dart';
import 'package:mealmonkey/screens/ingredients.dart';
import 'package:mealmonkey/screens/onboarding/onboardingMain.dart';
import 'package:mealmonkey/screens/recipes.dart';
import 'package:mealmonkey/screens/settings.dart';
import 'package:mealmonkey/screens/setup/start.dart';
import 'package:mealmonkey/screens/util/colours.dart';
import 'package:mealmonkey/sentry.dart';
import 'package:mealmonkey/state/action.dart';
import 'package:mealmonkey/state/appstate.dart';
import 'package:mealmonkey/state/middleware.dart';
import 'package:mealmonkey/state/reducers.dart';
import 'package:redux/redux.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:redux_persist/redux_persist.dart';
import 'package:redux_persist_flutter/redux_persist_flutter.dart';

// Set up sentry crash analytics.
main() {
  FlutterError.onError = (FlutterErrorDetails details) {
    if (SentryUtil.isInDebugMode) {
      FlutterError.dumpErrorToConsole(details);
    } else {
      Zone.current.handleUncaughtError(details.exception, details.stack);
    }
  };

  /// Logs the user in if they did not logout in previous session.
  runZoned<Future<Null>>(() async {
    Firestore firestoreInstance = Firestore.instance;
    await firestoreInstance.settings(timestampsInSnapshotsEnabled: true);

    final persistor = new Persistor<AppState>(
      storage: FlutterStorage(key: "mealmonkey"),
      serializer: JsonSerializer<AppState>(AppState.fromJson),
      debug: true,
    );

    // Load initial state
    final initialState = await persistor.load();

    final store = new Store<AppState>(
      appReducer,
      initialState: initialState ?? new AppState.initialState(),
      middleware: [
        new EpicMiddleware(allEpics),
        persistor.createMiddleware(),
      ],
    );

    // Check if a user is logged in
    FirebaseUser existingFirebaseUser = await FirebaseAuth.instance.currentUser();

    if (existingFirebaseUser != null) {
      DocumentSnapshot userSnapshot = await Firestore.instance
          .collection(User.collectionName)
          .document(existingFirebaseUser.uid)
          .get();

      if (userSnapshot.exists) {
        // Convert the Firebase user to an instance of our frontend users
        User loggedInUser = User.from(userSnapshot);

        // Put our new user in the Redux store
        store.dispatch(UpdateLocalUserAction(user: loggedInUser));

        // Check if we have a new fcmToken
        String fcmToken = await FirebaseMessaging().getToken();
        if (!store.state.user.fcmToken.contains(fcmToken)) {
          store.state.user.fcmToken.add(fcmToken);
          store.dispatch(UpdateUserAction(user: store.state.user));
        }

        // Check if the user is setup
        if (store.state.user.setup) {
          // Get the user's plan and put it in the Redux store
          store.dispatch(UpdateLocalPlanAction(plan: Plan.from(await store.state.user.plan.get())));

          // // Check to see if we need to add the new FCM Token to the user in the plan
          PlanUser planUser = store.state.plan.getPlanUserFromRef(store.state.user.reference);
          if (!planUser.fcmToken.contains(fcmToken)) {
            store.state.plan.addFCMTokenToPlanUser(store.state.user.reference, fcmToken);
            store.dispatch(UpdatePlanAction(plan: store.state.plan));
          }

          // Get the plan's current week and put it in the Redux store
          store.dispatch(
            UpdateLocalWeekAction(
              week: Week.from(await store.state.plan.getCurrentWeek()),
            ),
          );

          // Get the list of meals from the plan's current week and put them in the Redux store
          store.dispatch(UpdateLocalMealsAction(meals: await store.state.week.getMeals()));
        }
      } else {
        FirebaseAuth.instance.signOut();
      }
    }

    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then((_) {
      runApp(MealMonkeyApp(
        store: store,
        persistor: persistor,
      ));
    });
  }, onError: (error, stackTrace) {
    SentryUtil.reportError(error, stackTrace);
  });
}

class MyBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}

class MealMonkeyApp extends StatefulWidget {
  MealMonkeyApp({this.store, this.persistor});

  final Store<AppState> store;
  final Persistor<AppState> persistor;

  @override
  MealMonkeyAppState createState() => MealMonkeyAppState();
}

class MealMonkeyAppState extends State<MealMonkeyApp> {
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  void firebaseCloudMessagingListeners() {
    if (Platform.isIOS) iOSPermissions();

    _firebaseMessaging.getToken().then((token) {
      print('\n\nFCM Token: ' + token + '\n\n');
    });

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');
      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },
    );
  }

  void iOSPermissions() {
    _firebaseMessaging.requestNotificationPermissions(
      IosNotificationSettings(sound: true, badge: true, alert: true),
    );
    _firebaseMessaging.onIosSettingsRegistered.listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  @override
  void initState() {
    super.initState();
    firebaseCloudMessagingListeners();
  }

  @override

  /// Builds the ingredients screen using the build method with [context].
  /// Documentation can be found here:
  /// https://docs.flutter.io/flutter/widgets/State/build.html
  Widget build(BuildContext context) {
    FirebaseAnalytics analytics = new FirebaseAnalytics();

    return StoreProvider<AppState>(
      store: widget.store,
      child: new MaterialApp(
        builder: (context, child) {
          return ScrollConfiguration(
            behavior: MyBehavior(),
            child: child,
          );
        },
        navigatorObservers: [
          new FirebaseAnalyticsObserver(analytics: analytics),
        ],
        theme: ThemeData(primaryColor: MealMonkeyColours.primary),
        title: 'Meal Monkey',
        home: getStartRoute(widget.store),
        routes: <String, WidgetBuilder>{
          '/register': (BuildContext context) => new RegisterScreen(),
          '/login': (BuildContext context) => new LoginScreen(),
          '/forgot': (BuildContext context) => new ForgotScreen(),
        },
        debugShowCheckedModeBanner: false,
      ),
    );
  }

  /// Returns a [Widget] route depending on if the user is logged in, done onboarding or has never
  /// created an account.
  Widget getStartRoute(Store<AppState> store) {
    if (store.state.onboarded) {
      // Check if user is set
      if (store.state.user == null) {
        return new RegisterScreen();
      } else {
        // Check to see if setup
        if (store.state.user.setup) {
          return new MainPage();
        } else {
          return new SetupStart();
        }
      }
    } else {
      store.dispatch(new SetOnboardedAction(onboarded: true));
      return new Onboarding();
    }
  }
}

class MainPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _MainPageState();
  }
}

class _MainPageState extends State<MainPage> {
  PageController _pageController;
  int _page = 0;

  @override

  /// Builds the ingredients screen using the build method with [context].
  /// Documentation can be found here:
  /// https://docs.flutter.io/flutter/widgets/State/build.html
  Widget build(BuildContext context) {
    return new WillPopScope(
      onWillPop: () => Future<bool>.value(false),
      child: new Scaffold(
        // Builds the three main pages.
        body: new PageView(
          physics: new NeverScrollableScrollPhysics(),
          children: [
            new RecipeScreen(),
            new IngredientScreen(),
            new SettingsScreen(),
          ],
          controller: _pageController,
          onPageChanged: onPageChanged,
        ),
        bottomNavigationBar: new CupertinoTabBar(
          activeColor: MealMonkeyColours.primary,
          backgroundColor: Colors.white.withOpacity(0.1),
          items: [
            new BottomNavigationBarItem(
              icon: new Icon(const IconData(0xe805, fontFamily: 'IonIcons')),
              title: new Text('Recipes'),
            ),
            new BottomNavigationBarItem(
              icon: new Icon(const IconData(0xe8af, fontFamily: 'IonIcons')),
              title: new Text('Ingredients'),
            ),
            new BottomNavigationBarItem(
              icon: new Icon(Icons.settings),
              title: new Text('Settings'),
            ),
          ],
          onTap: navigationTapped,
          currentIndex: _page,
        ),
      ),
    );
  }

  /// Navigates to the input [page] on the bottom navigation bar.
  void navigationTapped(int page) {
    _pageController.animateToPage(
      page,
      duration: const Duration(milliseconds: 300),
      curve: Curves.ease,
    );
  }

  @override
  void initState() {
    super.initState();
    _pageController = new PageController();
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  void onPageChanged(int page) {
    setState(() {
      this._page = page;
    });
  }
}
