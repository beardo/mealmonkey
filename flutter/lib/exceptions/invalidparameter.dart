
/// Exception for invalid parameters 
class InvalidParameterException implements Exception {
  String errMsg() => 'Invalid parameter given.';
}