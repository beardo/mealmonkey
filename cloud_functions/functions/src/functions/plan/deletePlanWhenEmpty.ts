import * as functions from 'firebase-functions';
import collectionNames from '../../models/collectionNames';

export const deletePlanWhenEmpty = functions.firestore.document('plans/{planId}').onUpdate(
    async (change, context) => {
        const newPlanData: FirebaseFirestore.DocumentData = change.after.data();
        const planReference: FirebaseFirestore.DocumentReference = change.after.ref;
        const users: any[] = newPlanData.users;
        const promises: Promise<FirebaseFirestore.WriteResult>[] = [];

        if (users.length === 0) {
            planReference.collection(collectionNames.WEEKS).get().
            then((weeks) => {
                weeks.forEach((week) => {
                    week.ref.collection(collectionNames.MEALS).get()
                    .then((meals) => {
                        meals.forEach((meal) => {
                            promises.push(meal.ref.delete());
                        });
                    }).catch((error) => {
                        console.error('Couldn\'t get meals for week ' + week.ref.id + ' in plan ' + planReference.id + '. Error: ' + error);
                        return 0;                
                    });
                    promises.push(week.ref.delete());
                });
            }).catch((error) => {
                console.error('Couldn\'t get weeks for plan ' + planReference.id + '. Error: ' + error);
                return 0;                
            });

            Promise.all(promises).then(() => {
                planReference.delete().then(() => {
                    return 0;
                }).catch((error) => {
                    console.error('Plan ' + planReference.id + ' couldn\'t be deleted. Error: ' + error);
                    return 0;
                });
            }).catch((error) => {
                console.error('Plan ' + planReference.id + ' and its subcollections couldn\'t be deleted. Error: ' + error);
                return 0;
            });
        }
    }
);