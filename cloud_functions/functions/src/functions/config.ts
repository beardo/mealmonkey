import * as functions from 'firebase-functions';

const SENDGRID_API_KEY: String = (functions.config().sendgrid) ? functions.config().sendgrid.key : undefined;
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(SENDGRID_API_KEY);

export { sgMail };