import * as functions from 'firebase-functions';
import { firestore, messaging } from 'firebase-admin';
import collectionNames from '../../models/collectionNames';
// import moment = require('moment');
import moment_tz = require('moment-timezone');
moment_tz.tz.setDefault("Pacific/Auckland");

export const sendNotificationsToCooks = functions.pubsub.topic("sendNotificationsToCooks").onPublish(async (event) => {

    // Get all plans from Firestore
    const plans: firestore.QuerySnapshot = await firestore().collection(collectionNames.PLANS).get();

    // Create a notifications object which we will use to store the meal name and which user 
    // messaging tokens we are going to message.
    const notifications: { mealName: string, fcmTokens: string[] } = { mealName: "", fcmTokens: [] }

    // Iterate over each plan
    // await plans.docs.forEach(async (plan: FirebaseFirestore.QueryDocumentSnapshot) => {
    for (const plan of plans.docs) {

        // Get the current date and set its time to the start of the way (midnight)
        const now = new Date();
        now.setHours(0, 0, 0, 0);

        // Get the date seven days from now and set its time to right before end of the day (11:59)
        const weekLater = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 7);
        weekLater.setHours(23, 59, 59, 999);

        // Get the current week filtering by end_date on now and seven days from now
        const currentWeekQuery: firestore.QuerySnapshot = await plan
            .ref
            .collection(collectionNames.WEEKS)
            .where("end_date", ">=", now)
            .where("end_date", "<=", weekLater)
            .get();

        // Check to see if we have more than one week meeting our current week query
        if (currentWeekQuery.docs.length !== 1) {
            // Because we have more than one week for the plan, something has gone wrong. Log the error.
            console.error('Sending notifications failed for plan ' + plan.ref.path + '. Query weeks query (to get current week) got ' + currentWeekQuery.docs.length + ' documents.');
        } else {
            // Get the Document Snapshot of the current week
            const currentWeek: FirebaseFirestore.QueryDocumentSnapshot = currentWeekQuery.docs[0];

            console.log(moment_tz.tz('Pacific/Auckland').utc().startOf('day').toDate());

            // Get the meals from the current week
            const currentWeekTodaysMeal: firestore.QuerySnapshot = await currentWeek
                .ref
                .collection(collectionNames.MEALS)
                .where('date', '==', moment_tz.tz('Pacific/Auckland').startOf('day').utc().toDate())
                .get();

            // currentWeekTodaysMeal.do

            console.log(currentWeekTodaysMeal.docs.length);

            // currentWeekTodaysMeal.docs.forEach((todayMeal: FirebaseFirestore.QueryDocumentSnapshot) => {
            //     console.log(`todayMeal: ${JSON.stringify(todayMeal.data())}`);
            // });

            // Check to see that we have a meal scheduled for today
            if (currentWeekTodaysMeal.docs.length === 1) {
                // Get the meal from the query
                const meal: FirebaseFirestore.QueryDocumentSnapshot = currentWeekTodaysMeal.docs[0];
                const mealName: string = meal.data().recipe.name
                const fcmTokens: string[] = []

                // Iterate over each user in the meal users
                meal.data().users.forEach((mealUser) => {

                    // Iterate over each user in the plan and find the matching one so that we can
                    // push their FCM Token into the tokens array.
                    plan.data().users.forEach((planUser) => {
                        if (planUser.ref.id === mealUser.id) {
                            planUser.fcm_token.forEach((fcmToken) => {
                                fcmTokens.push(fcmToken);
                            });
                        }
                    });

                });

                // Get the day of week string for the curent day (e.g. 'Monday')
                const dayOfWeekString: string = now.toLocaleString('en-US', { timeZone: 'Pacific/Auckland', weekday: 'long' });

                // Get the days config object
                const daysConfig = plan.data().days_config

                // Check to see if the days config object has the current day of week string 
                // (e.g. does it have a 'Monday' key)
                if (dayOfWeekString in daysConfig) {

                    // Iterate over each user for the day and add their token to the fcmTokens array
                    daysConfig[dayOfWeekString].forEach((daysConfigUser) => {

                        // Iterate over each user in the plan and find the matching one so that we can
                        // push their FCM Token into the tokens array.
                        plan.data().users.forEach((planUser) => {
                            if (planUser.ref.id === daysConfigUser.id) {
                                planUser.fcm_token.forEach((fcmToken) => {
                                    fcmTokens.push(fcmToken);
                                });
                            }
                        });

                    });

                }

                // Set the notifications object to the current meal and fcmTokens
                notifications.mealName = mealName;
                notifications.fcmTokens = fcmTokens;

                console.log(mealName);
                console.log(fcmTokens);
            } else {
                if (currentWeekTodaysMeal.docs.length > 1) {
                    console.error('Sending notifications failed for plan ' + plan.ref.path + '. Todays meal query returned ' + currentWeekTodaysMeal.docs.length + ' documents.');
                }
            }
        }
    }

    console.log(notifications);

    // Check to see if we have any tokens to send a push notification too
    if (notifications.fcmTokens.length > 0) {
        // Create the push notification
        const payload = {
            notification: {
                title: 'Meal Monkey',
                body: getPushMessageWithMealName(notifications.mealName)
            }
        };

        console.log(payload.notification.body);

        // Send it to all fcmTokens who are cooking today
        messaging().sendToDevice(
            notifications.fcmTokens,
            payload
        ).then((response) => {
            console.log("Push notification sent to " + notifications.fcmTokens.length + " recipients.");
            console.log("Successfully sent message: " + response);
        }).catch((error) => {
            console.log("Error sending message: ", error);
        });

        console.log('sent');

        return 0;
    } else {
        console.log("No push notifications to send.")
        return 0;
    }
});

function getPushMessageWithMealName(mealname: string): string {
    const randomMessage: number = randomIntFromInterval(0, 3);
    if (randomMessage === 0) {
        return "You're cooking " + mealname + " tonight. Have you got all the ingredients?";
    } else if (randomMessage === 1) {
        return "You're cooking " + mealname + " tonight. Do you need to stop by the store?";
    } else if (randomMessage === 2) {
        return "You're cooking " + mealname + " tonight. Are you prepared?";
    } else {
        return mealname + " is on the menu tonight. Have you got everything?";
    }
}

function randomIntFromInterval(min, max): number {
    return Math.floor(Math.random() * (max - min + 1) + min);
}
