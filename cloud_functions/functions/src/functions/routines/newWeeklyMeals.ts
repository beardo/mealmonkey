import * as functions from 'firebase-functions';
import { firestore, messaging } from 'firebase-admin';
import collectionNames from '../../models/collectionNames';
import * as generate from '../meal/generateMeals';

export const newWeeklyMeals = functions.pubsub.topic("newWeeklyMeals").onPublish(async (event) => {
// export const newWeeklyMeals = functions.runWith({timeoutSeconds: 540, memory: '2GB'}).firestore.document('notinuse').onCreate(async (data, _) => {

    // Get all plans from Firestore
    const plans: firestore.QuerySnapshot = await firestore().collection(collectionNames.PLANS).get();

    // Iterate over each plan
    for (const plan of plans.docs) {

        // Get the needed data from the plan
        const planData: firestore.DocumentData = plan.data();

        const labels: string[] = planData.labels;
        const required_meals: number = Object.keys(planData.days_config).length;
        const plan_id: string = plan.id;
        const total_budget: number = planData.budget;

        // Generate new meals for the plan
        await generate.meals(labels, required_meals, plan_id, total_budget);
    }
});
