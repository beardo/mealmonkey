import * as functions from 'firebase-functions';
import { firestore } from 'firebase-admin';
import collectionNames from '../../models/collectionNames';
import * as util from './util';

// data = { rating: 3, recipe: '/recipes/-LNsZkOY5OGV7xFDikXq', user: '/users/LgAXEZYP2BXujXfDYw4hGvVdc2I3' }

// export const rateMeal = functions.runWith({ memory: '1GB', timeoutSeconds: 540 }).firestore.document('notinuse').onCreate(async (data, _) => {
//     const rating: number = data.data().rating
//     const recipeRef: FirebaseFirestore.DocumentReference = firestore().doc(data.data().recipe);
//     const userRef: FirebaseFirestore.DocumentReference = firestore().doc(data.data().user);

export const rateMeal = functions.https.onCall(async (data, context) => {
    const rating: number = data.rating;
    const recipeRef: FirebaseFirestore.DocumentReference = firestore().doc(data.recipe);
    const userRef: FirebaseFirestore.DocumentReference = firestore().doc(data.user);

    // Get the recipe and history objects needed for the rating update
    const recipeSnapshot: FirebaseFirestore.DocumentSnapshot = await recipeRef.get();
    const recipeData: FirebaseFirestore.DocumentData = recipeSnapshot.data();

    const historyRef: FirebaseFirestore.DocumentReference = await userRef.collection(collectionNames.HISTORIES).doc(recipeRef.id);
    const historySnapshot: FirebaseFirestore.DocumentSnapshot = await historyRef.get();
    const historyData: FirebaseFirestore.DocumentData = historySnapshot.data();

    // Get the new total ratings for the app and increment the number of ratings given
    let count = recipeData.rating_count;
    let average = recipeData.rating_average;
    let total = (average * count) + rating;
    count = count + 1;
    
    // If the user has rated the meal before, remove their old rating
    if (historyData.rating !== 0) {
        total = total - historyData.rating;
        count = count - 1;
    }

    // Find the new average rating
    average = total / count;

    // Update the user's history of rating the meal
    await historyRef.update({
        rating: rating,
    });

    // Set the new rating in the recipe
    await recipeRef.update({
        rating_average: average,
        rating_count: count,
    });

    // Find the recipe object copy that the user has and update its rating as well
    const user: FirebaseFirestore.DocumentSnapshot = await userRef.get()
    const queryMeals: FirebaseFirestore.QuerySnapshot = await user.data().plan
        .collection(collectionNames.WEEKS)
        .doc(util.getFormatedStartOfWeek())
        .collection(collectionNames.MEALS)
        .get();

    for(const queryMeal of queryMeals.docs) {
        if (queryMeal.data().recipe.ref.path === recipeRef.path) {
            const userRatings: any[] = queryMeal.data().recipe.user_ratings;
            for(const userRating of userRatings) {
                if (userRating.ref.path === userRef.path) {
                    userRating.rating = rating;
                }
            };

            await queryMeal.ref.update({
                'recipe.rating': average,
                'recipe.user_ratings': userRatings,
            });
        }
    }
});