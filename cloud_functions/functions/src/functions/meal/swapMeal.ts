import * as functions from 'firebase-functions';
import * as util from './util';
import collectionNames from '../../models/collectionNames';
import { firestore } from 'firebase-admin';

// data = { rejected_recipe: 'recipes/-LNs_qOz-MOBy_DuqIdG', rejected_meal: '/plans/WTYWQ3/weeks/07-10-2018/meals/09-10-2018', week: '/plans/WTYWQ3/weeks/07-10-2018', labels: [], budget: 500 }

// export const swapMeal = functions.runWith({ memory: '1GB', timeoutSeconds: 540 }).firestore.document('notinuse').onCreate(async (data, _) => {
//     const rejectedRecipeRef: FirebaseFirestore.DocumentReference = firestore().doc(data.data().rejected_recipe); // Reference to the rejected recipe
//     const rejectedMealRef: FirebaseFirestore.DocumentReference = firestore().doc(data.data().rejected_meal); // Reference to the rejected meal
//     const weekRef: FirebaseFirestore.DocumentReference = firestore().doc(data.data().week);
//     const labels: string[] = data.data().labels; // The plan's labels
//     const budget: number = data.data().budget; // The plan's budget
//     const users: any[] = data.data().users;

// data includes: rejected (reference), labels (string[]), budget (number), week (reference), meals (object[]), users (references[])
export const swapMeal = functions.https.onCall(async (data, _) => {
    const rejectedRecipeRef: FirebaseFirestore.DocumentReference = firestore().doc(data.rejected_recipe); // Reference to the rejected recipe
    const rejectedMealRef: FirebaseFirestore.DocumentReference = firestore().doc(data.rejected_meal); // Reference to the rejected meal
    const weekRef: FirebaseFirestore.DocumentReference = firestore().doc(data.week);
    const labels: string[] = data.labels; // The plan's labels
    const budget: number = data.budget; // The plan's budget

    const weekSnapshot: FirebaseFirestore.DocumentSnapshot = await weekRef.get();
    const weekData: FirebaseFirestore.DocumentData = weekSnapshot.data();
    const tried: FirebaseFirestore.DocumentReference[] = weekData.tried;

    // // The frontend's meals collection
    const mealsSnapshot: FirebaseFirestore.QuerySnapshot = await weekRef
        .collection(collectionNames.MEALS)
        .get();

    const meals: FirebaseFirestore.QueryDocumentSnapshot[] = mealsSnapshot.docs;

    let usedBudget: number = 0;
    const all_recipes: FirebaseFirestore.QueryDocumentSnapshot[] = [];
    for (const meal of meals) {
        if (meal.data().recipe.ref.path !== rejectedRecipeRef.path) {
            usedBudget += meal.data().price;
            all_recipes.push(await meal.data().recipe.ref.get());
        }
    }
    const remainingBudget: number = budget - usedBudget; // Calculate the remaining budget

    // // Get all the recipes that match the dietary requirements and are not in tried
    const recipes: FirebaseFirestore.QueryDocumentSnapshot[] = await util.getMatchingRecipes(labels, tried);
    let newRecipe: FirebaseFirestore.QueryDocumentSnapshot = null;

    // // Randomly choose recipes and get their prices
    while (recipes.length > 0 && newRecipe === null) {
        const randomMealIndex: number = util.randomIntFromInterval(0, recipes.length - 1);
        const recipePrice = recipes[randomMealIndex].data().price;

        // Select the random recipe if it fits within the remaining budget
        if (recipePrice <= remainingBudget) {
            newRecipe = recipes[randomMealIndex];
        } else {
            recipes.splice(randomMealIndex, 1);
        }
    }

    if (newRecipe === null) {
        // error handling of some sort
        console.error('Didn\'t find a new recipe.');
        throw new functions.https.HttpsError('invalid-argument', 'There were no recipes to replace your current recipe.');
    } else {

        all_recipes.push(newRecipe);
        tried.push(newRecipe.ref);

        // Find the new combined ingredients page
        const combine_results: any[][] = await util.combineIngredients(all_recipes);
        const combined_departments = combine_results[0];
        const all_products: FirebaseFirestore.DocumentSnapshot[] = combine_results[1];

        // Update the current week object
        await weekRef.update({
            'departments': combined_departments,
            'tried': tried,
        });

        // Put together the department list for the new meal
        const departments: any[] = []
        const newRecipeData: FirebaseFirestore.DocumentData = newRecipe.data();
        for (const department of newRecipeData.departments) {
            const new_department_object = {
                'name': department.name,
                'ingredients': [],
            };
            departments.push(new_department_object);

            for (const ingredient of department.ingredients) {
                let new_ingredient_object: FirebaseFirestore.DocumentData;

                if (department.name !== 'Staple') {
                    for (const product of all_products) {
                        if (ingredient.product.id === product.id) {
                            new_ingredient_object = {
                                'name': product.data().name,
                                'quantity': ingredient.quantity,
                                'unit': product.data().unit,
                                'checked': 'IngredientStatus.NOT_CHECKED',
                            };

                            new_department_object.ingredients.push(new_ingredient_object);
                        }
                    }
                } else {
                    new_ingredient_object = {
                        'name': ingredient.name,
                        'quantity': ingredient.quantity,
                        'unit': ingredient.unit,
                        'checked': 'IngredientStatus.NOT_CHECKED',
                    };

                    new_department_object.ingredients.push(new_ingredient_object);
                }
            }
        }

        const rejectedMealSnapshot: FirebaseFirestore.DocumentSnapshot = await rejectedMealRef.get();
        const userRatings: any[] = []

        // const rejectedMealUsers = rejectedMealSnapshot.data().users;
        // if (rejectedMealUsers !== undefined && rejectedMealUsers.length !== 0) {
        
        // Get the plan
        const planSnapshot: FirebaseFirestore.DocumentSnapshot = await weekRef.parent.parent.get();
        console.log(planSnapshot.data());
        for (const user of planSnapshot.data().users) {

            const historySnapshot: FirebaseFirestore.DocumentSnapshot = await user.ref
                .collection(collectionNames.HISTORIES)
                .doc(rejectedRecipeRef.id)
                .get();

            if (historySnapshot.data().total_times_cooked === 0) {
                await historySnapshot.ref.delete();
            }

            const newHistorySnapshot: FirebaseFirestore.DocumentSnapshot = await user.ref
                .collection(collectionNames.HISTORIES)
                .doc(newRecipe.ref.id)
                .get();

            console.log('ref.id: ' + newRecipe.ref.id);
            // console.log('.id: ' + newRecipe.id);

            if (newHistorySnapshot.exists) {
                const new_user_rating_object: any = {
                    'ref': user.ref,
                    'rating': newHistorySnapshot.data().rating,
                };

                userRatings.push(new_user_rating_object);
            } else {
                const new_history_object: FirebaseFirestore.DocumentData = {
                    'ref': newRecipe.ref,
                    'total_times_cooked': 0,
                    'rating': 0,
                }
                await newHistorySnapshot.ref.set(new_history_object);

                const new_user_rating_object: any = {
                    'ref': user.ref,
                    'rating': 0,
                };
                userRatings.push(new_user_rating_object);
            }
        };
        // }

        // const rejectedMealUsers = rejectedMealSnapshot.data().users;
        // if (rejectedMealUsers !== undefined && rejectedMealUsers.length !== 0) {
        //     for (const user of rejectedMealSnapshot.data().users) {
        //         const historySnapshot: FirebaseFirestore.DocumentSnapshot = await user
        //             .collection(collectionNames.HISTORIES)
        //             .doc(rejectedRecipeRef.id)
        //             .get();
        //         if (historySnapshot.data().total_times_cooked === 0) {
        //             await historySnapshot.ref.delete();
        //         }

        //         const newHistorySnapshot: FirebaseFirestore.DocumentSnapshot = await user
        //             .collection(collectionNames.HISTORIES)
        //             .doc(newRecipe.ref.id)
        //             .get();

        //         console.log('ref.id: ' + newRecipe.ref.id);
        //         // console.log('.id: ' + newRecipe.id);

        //         if (newHistorySnapshot.exists) {
        //             const new_user_rating_object: any = {
        //                 'ref': user.ref,
        //                 'rating': newHistorySnapshot.data().rating,
        //             };

        //             userRatings.push(new_user_rating_object);
        //         } else {
        //             const new_history_object: FirebaseFirestore.DocumentData = {
        //                 'ref': newRecipe.ref,
        //                 'total_times_cooked': 0,
        //                 'rating': 0,
        //             }
        //             await newHistorySnapshot.ref.set(new_history_object);

        //             const new_user_rating_object: any = {
        //                 'ref': user.ref,
        //                 'rating': 0,
        //             };
        //             userRatings.push(new_user_rating_object);
        //         }
        //     };
        // }

        // Create the new recipe object
        const new_recipe_object: any = {
            'ref': newRecipe.ref,
            'name': newRecipeData.name,
            'link': newRecipeData.link,
            'picture': newRecipeData.picture,
            'time': newRecipeData.time,
            'difficulty': newRecipeData.difficulty,
            'servings': newRecipeData.servings,
            'rating': newRecipeData.rating_average,
            'user_ratings': userRatings,
            'author': newRecipeData.author,
            'labels': newRecipeData.labels,
            'departments': departments,
        }

        // Update the meal
        await rejectedMealRef.update({
            'recipe': new_recipe_object,
            'total_times_cooked': 0,
            'price': newRecipeData.price,
        });

        console.log('Swap meal was successful. New meal generated: ' + newRecipeData.name);

        // console.log(`rejectedMealSnapshot: ${JSON.stringify(rejectedMealSnapshot.data())}`);

        return 0;
    }
});