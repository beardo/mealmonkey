import * as functions from 'firebase-functions';
import { firestore } from 'firebase-admin';
import * as util from './util';
import collectionNames from '../../models/collectionNames';
// import moment = require('moment');
import moment_tz = require('moment-timezone');
moment_tz.tz.setDefault("Pacific/Auckland");

// export const generateMeals = functions.runWith({ memory: '1GB', timeoutSeconds: 540 }).firestore.document('notinuse').onCreate(async (data, _) => {
//     const labels: string[] = data.data().labels;
//     const required_meals: number = data.data().required_meals;
//     const plan_id: string = data.data().plan_id;
//     const total_budget: number = data.data().total_budget;

// data = { labels: [], tried: [], required_meals: 4, total_budget: 200, plan_id: 'KP5TS8' }

export const generateMeals = functions.https.onCall(async (data, context) => {
    const labels: string[] = data.labels;
    const required_meals: number = data.required_meals;
    const plan_id: string = data.plan_id;
    const total_budget: number = data.total_budget;

    await meals(labels, required_meals, plan_id, total_budget);
});

export async function meals(labels: string[], required_meals: number, plan_id: string, total_budget: number) {
    const plan: FirebaseFirestore.DocumentSnapshot = await firestore()
        .collection(collectionNames.PLANS)
        .doc(plan_id)
        .get();

    // Update user's histories based on the last week's meals
    const lastWeekQuery: FirebaseFirestore.QuerySnapshot = await plan.ref
        .collection(collectionNames.WEEKS)
        .where('start_date', '==', moment_tz.tz('Pacific/Auckland').startOf('isoWeek').subtract(1, 'weeks').utc().toDate())
        .get();
    if (lastWeekQuery.size > 0) {
        const lastWeekRecipeRefs: FirebaseFirestore.DocumentReference[] = [];
        const lastWeekMeals: FirebaseFirestore.QuerySnapshot = await lastWeekQuery.docs[0].ref.collection(collectionNames.MEALS).get();
        lastWeekMeals.docs.forEach((lastWeekMeal: FirebaseFirestore.QueryDocumentSnapshot) => {
            const lastWeekMealRecipe: FirebaseFirestore.DocumentData = lastWeekMeal.data().recipe;
            lastWeekRecipeRefs.push(lastWeekMealRecipe.ref);
        });

        plan.data().users.forEach(async (planUser: FirebaseFirestore.DocumentData) => {
            for (const lastWeekRecipeRef of lastWeekRecipeRefs) {
                const planUserRef: FirebaseFirestore.DocumentReference = planUser.ref;
                const historySnapshot: FirebaseFirestore.DocumentSnapshot = await planUserRef.collection(collectionNames.HISTORIES).doc(lastWeekRecipeRef.id).get();
                await historySnapshot.ref.update({
                    'total_times_cooked': historySnapshot.data().total_times_cooked + 1
                });
            }
        })
    }

    const mealSolver: util.MealSolver = new util.MealSolver();

    // Get recipes that match the labels
    mealSolver.allMeals = await util.getMatchingRecipes(labels, []);

    // mealSolver.all_meals.forEach((meal) => {
    //     console.log(`all_meals meal: ${JSON.stringify(meal)}`);
    // });

    // Call meal generator function
    mealSolver.randomInt = util.randomIntFromInterval(0, mealSolver.allMeals.length - 1)

    const tryLimit: number = 5;
    let numberOfTries: number = 0;
    while (numberOfTries < tryLimit) {
        try {
            await mealSolver.solve(mealSolver.randomInt, required_meals, total_budget, []);
            numberOfTries = tryLimit;
        } catch (error) {
            // Error
            console.error('Meal solver overflowed the stack... again...');
            numberOfTries++;
        }
    }

    if (mealSolver.candidateMeals.length === 0) {
        // No meals found, return relevant error
        if (numberOfTries === tryLimit) {
            // Return specific error to unluckiness
            throw new functions.https.HttpsError('cancelled', 'We were unable to genearte you a set of meals. Please try again.');
        }
        // No matching meals
        throw new functions.https.HttpsError('invalid-argument', 'We could not generate a set of meals to meet your specifications.');
    }

    let new_meals: FirebaseFirestore.DocumentSnapshot[] = []
    new_meals = mealSolver.candidateMeals[0];

    // Create the tried_meals list
    const tried_meals: FirebaseFirestore.DocumentReference[] = []
    new_meals.forEach((meal) => {
        tried_meals.push(meal.ref);
    });

    // Create the departments for the week
    const combine_results: any[][] = await util.combineIngredients(new_meals);
    const combined_departments = combine_results[0];
    const all_products: FirebaseFirestore.DocumentSnapshot[] = combine_results[1];

    // Create new week and set week's tried recipes to generated recipes
    const startOfWeek: Date = util.getStartOfWeek();
    const endOfWeek: Date = util.getEndOfWeek();
    const startOfWeekFormatted: string = util.getFormatedStartOfWeek();

    const newWeek: FirebaseFirestore.DocumentData = {
        'start_date': startOfWeek,
        'end_date': endOfWeek,
        'tried': tried_meals,
        'departments': combined_departments,
    }

    try {
        await firestore()
            .collection(collectionNames.PLANS)
            .doc(plan_id)
            .collection(collectionNames.WEEKS)
            .doc(startOfWeekFormatted)
            .set(newWeek);
    } catch (error) {
        console.error(`newWeek ${JSON.stringify(newWeek)}`);
        throw new functions.https.HttpsError('cancelled', 'We were unable to genearte you a set of meals. Please try again.');
    }

    const planData = plan.data()
    const planDays = util.getPlanDays(planData.days_config);

    // Create meals for week based on recipes generated
    for (let i = 0; i < required_meals; i++) {
        const mealSnapshot: FirebaseFirestore.DocumentSnapshot = new_meals[i];
        const mealData: FirebaseFirestore.DocumentData = mealSnapshot.data();

        const departments: any[] = []
        for (const department of mealData.departments) {
            const new_department_object = {
                'name': department.name,
                'ingredients': [],
            };
            departments.push(new_department_object);

            for (const ingredient of department.ingredients) {
                let new_ingredient_object: FirebaseFirestore.DocumentData;

                if (department.name !== 'Staple') {
                    for (const product of all_products) {
                        if (ingredient.product.id === product.id) {
                            new_ingredient_object = {
                                'name': product.data().name,
                                'quantity': ingredient.quantity,
                                'unit': product.data().unit,
                                'checked': 'IngredientStatus.NOT_CHECKED',
                            };

                            new_department_object.ingredients.push(new_ingredient_object);
                        }
                    }
                } else {
                    new_ingredient_object = {
                        'name': ingredient.name,
                        'quantity': ingredient.quantity,
                        'unit': ingredient.unit,
                        'checked': 'IngredientStatus.NOT_CHECKED',
                    };

                    new_department_object.ingredients.push(new_ingredient_object);
                }
            }
        }

        // plan.data().users.forEach(async (planUser: FirebaseFirestore.DocumentData) => {
        //     const userHistories: FirebaseFirestore.QuerySnapshot = await planUser.ref.collection(collectionNames.HISTORIES).get();
        //     for (const newRecipe of new_meals) {
        //         let count: number = 0;
        //         userHistories.forEach(async (userHistory: FirebaseFirestore.QueryDocumentSnapshot) => {
        //             if (userHistory.ref.path === newRecipe.ref.path) {
        //                 count++;
        //             }
        //         });
        //         if (count === 0) {
        //             const new_history_object: any = {
        //                 'ref': newRecipe.ref,
        //                 'total_times_cooked': 0,
        //                 'rating': null,
        //             }
        //             await planUser.ref.collection(collectionNames.HISTORIES).doc(newRecipe.id).set(new_history_object);
        //         }
        //     }
        // });

        const userRatings: any[] = []
        for (const planUser of plan.data().users) {
            const userHistory: FirebaseFirestore.DocumentSnapshot = await planUser.ref.collection(collectionNames.HISTORIES).doc(mealSnapshot.ref.id).get();

            if (userHistory.exists) {
                const new_user_rating_object: any = {
                    'ref': planUser.ref,
                    'rating': userHistory.data().rating,
                };
                userRatings.push(new_user_rating_object);
            } else {
                const new_history_object: any = {
                    'ref': mealSnapshot.ref,
                    'total_times_cooked': 0,
                    'rating': 0,
                }
                await planUser.ref.collection(collectionNames.HISTORIES).doc(mealSnapshot.ref.id).set(new_history_object);
                const new_user_rating_object: any = {
                    'ref': planUser.ref,
                    'rating': 0,
                };
                userRatings.push(new_user_rating_object);
            }
        };

        const recipe: any = {
            'ref': mealSnapshot.ref,
            'name': mealData.name,
            'link': mealData.link,
            'picture': mealData.picture,
            'time': mealData.time,
            'difficulty': mealData.difficulty,
            'servings': mealData.servings,
            'rating': mealData.rating_average,
            'user_ratings': userRatings,
            'author': mealData.author,
            'labels': mealData.labels,
            'departments': departments,
        }

        const new_meal_object: FirebaseFirestore.DocumentData = {
            'date': planDays[i][0],
            'users': planDays[i][1],
            'price': mealData.price,
            'recipe': recipe,
            'total_times_cooked': 0,
        }

        await firestore()
            .collection(collectionNames.PLANS)
            .doc(plan_id)
            .collection(collectionNames.WEEKS)
            .doc(startOfWeekFormatted)
            .collection(collectionNames.MEALS)
            .doc(planDays[i][2])
            .set(new_meal_object);
    }

    return 0;
}

