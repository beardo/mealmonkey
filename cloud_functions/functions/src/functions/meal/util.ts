import { firestore } from 'firebase-admin';
import collectionNames from '../../models/collectionNames';
import moment = require('moment');
import moment_tz = require('moment-timezone');
moment_tz.tz.setDefault("Pacific/Auckland");

export async function calculateRecipePrice(recipe: FirebaseFirestore.DocumentData): Promise<number> {
    // Get recipe data
    let recipePrice = 0.0;

    for (const department of recipe.departments) {
        if (department.name !== 'Staple') {
            for (const ingredient of department.ingredients) {
                recipePrice += await calculateIngredientPrice(ingredient);
            }
        }
    }

    return recipePrice;
}

export async function calculateIngredientPrice(ingredient: FirebaseFirestore.DocumentData): Promise<number> {
    const productSnapshot: FirebaseFirestore.DocumentSnapshot = await firestore().collection(collectionNames.PRODUCTS).doc(ingredient.product.id).get();
    const productData: FirebaseFirestore.DocumentData = productSnapshot.data()
    return Math.ceil(ingredient.quantity / productData.quantity) * productData.price;
}

export async function getMatchingRecipes(labels: string[], tried: FirebaseFirestore.DocumentReference[]): Promise<FirebaseFirestore.QueryDocumentSnapshot[]> {
    // Get all the recipes available for the swap
    const recipes: FirebaseFirestore.QueryDocumentSnapshot[] = [];
    const recipesQuery: FirebaseFirestore.QuerySnapshot = await firestore()
        .collection(collectionNames.RECIPES)
        .get();

    // Convert tried references to paths for better comparison later
    const triedPaths: string[] = [];
    tried.forEach((triedMeal) => {
        triedPaths.push(triedMeal.path);
    });

    // If there are no dietary requirements, add all recipes and ignore the tried ones
    if (labels.length === 0) {
        recipesQuery.docs.forEach((recipe: FirebaseFirestore.QueryDocumentSnapshot) => {
            if (triedPaths.indexOf(recipe.ref.path) === -1) {
                recipes.push(recipe);
            }
        });

    // If there are dietary requirements, get recipes that match them and ignore the tried ones
    } else {
        recipesQuery.docs.forEach((recipe: FirebaseFirestore.QueryDocumentSnapshot) => {
            if (triedPaths.indexOf(recipe.ref.path) === -1) {
                let count: number = 0;
                labels.forEach((label: string) => {
                    if (recipe.data().labels.indexOf(label) === -1) {
                        count++;
                    }
                });
                if (count === 0) {
                    recipes.push(recipe);
                }
            }
        });
    }

    return recipes;
}

export function randomIntFromInterval(min, max): number {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

export class MealSolver {

    public candidateMeals: FirebaseFirestore.DocumentSnapshot[][] = [];
    public allMeals: FirebaseFirestore.QueryDocumentSnapshot[] = [];
    public randomInt: number = 0;

    public solve(index: number, requiredMeals: number, remainingBudget: number, selectedMeals: FirebaseFirestore.DocumentSnapshot[]) {
        if (index === this.randomInt - 1 || this.candidateMeals.length > 0) {
            return;
        }
        if (requiredMeals === 0) {
            this.candidateMeals.push(selectedMeals);
        }

        for (let i = index; i < this.allMeals.length; i++) {
            if (remainingBudget >= this.allMeals[i].data().price) {
                const new_selected_meals: FirebaseFirestore.DocumentSnapshot[] = selectedMeals.slice();
                new_selected_meals.push(this.allMeals[i]);
                this.solve((i + 1) % this.allMeals.length, requiredMeals - 1, remainingBudget - this.allMeals[i].data().price, new_selected_meals);
            }
        }
        this.solve((index + 1) % this.allMeals.length, requiredMeals, remainingBudget, selectedMeals);
    }
}

export function getStartOfWeek(): Date {
    console.log(moment_tz.tz('Pacific/Auckland').startOf('isoWeek').utc().toDate());
    return moment_tz.tz('Pacific/Auckland').startOf('isoWeek').utc().toDate();
}

export function getEndOfWeek(): Date {
    console.log(moment_tz.tz('Pacific/Auckland').endOf('isoWeek').utc().toDate());
    return moment_tz.tz('Pacific/Auckland').endOf('isoWeek').utc().toDate();

}

export function getDayOfWeek(i): moment.Moment {
    return moment_tz.tz('Pacific/Auckland').startOf('isoWeek').add(i, "days").utc();
}

export function getFormatedStartOfWeek(): string {
    return moment_tz.tz('Pacific/Auckland').startOf('isoWeek').utc().format("DD-MM-YYYY");
}

export async function combineIngredients(meals: FirebaseFirestore.DocumentSnapshot[]): Promise<any[][]> {
    const combined_departments: any[] = []
    // combined_departments: [{name: "Bakery", ingredients: [{name: "Bread", Quantity: 5, Unit: "buns", Checked: False}]}]

    // data = { labels: [], tried: [], required_meals: 3, total_budget: 100, plan_id: 'G8KFO9' }
    const all_products: FirebaseFirestore.DocumentData[] = []
    for (const recipeSnapshot of meals) {
        const recipe: FirebaseFirestore.DocumentData = recipeSnapshot.data();
        // console.log(recipe.departments);

        for (const department of recipe.departments) {
            let combined_department_object: any
            let found_department: Boolean = false
            for (const existing_department of combined_departments) {
                if (department.name === existing_department.name) {
                    combined_department_object = existing_department;
                    found_department = true;
                    break;
                }
            }
            if (!found_department) {
                combined_department_object = {
                    name: department.name,
                    ingredients: [],
                };
                combined_departments.push(combined_department_object);
            }

            for (const ingredient of department.ingredients) {
                let combined_ingredient_object: any;
                let found_ingredient: Boolean = false;
                if (department.name !== 'Staple') {
                    const productSnapshot: FirebaseFirestore.DocumentSnapshot = await firestore()
                        .collection(collectionNames.PRODUCTS).doc(ingredient.product.id).get();
                    const product: FirebaseFirestore.DocumentData = productSnapshot.data();

                    for (const existing_ingredient of combined_department_object.ingredients) {
                        if (product.name === existing_ingredient.name) {
                            combined_ingredient_object = existing_ingredient;
                            found_ingredient = true;
                            break;
                        }
                    }

                    if (!found_ingredient) {
                        combined_ingredient_object = {
                            name: product.name,
                            quantity: 0,
                            unit: product.unit,
                            checked: 'IngredientStatus.NOT_CHECKED',
                        };

                        all_products.push(productSnapshot);
                        combined_department_object.ingredients.push(combined_ingredient_object);
                    }

                    combined_ingredient_object.quantity += ingredient.quantity;

                } else {
                    for (const existing_ingredient of combined_department_object.ingredients) {
                        if (ingredient.name === existing_ingredient.name) {
                            combined_ingredient_object = existing_ingredient;
                            found_ingredient = true;
                            break;
                        }
                    }

                    if (!found_ingredient) {
                        combined_ingredient_object = {
                            name: ingredient.name,
                            quantity: 0,
                            unit: ingredient.unit,
                            checked: 'IngredientStatus.NOT_CHECKED',
                        };

                        combined_department_object.ingredients.push(combined_ingredient_object);
                    }

                    combined_ingredient_object.quantity += ingredient.quantity;
                }
            }
        }
    }

    return [combined_departments, all_products];
}

export function getPlanDays(daysConfig: any): any[][] {
    const days: any[][][] = [];

    if (daysConfig.Monday) {
        console.log('0: ' + getDayOfWeek(0).toDate())
        console.log('1: ' + getDayOfWeek(0).format("DD-MM-YYYY"))
        days.push([getDayOfWeek(0).toDate(), daysConfig.Monday, getDayOfWeek(0).format("DD-MM-YYYY")])
    }
    if (daysConfig.Tuesday) {
        days.push([getDayOfWeek(1).toDate(), daysConfig.Tuesday, getDayOfWeek(1).format("DD-MM-YYYY")])
    }
    if (daysConfig.Wednesday) {
        days.push([getDayOfWeek(2).toDate(), daysConfig.Wednesday, getDayOfWeek(2).format("DD-MM-YYYY")])
    }
    if (daysConfig.Thursday) {
        days.push([getDayOfWeek(3).toDate(), daysConfig.Thursday, getDayOfWeek(3).format("DD-MM-YYYY")])
    }
    if (daysConfig.Friday) {
        days.push([getDayOfWeek(4).toDate(), daysConfig.Friday, getDayOfWeek(4).format("DD-MM-YYYY")])
    }
    if (daysConfig.Saturday) {
        days.push([getDayOfWeek(5).toDate(), daysConfig.Saturday, getDayOfWeek(5).format("DD-MM-YYYY")])
    }
    if (daysConfig.Sunday) {
        days.push([getDayOfWeek(6).toDate(), daysConfig.Sunday, getDayOfWeek(6).format("DD-MM-YYYY")])
    }
    return days;
}

