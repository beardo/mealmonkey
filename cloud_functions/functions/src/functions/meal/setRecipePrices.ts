import * as functions from 'firebase-functions';
import * as util from './util';
import { firestore } from 'firebase-admin';
import collectionNames from '../../models/collectionNames';

export const setRecipePrices = functions.https.onCall(async (data, _) => {
// export const setRecipePrices = functions.runWith({timeoutSeconds: 540, memory: '2GB'}).firestore.document('notinuse').onCreate(async (data, _) => {
    const labels: string[] = data.data().labels;
    const tried: FirebaseFirestore.DocumentReference[] = data.data().tried;

    // data = { labels: [], tried: [] }
    // Cauli: /recipes/-LNmpgD2UsRH95pyuw12
    // Feta: /recipes/-LNs_9yIrS_vI1yf-ldQ

    // Get all the recipes that match the dietary requirements and are not in tried
    let recipes: FirebaseFirestore.DocumentSnapshot[] = await util.getMatchingRecipes(labels, tried);
    recipes = recipes.reverse()
    for (const recipe of recipes) {
        const recipePrice: number = await util.calculateRecipePrice(recipe.data());
        console.log(recipe.ref.id);
        firestore().collection(collectionNames.RECIPES).doc(recipe.ref.id)
        .update({
            price: recipePrice
        });
    }
});
