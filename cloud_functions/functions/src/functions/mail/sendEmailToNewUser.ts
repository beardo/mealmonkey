import * as functions from 'firebase-functions';
import { sgMail } from '../config';

export const sendEmailToNewUser = functions.firestore.document('users/{userId}').onCreate(
    (snap, _) => {
        const newUser: FirebaseFirestore.DocumentData = snap.data();

        const joke = getRandomJoke();

        const msg: any = {
            to: newUser.email,
            from: {
               name: 'Moojo the Monkey',
               email: 'noreply@mealmonkey.nz',
            },
            replyTo: 'developers@mealmonkey.nz',
            subject: 'Welcome to Meal Monkey!',
            templateId: 'd-06059fe7f7b045b5a388ed4b1471ac3c',
            dynamic_template_data: {
                displayName: newUser.username,
                jokeQuestion: joke.question,
                jokeAnswer: joke.answer,
            }
        };

        return sgMail.send(msg);
    }
);


function getRandomJoke(): any {
    const jokes: any[] = [
        {question: 'What do you call a monkey that wins the World Series?', answer: 'A chimpion.'},
        {question: 'What do you call a baby monkey?', answer: 'A chimp off the old block'},
        {question: 'Where do monkeys go to grab a beer?', answer: 'The monkey bars!'},
        {question: 'What kind of key opens a banana?', answer: 'A mon-key!'},
        {question: 'What do you call a monkey in a minefield?', answer: 'A Ba-boom!'},
        {question: 'Why don’t monkeys play cards in the jungle?', answer: 'There are too many cheetahs around.'},
        {question: 'Did you hear about that dumb party in the jungle?', answer: 'Someone forgot to bring the chimps and dip.'},
        {question: 'What did the monkey say when someone cut off its tail?', answer: 'It won’t be long now.'},
        {question: 'Why do monkeys love bananas?', answer: 'Because they have appeal.'},
        {question: 'What does a banana do when it sees a monkey?', answer: 'The banana splits.'},
        {question: 'Why shouldn’t you fight with a monkey?', answer: 'They use gorilla warfare.'},
        {question: 'Where do chimps hear all their gossip?', answer: 'The ape vine.'},
    ];
    return jokes[randomIntFromInterval(0, jokes.length - 1)];
}

function randomIntFromInterval(min, max): number {
    return Math.floor(Math.random() * (max - min + 1) + min);
}