import * as functions from 'firebase-functions';
const request = require('request');
import { google } from 'googleapis';
export const backupFirestoreData = functions.pubsub.topic("backupFirestoreData").onPublish(async (event) => {

    const url: string = "https://firestore.googleapis.com/v1beta1/projects/meal-monkey-proj/databases/*:exportDocuments";
    const body = { outputUriPrefix: "gs://meal-monkey-proj-firestore-backup" };

    const serviceAccount: any = {
        "type": "service_account",
        "project_id": "meal-monkey-proj",
        "private_key_id": "76e0bcd7384e0d87a0a438bdf7e8ff8339a7e951",
        "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCzUTZ3GBL3C53L\n4fdTYnEXGcf0KaADb7WWPlv7jqAbDaf4cJ1yzphG+4W6l0IXzUR2R7xro7GtNe3z\nhzmHbu0w5LvlK60BDppA9iCNJkNLlyjw/P4kGfW3+zXgIoEVz5+W6onsb5CnHMBv\nW6GiPnw6g6fzvueDrFaiB6G5kjvHmxMvWY5hfYUoPjFZAyaQzSa/TWf8Ftvegcdz\ncPViaf0rtj9Bo5qm/3OngGAYV4S8IK8ozNPNMYi4IvtWaIwzB2MIbHihHStkSxZV\nSfZWeTCC0KObUxgzX+Ur6o6osXHP5XRvyjmdnIy5GLetLCLzy0uv5tDN4Ex6LNX5\nEjzwjXklAgMBAAECggEAJXfsx4nSPKjCuh1nTpcdRlRuwYgJFW68JL62/aNZtQ1v\ncawmWhztGLfpw4ljmBGeIpDuwBceQzEnnWE+FhyJl1PUJxivN69TcAt91cCPAzZ8\nqY5TlDuQanclVMYg7uvJTwGG/E57jU+mrh4ZcgtYbemgqH/VZGAYYhbyJhJ2R0i4\ndVREPOhEHicL3QDzYcf8evsIM6QUkIoSo4Z4Zsa0TNt0EPBhYeeRk0nUACV+iGxy\nxm9J9ngUhyS/Q1DZebL7lpMv7VfBHT7569M+7NTyP9MEpU0x7kQ5EQe5gDJxmzrl\n8YhH5PnYC6QOPdygLmdh9TgbyVFCCSFNeqVj2Zh8AQKBgQDabI8AyGtLHYR2+P9e\npydX7hKdiZMh5a5cxj5NLxea85lT4DH3/FOTS9S8Vqch7+m/Mto2mnHHtPabJf5s\ndDiidHIIDbzi1TOhjC6aKP9LIIdDdiA0A6I2qeWHdDYFtFHmUskTW4eyh09dGo6t\nodvmdqwZ9Cj2awkAKb9zaRV8gQKBgQDSKmGvoxT5bS927M4e+iXb7DIrWJlVT2fw\nkqGXKlPvEgS/xzUY4lzYtvwTb9rPMFxEJLg+Mr+MYjqTaQ5lF0xHfAj5e6PpMwBw\nu4zlNJ6qjqSrejk5cVFXhLv1+GZNJ/FNy/qnj9rOWhbv+rBWt4c5UWsmsMsgeVYJ\naDoG0/86pQKBgB3+2oSZAWMHYrGT8D3nZpfEsfnActws1o0XDGc9GFjHKz9Bgpsz\no6aOu2BB5iHa+JaxPRZSj12FlHgp946dR0nIBADnIhFkhfqhbaTuDGEcznaNOk7q\nUNSA8GxxoRmXccfXhpWJ9FCoFFYM15swpyo3EdtelGGCwXGsSgZnX/QBAoGATCsg\nDE/Goj94HFnmukNexT3tUNhjeg+1U/NQrUCh+ttE7Vx9We9wRvrKkSDQ5JZaWjgU\nrjYmRfKz2AkTNoAjHSVO+1xcI4BggHr4rXlMp/Paqms5oh/3owhwNs04Bm6bfd6X\nkXMJkhAEkCUDE+qgD536f6NDexl+nRoxLZhXwCECgYBf4kCWbmDKjVTJ1aun3A/A\n26/EK8QFbQOAj2MB/GV9k7QvlK6afteFR2/oBOvzRhQU6KiyR7n4ju8SWjBLTwms\nmePI/QdqlsHOLPj6X73/qMeO9lFvp4tTfxF+dK06RCziyoTgfT2v5FlIYEhlVAZK\nLig3NtSEWWyZ1+EVON7ThA==\n-----END PRIVATE KEY-----\n",
        "client_email": "root-229@meal-monkey-proj.iam.gserviceaccount.com",
        "client_id": "100796508998879859238",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/root-229%40meal-monkey-proj.iam.gserviceaccount.com"
    }

    const googleJWTClient = new google.auth.JWT(
        serviceAccount.client_email,
        null,
        serviceAccount.private_key,
        ['https://www.googleapis.com/auth/datastore'],
        null,
    )

    googleJWTClient.authorize((error, access_token) => {
        if (error) {
           return console.error("Couldn't get access token", error)
        }

        request({
            url: url,
            method: "POST",
            json: true,
            headers: {
                'Authorization': ' Bearer ' + access_token.access_token,
            },
            body: body
        }, function (e, response) {
            if (!e && response.statusCode === 200) {
                const date: string = new Date().toLocaleString('en-US', { timeZone: 'Pacific/Auckland' });
                console.log('Successfully backed up data at ' + date + '.');
            } else {
                console.log(access_token);
                console.log('Something went wrong backing up data. Error: ' + e + ', Status Code: ' + response.statusCode);
            }
        });
     });


});