export const PLANS = "plans";
export const WEEKS = "weeks";
export const MEALS = "meals";
export const USERS = "users";
export const RECIPES = 'recipes';
export const HISTORIES = 'histories';
export const PRODUCTS = 'products';

export default { PLANS, WEEKS, MEALS, USERS, RECIPES, HISTORIES, PRODUCTS };