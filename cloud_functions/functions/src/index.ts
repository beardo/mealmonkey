import * as admin from 'firebase-admin';

const serviceAccount: any = {
    "type": "service_account",
    "project_id": "meal-monkey-proj",
    "private_key_id": "cede948dd0b57269c06a2fb521f0f16f77dd7d93",
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCvTn1MXUmmJzkI\n3sdKowmPPH0A6TLcqZ8F9VghL9sT9a9BqgYS+hmfjtIMU4LVRY/BwJxg/miqrqqA\n00fh9JVMjiaQG9PjSPqs+lqYwKGOvoeP1T5lzQ5Z+Pj4fw93D2Kl5ycVxzdxejLr\nNPyCO+B1FynSTkDB+XTauroWMGIbdv0LaUK0Fq5UgreoEhHoefHaA6xCeJblwcZf\nInejnG+AEUzjIsA55PKwm7681F5gce/+U9Xp9khFDI/JQnHaJfnWaPKsf/ykSPS4\nU+rKQmKjIY/Cijcgb6KK0Ksh4ljylF/1jZSOfLJNbgTsWKyEqTbw73BKjjjoJswM\nL0guLZfNAgMBAAECggEAAvkiT+K/JoXAxmE84Qut1FOSzSqnVa28yDWWho2QuiBv\nFf6DttuuBpI+Br592A8jdoZFQLHf5ZErzB6JOBBVXeSFS+lk6RRqR/fBBJkzxD9/\n/hUAR2q4ZRc/ZbuF8B9/LVAjzg9AZfjMF0ghRjYswt+fMnVoTMHl1PXYxRqR92RA\nhkbQ2I7mS6wFW2HPeZz8WbyP8jXNmg6/s0CiKlevrYsUqkVqm/6lwAx44QBryVu4\nS83wn4AMCT43IwW/vPCAvI9RlhNTwFmYqW/WhsFeJSq0ZZvs9UMpgBaCmxUmGofc\nM1atnlewRggDbWeBOJI+BIZEmKD8gmq3xmnniMEcIwKBgQDk9+4adg6i8DTe3e0d\nz0L3kJYUG8WFuBK/qsX6px/EquXaFhNJce9gApDg2lvQoL9oYgeAGnC3svqMVeq6\n4ZnC9QR92O0XxkS4CSHR2tFwUFtkb9LRpwEgFFOBY/Gke+8uRe+cFi1KKZiLUNq1\nw4TeBTyO30nlydIgcUI3bjXyewKBgQDEAL8wN6Qem2SG0Xnb7rrYCH+EZWdzU5sL\nT4ULGU+imn5uOo75KY4344pdfNS68TbthXEz96xdfPAvKA3z2NLESDegJp+3dBrO\nPRKNVxv7TD6ckWd1yKJFXuD2PoPWDHn0uIMl9Ak/cB5hcVv2gbKMkOMaytSWglon\nXx3nt2mQVwKBgHKHJR0tQiL0Pa3gmfNYMan9M2R5g7JgIVinxgGbSMOpq/ELK7iH\n+fEvN2KRNm3DihpfNls4Xns0ViiN7SxvHecrgYSlVeqXbNYVE0qRxkMEnsI9QNlH\nTMPMBvzojfz1VOzzZGfRm/PFnJzK46sZUkNvoRvhymfbLHS30iRBx1AzAoGAHxgz\nPJYHLmfemdjPqUmFUNKAUGO59rzTwwaZh7o7PQOffSNDHVovWsR5Zf1wzjuISEQ8\nQk9PhAFXJVQIaqACR+thD8Iw4mzD9+YOdGfWY5aj9DjJHhF3VOQJcmdWiPWvv1gq\nTjIZ6XuDBlhyi3fl+JDfxpdZBIune8mGOAWL9L8CgYAp1siAbebqSb2JTAOGgOEl\nnZ5eq2qGYIFvRzBAXg0hgFSz/+gjpMcXsZ0+zvUwo6lFT2ERWJqXphQOs2mWAdXG\ns3FOf4mrmrm/psDkIsRK/q2YVaN4CNSmxNBpd38BvaogjJvlP1MI9j3DpwU63JTu\nSxMSMu9TU57PX1EwzyVjfg==\n-----END PRIVATE KEY-----\n",
    "client_email": "firebase-adminsdk-waiiq@meal-monkey-proj.iam.gserviceaccount.com",
    "client_id": "114873463319170581649",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-waiiq%40meal-monkey-proj.iam.gserviceaccount.com"
}

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://meal-monkey-proj.firebaseio.com",
});

const settings = {timestampsInSnapshots: true};
admin.firestore().settings(settings);

export * from './functions/mail';
export * from './functions/plan';
export * from './functions/user';
export * from './functions/routines';
export * from './functions/backup';
export * from './functions/meal';

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
