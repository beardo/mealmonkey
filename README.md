### Meal Monkey

Welcome to the repository for Meal Monkey!

If you are looking for our frontend code, you will find it in the `flutter`
folder. The main code for flutter is within the `lib` folder, and has been
divided into some helpful sections:
- `exceptions` holding our custom exceptions
- `firebase` covering models for syncing up with our backend
- `screens` which has a file for every screen in the app
- `state` for controlling our local state, which syncs to the backend

Our backend code can be found in the `cloud_functions` folder. Below that is 
`functions/src` which contains some helpful models, and then another `functions`
folder that has the actual cloud functions that run on our backend:
- `backup` runs an automatic backup of our database
- `mail` sends a welcome email to our new users
- `meal` contains several meal-related functions, including generating a meal 
plan and swapping out a meal
- `plan` deletes old plans when a user starts a new one
- `routines` sends push notifications to our users
- `user` updates a user on our backend
